<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $fillable = ['clinic_id', 'clinic_name', 'vision', 'mission', 'address', 'phone', 'work_time'];
}
