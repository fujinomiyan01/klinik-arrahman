<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $fillable = ['username', 'title', 'detail', 'category_name'];
    
    public function CategoryId(){
        return $this->hasMany(Category::class);
    }
}
