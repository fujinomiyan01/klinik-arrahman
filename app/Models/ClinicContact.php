<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClinicContact extends Model
{
    use HasFactory;
    
    protected $fillable = ['clinic_name', 'facebook_name', 'facebook_link', 'instagram_name', 'instagram_link', 'twitter_name', 'twitter_link', 'address', 'googleMap_link', 'phone'];
}
