<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $fillable = ['doctor_name', 'role', 'detail', 'image'];


    // public function doctor() {
    //     return $this->belongsTo(DoctorClinic::class); 
    // }
}
