<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ['category_name'];

    public function Usernames(){
        return $this->belongsTo(User::class);
    }

    public function CategoryId(){
        return $this->belongsTo(Article::class);
    }
}
