<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorClinic extends Model
{
    protected $fillable = ['clinic_id', 'doctor_id'];

    // public function id() {
    //     return $this->hasMany(Doctor::class, 'id', 'doctor_id'); 
    // }

    // public function ClinicId() {
    //     return $this->belongsTo(Clinic::class); 
    // }
}   
