<?php

namespace App\Http\Controllers;

use App\Models\HeroContent;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AddHeroContentController extends Controller
{
    public function create(){
        return view('admin.add-heroContent');
    }

    public function store(Request $request){
        $request->validate([
            'title' => 'string|min:6',
            'detail' => 'string|min:15',
            // 'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        // $getMime = $request->file('image')->getMimeType(); 
        // $explodedMime = explode('/' ,$getMime);
        // $mime = end($explodedMime);
        // $name = Str::random(40) . '.' . $mime;
        // $request->image->move('img', $name);

        $heroContent = new HeroContent();

        $heroContent->title = $request->title;
        $heroContent->detail = $request->detail;
        // $heroContent->image = ('/img/'. $name);

        $heroContent->save();
        return redirect('/admin/heroContent')->with('success', 'Data berhasil ditambahkan');
    }
}
