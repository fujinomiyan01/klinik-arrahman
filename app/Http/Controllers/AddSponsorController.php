<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sponsor;
use Illuminate\Support\Str;

class AddSponsorController extends Controller
{
    public function create(){
        return view('admin.add-sponsor');
    }

    public function store(Request $request){
        $request->validate([
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        $getMime = $request->file('image')->getMimeType(); 
        $explodedMime = explode('/' ,$getMime);
        $mime = end($explodedMime);
        $name = Str::random(40) . '.' . $mime;
        $request->image->move('img', $name);

        $sponsor = new Sponsor();

        $sponsor->image = ('/img/'. $name);

        $sponsor->save();
        return redirect('/admin/sponsor');
    }
}
