<?php

namespace App\Http\Controllers;

use App\Models\Clinic;
use App\Models\ClinicContact;
use Illuminate\Http\Request;

class EditClinicContactController extends Controller
{
    public function edit($id){
        $clinics = Clinic::select('*')->get();
        $clinic_edit = ClinicContact::select('*')->where('id', $id)->first(); 
        return view('admin.editClinicContact', compact('clinics', 'clinic_edit'));
    }

    public function update(Request $request){
        $clinicContact = ClinicContact::select('*')->where('id', $request->id)->first();

        if($request->clinic_name){
            $request->validate([
                'clinic_name' => 'string',
            ]);
            $clinicContact->clinic_name = $request->clinic_name;
        }

        if($request->facebook_name){
            $request->validate([
                'facebook_name' => 'string',
            ]);
            $clinicContact->facebook_name = $request->facebook_name;
        }

        if($request->facebook_link){
            $request->validate([
                'facebook_link' => 'url',
            ]);
            $clinicContact->facebook_link = $request->facebook_link;
        }

        if($request->instagram_name){
            $request->validate([
                'instagram_name' => 'string',
            ]);
            $clinicContact->instagram_name = $request->instagram_name;
        }

        if($request->instagram_link){
            $request->validate([
                'instagram_link' => 'url',
            ]);
            $clinicContact->instagram_link = $request->instagram_link;
        }

        if($request->twitter_name){
            $request->validate([
                'twitter_name' => 'string',
            ]);
            $clinicContact->twitter_name = $request->twitter_name;
        }

        if($request->twitter_link){
            $request->validate([
                'twitter_link' => 'url',
            ]);
            $clinicContact->twitter_link = $request->twitter_link;
        }

        if($request->address){
            $request->validate([
                'address' => 'string',
            ]);
            $clinicContact->address = $request->address;
        }

        if($request->googleMap_link){
            $request->validate([
                'googleMap_link' => 'url',
            ]);
            $clinicContact->googleMap_link = $request->googleMap_link;
        }

        if($request->email){
            $request->validate([
                'email' => 'email',
            ]);
            $clinicContact->email = $request->email;
        }

        if($request->phone){
            $request->validate([
                'phone1' => 'numeric|min:10',
            ]);
            $clinicContact->phone = $request->phone;
        }

        $clinicContact->save();
        return redirect('/admin/clinicContact')->with('success', 'Update berhasil');
    }
}
