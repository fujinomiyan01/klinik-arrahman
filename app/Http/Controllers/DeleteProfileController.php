<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;

class DeleteProfileController extends Controller
{
    public function __invoke($id){
        $facility = Profile::find($id);

        $facility->delete();

        return back()->with('delete', 'Delete berhasil');
    }
}
