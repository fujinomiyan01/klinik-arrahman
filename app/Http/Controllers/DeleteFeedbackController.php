<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use Illuminate\Http\Request;

class DeleteFeedbackController extends Controller
{
    public function __invoke($id){
        $facility = Feedback::find($id);

        $facility->delete();

        return back()->with('delete', 'Delete berhasil');
    }
}
