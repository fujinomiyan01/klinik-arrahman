<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ClinicContact;
use App\Models\Facility;
use App\Models\Profile;
use App\Models\Service;
use Illuminate\Http\Request;

class UserContactController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // $articles = Article::select('*')->where('id', $id)->first();
        $services = Service::select('*')->get();
        $facilities = Facility::select('*')->get();
        $profiles = Profile::select('*')->first();
        $clinicContact = ClinicContact::select('*')->first();

        return view('kontak', [
            // 'articles' => $articles,
            'services' => $services,
            'facilities' => $facilities,
            'profiles' => $profiles,
            'clinicContact' => $clinicContact
        ]);
    }
}
