<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Doctor;
use App\Models\Facility;
use App\Models\Feedback;
use App\Models\Service;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function create () {
        $articles = Article::select('*')->get();
        $feedbacks = Feedback::select('*')->get();
        $facilities = Facility::select('*')->get();
        $services = Service::select('*')->get();
        $doctors = Doctor::select('*')->get();

        // $facility =
        return view('admin.index', [
            'articles' => $articles,
            'feedbacks' => $feedbacks,
            'facility' => $facilities,
            'services' => $services,
            'doctors' => $doctors
            

        ]);
    }

    

   
}
