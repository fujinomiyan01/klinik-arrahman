<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClinicContactController extends Controller
{
    public function __invoke(){
        return view('admin.clinicContact');
    }
}
