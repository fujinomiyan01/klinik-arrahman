<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use App\Models\Clinic;
use App\Models\Doctor;
use App\Models\Facility;
use App\Models\Feedback;
use App\Models\Gallery;
use App\Models\HeroContent;
use App\Models\HeroImage;
use App\Models\Profile;
use App\Models\Service;
use App\Models\Sponsor;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __invoke () {
        $feedbacks = Feedback::select('*')->get();
        $facilities = Facility::select('*')->get();
        $services = Service::select('*')->get();
        $doctors = Doctor::select('*')->get();
        $categories = Category::select('*')->get();
        $galleries = Gallery::select('*')->get();
        $profiles = Profile::select('*')->first();
        $clinics = Clinic::select('*')->get();
        $articles = Article::select('*')->limit(3)->orderBy('created_at', 'desc')->get();
        $sponsors = Sponsor::select('*')->get();
        $heroContents = HeroContent::select('*')->first();
        $heroImages = HeroImage::select('*')->get();
        // $firstHeroImage = HeroImage::select('*')->latest();

        // dd($heroContents);
      
        return view('index', [
            'feedbacks' => $feedbacks,
            'facilities' => $facilities,
            'services' => $services,
            'doctors' => $doctors,
            'categories' => $categories,
            'galleries' => $galleries,
            'profiles' => $profiles,
            'clinics' => $clinics,
            'articles' => $articles,
            'sponsors' => $sponsors,
            'heroContents' => $heroContents,
            'heroImages' => $heroImages,
            // 'firstHeroImage' => $firstHeroImage
        ]);
    }
}
