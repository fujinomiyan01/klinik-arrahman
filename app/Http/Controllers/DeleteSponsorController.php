<?php

namespace App\Http\Controllers;

use App\Models\Sponsor;
use Illuminate\Http\Request;

class DeleteSponsorController extends Controller
{
    public function __invoke($id){
        $sponsor = Sponsor::find($id);

        $sponsor->delete();

        return back()->with('delete', 'Delete berhasil');
    }
}
