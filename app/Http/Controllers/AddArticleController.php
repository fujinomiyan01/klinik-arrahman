<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AddArticleController extends Controller
{
    public function create(){
        $categories = Category::select('*')->get();
        $articles = Article::select('*')->get();
        return view('admin.add-article', compact('categories', 'articles'));
        
    }
    
    public function store(Request $request){
        $request->validate([
            'title' => 'required|string|max:150',
            'detail' => 'required|min:30',
            'category_name' => 'required',
        ]);

        $getMime = $request->file('image')->getMimeType(); 
        $explodedMime = explode('/' ,$getMime);
        $mime = end($explodedMime);
        $name = Str::random(40) . '.' . $mime;
        $request->image->move('img', $name);

        $save = new Article();
        $admin = User::select('*')->where('id', $request->user_id)->first();

        $save->username = $admin->name;
        $save->title = $request->title;
        $save->detail = $request->detail;
        $save->image = ('/img/'. $name);
        $category_name = '';

        foreach($request->category_name as $name){
            $category_name .= $name . ','; 
        }

        $save->category_name = $category_name;
        $save->save();
        return redirect('/admin/article')->with('success', 'Data berhasil ditambahkan');
    }
}
