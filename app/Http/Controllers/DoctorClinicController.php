<?php

namespace App\Http\Controllers;

use App\Models\Clinic;
use App\Models\Doctor;
use App\Models\DoctorClinic;
use Illuminate\Http\Request;

class DoctorClinicController extends Controller
{
    public function create(){
        // $doctor_clinics = DoctorClinic::with('id')->get();
        // $doctors = Doctor::with('doctor')->get();
        // $clinics = Clinic::with('ClinicId')->get();
        // return view('admin.doctorClinic', compact('doctor_clinics', 'doctors'));
        $doctorClinics = DoctorClinic::select('*')->get();
        return view('admin.doctorClinic', compact('doctorClinics'));
    }

    public function store(Request $request){
        $request->validate([
            'clinic_id' => 'required| numeric',
            'doctor_id' => 'required| numeric',
        ]);
        
        $save = new DoctorClinic();

        $save->clinic_id = $request->clinic_id;
        $save->doctor_id = $request->doctor_id;

        $save->save();
        return back()->with('success', 'Added Successfully');
    }
}

