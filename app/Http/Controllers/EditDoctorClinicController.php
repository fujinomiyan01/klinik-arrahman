<?php

namespace App\Http\Controllers;

use App\Models\DoctorClinic;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EditDoctorClinicController extends Controller
{
    public function edit($id){ 
        $doctorClinic = DoctorClinic::select('*')->where('id', $id)->first();
        return view('admin.editDoctorClinic', [
            'doctorClinic' => $doctorClinic,
        ]);
    }

    public function update(Request $request){
        $doctorClinic = DoctorClinic::select('*')->where('id', $request->id)->first();

        if($request->clinic_id){
            $doctorClinic->clinic_id = $request->clinic_id;
        }

        if($request->doctor_id){
            $doctorClinic->doctor_id = $request->doctor_id;
        }

        $doctorClinic->save();
        return redirect('/admin/doctorClinic')->with('success', 'Update berhasil');
    }
}
