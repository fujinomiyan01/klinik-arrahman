<?php

namespace App\Http\Controllers;

use App\Models\DoctorClinic;
use Illuminate\Http\Request;

class DeleteDoctorClinicController extends Controller
{
    public function __invoke($id){
        $facility = DoctorClinic::find($id);

        $facility->delete();

        return back();
    }
}
