<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AddCategoryController extends Controller
{
    public function create(){
        return view('admin.add-category');
    }

    public function store(Request $request){
        $request->validate([
            'category_name' => 'required|string|min:2|max:100',
        ]);

        $save = new Category();
        $save->category_name = Str::ucfirst($request->category_name);

        $save->save();
        return redirect('/admin/category')->with('success', 'Data Berhasil Ditambahkan');
    }
}
