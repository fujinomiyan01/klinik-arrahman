<?php

namespace App\Http\Controllers;

use App\Models\Facility;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AddFacilityController extends Controller
{
    public function create(){
        return view('admin.add-facility');
    }

    public function store(Request $request){
        $request->validate([
            'facility_name' => 'required|string',
            'mini_detail' => 'required|string|max:50',
            'detail' => 'required|string|min:30',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);
    
    
        //    $path = $request->file('image')->store('/public/img');
        $getMime = $request->file('image')->getMimeType(); 
        $explodedMime = explode('/' ,$getMime);
        $mime = end($explodedMime);
        $name = Str::random(40) . '.' . $mime;
        $request->image->move('img', $name);

        $save = new Facility();
        
        $save->facility_name = $request->facility_name;
        $save->mini_detail = $request->mini_detail;
        $save->detail = $request->detail;
        $save->route = str_replace(' ', '_', $request->facility_name);
        $save->image = ('/img/'. $name);

        $save->save();
        return redirect('/admin/facility')->with('success', 'Data berhasil ditambahkan');
    }
}
