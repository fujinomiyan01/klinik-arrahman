<?php

namespace App\Http\Controllers;

use App\Models\Clinic;
use Illuminate\Http\Request;

class DeleteClinicController extends Controller
{
    public function __invoke($id){
        $facility = Clinic::find($id);

        $facility->delete();

        return back()->with('delete', 'Delete berhasil');
    }
}
