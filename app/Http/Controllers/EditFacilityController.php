<?php

namespace App\Http\Controllers;

use App\Models\Facility;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EditFacilityController extends Controller
{
    public function edit($id){ 
        $facilities = Facility::select('*')->where('id', $id)->first();
        return view('admin.editFacility', compact('facilities'));
    }

    public function update(Request $request){
        $facility = Facility::select('*')->where('id', $request->id)->first();

        if($request->facility_name){
            $request->validate([
                'facility_name' => 'string',
            ]);
            $facility->facility_name = $request->facility_name;
        }

        if($request->mini_detail){
            $request->validate([
                'mini_detail' => 'string|max:50',
            ]);
            $facility->mini_detail = $request->mini_detail;
        }

        if($request->detail){
            $request->validate([
                'detail' => 'string|min:30',
            ]);
            $facility->detail = $request->detail;
        }

        if($request->image){
            $request->validate([
                'image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);

            $getMime = $request->file('image')->getMimeType(); 
            $explodedMime = explode('/' ,$getMime);
            $mime = end($explodedMime);
            $name = Str::random(40) . '.' . $mime;
            $request->image->move('img', $name);
    
            $facility->image = ('/img/' . $name);
        }

        $facility->save();
        return redirect('/admin/facility')->with('success', 'Update berhasil');
    }
}
