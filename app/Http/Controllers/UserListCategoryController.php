<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ArticleComments;
use App\Models\Category;
use App\Models\Facility;
use App\Models\Service;
use Illuminate\Http\Request;

class UserListCategoryController extends Controller
{
    public function __invoke($category_name){

        // $category = Category::select('category_name')->get();
        $search = request()->query('search', '');
        $articleComments = ArticleComments::select('*')->get();
        $popularArticles = Article::select('*')->limit(3)->orderBy('views', 'DESC')->get(); 

        
        $articles = Article::where('category_name', 'LIKE','%'.$category_name.'%')
            ->when($search, function($query) use ($search) {
                $query->where('title', 'LIKE','%'.$search.'%');
            })
            ->paginate(9);

        $categories = Category::select('*')->get();
        $services = Service::select('*')->get();
        $facilities = Facility::select('*')->get();

        return view('artikel-kategori', compact('articles', 'categories', 'services', 'facilities', 'popularArticles'));
    }
}
