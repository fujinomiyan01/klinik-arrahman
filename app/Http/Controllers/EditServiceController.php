<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EditServiceController extends Controller
{
    public function edit($id){ 
        $services = Service::select('*')->where('id', $id)->first();
        return view('admin.editService', compact('services'));
    }

    public function update(Request $request){
        $service = Service::select('*')->where('id', $request->id)->first();

        if($request->service_name){
            $request->validate([
                'detail' => 'string',
            ]);
            $service->service_name = $request->service_name;
        } 
        
        if($request->mini_detail){
            $request->validate([
                'mini_detail' => 'string|max:50',
            ]);
            $service->mini_detail = $request->mini_detail;
        } 

        if($request->detail){
            $request->validate([
                'detail' => 'string|min:30',
            ]);
            $service->detail = $request->detail;
        } 
        
        if($request->image){
            $request->validate([
                'image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            
            $getMime = $request->file('image')->getMimeType(); 
            $explodedMime = explode('/' ,$getMime);
            $mime = end($explodedMime);
            $name = Str::random(40) . '.' . $mime;
            $request->image->move('img', $name);

            $service->image = ('/img/' . $name);
        }
        
        $service->save();
        return redirect('/admin/service')->with('success', 'Update berhasil');
    }
}
