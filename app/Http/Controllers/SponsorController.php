<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SponsorController extends Controller
{
    public function __invoke(){
        return view('admin.sponsor');
    }
}
