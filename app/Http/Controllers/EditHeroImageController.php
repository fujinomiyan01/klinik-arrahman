<?php

namespace App\Http\Controllers;

use App\Models\HeroImage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EditHeroImageController extends Controller
{
    public function edit($id){ 
        $heroImage = HeroImage::select('*')->where('id', $id)->first();
        return view('admin.editHeroImage', compact('heroImage'));
    }

    public function update(Request $request){
        $heroImage = HeroImage::select('*')->where('id', $request->id)->first();
        
        $request->validate([
            'image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);
        
        $getMime = $request->file('image')->getMimeType(); 
        $explodedMime = explode('/' ,$getMime);
        $mime = end($explodedMime);
        $name = Str::random(40) . '.' . $mime;
        $request->image->move('img', $name);

        $heroImage->image = ('/img/' . $name);
        
        
        $heroImage->save();
        return redirect('/admin/heroImage')->with('success', 'Update berhasil');
    }
}
