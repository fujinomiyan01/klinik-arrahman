<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HeroContentController extends Controller
{
    public function __invoke(){
        return view('admin.heroContent');
    }
}
