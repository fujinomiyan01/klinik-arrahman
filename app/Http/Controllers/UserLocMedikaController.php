<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use App\Models\Facility;
use App\Models\Service;
use Illuminate\Http\Request;

class UserLocMedikaController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $doctors = Doctor::select('*')->get();
        $services = Service::select('*')->get();
        $facilities = Facility::select('*')->get();

        return view('lokasi-medika', [
            'doctors' => $doctors,
            'services' => $services,
            'facilities' => $facilities
        ]);
    }
}
