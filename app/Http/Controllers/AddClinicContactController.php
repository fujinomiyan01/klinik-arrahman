<?php

namespace App\Http\Controllers;

use App\Models\Clinic;
use App\Models\ClinicContact;
use Illuminate\Http\Request;

class AddClinicContactController extends Controller
{
    public function create(){
        $clinics = Clinic::get();
        return view('admin.add-clinicContact', compact('clinics'));
    }

    public function store(Request $request){
        $request->validate([
            'clinic_name' => 'string|required',
            'address' => 'string|required',
            'googleMap_link' => 'url|required',
            'email' => 'email|required',
            'phone' => 'numeric|required|min:10',
        ]);

        $clinicContact = new ClinicContact();

        if($request->facebook_name){
            $request->validate([
                'facebook_name' => 'string',
            ]);
            $clinicContact->facebook_name = $request->facebook_name;
        }

        if($request->facebook_link){
            $request->validate([
                'facebook_link' => 'url',
            ]);
            $clinicContact->facebook_link = $request->facebook_link;
        }

        if($request->instagram_name){
            $request->validate([
                'instagram_name' => 'string',
            ]);
            $clinicContact->instagram_name = $request->instagram_name;
        }

        if($request->instagram_name){
            $request->validate([
                'instagram_link' => 'url',
            ]);
            $clinicContact->instagram_link = $request->instagram_link;
        }

        if($request->twitter_name){
            $request->validate([
                'twitter_name' => 'string',
            ]);
            $clinicContact->twitter_name = $request->twitter_name;
        }

        if($request->twitter_link){
            $request->validate([
                'twitter_link' => 'url',
            ]);
            $clinicContact->twitter_link = $request->twitter_link;
        }

        $clinicContact->clinic_name = $request->clinic_name;
        $clinicContact->address = $request->address;
        $clinicContact->googleMap_link = $request->googleMap_link;
        $clinicContact->email = $request->email;
        $clinicContact->phone = $request->phone;

        $clinicContact->save();
        return redirect('/admin/clinicContact')->with('success', 'Data berhasil ditambahkan');
    }
}
