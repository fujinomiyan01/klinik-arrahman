<?php

namespace App\Http\Controllers;

use App\Models\Clinic;
use App\Models\Doctor;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AddClinicController extends Controller
{
    public function create(){
        $doctors = Doctor::select('*')->get();
        return view('admin.add-clinic', compact('doctors'));
    }

    public function store(Request $request){
        $request->validate([
            'clinic_name' => 'required|string|min:5|max:120',
            'description' => 'required|string|min:30',
            'doctors' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        $getMime = $request->file('image')->getMimeType(); 
        $explodedMime = explode('/' ,$getMime);
        $mime = end($explodedMime);
        $name = Str::random(40) . '.' . $mime;
        $request->image->move('img', $name);

        $clinic = new Clinic();
    
        $clinic->clinic_name = $request->clinic_name;
        $clinic->description = $request->description;
        $clinic->image = ('/img/'. $name);
        $doctors = '';

        foreach($request->doctors as $doctor){
            $doctors .= $doctor . '|'; 
        }

        $clinic->doctors = $doctors;
        $clinic->save();
        return redirect('/admin/clinic')->with('success', 'Data berhasil ditambahkan');
    }
}
