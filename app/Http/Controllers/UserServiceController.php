<?php

namespace App\Http\Controllers;

use App\Models\Facility;
use App\Models\Service;
use Illuminate\Http\Request;

class UserServiceController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $services = Service::paginate(4);
        $facilities = Facility::select('*')->get();

        

        return view('layanan', [
            'services' => $services,
            'facilities' => $facilities
        ]);
    }
}
