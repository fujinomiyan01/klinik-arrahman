<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EditArticleController extends Controller
{
    // username, title, detail, category_name

    public function edit($id){ 
        $articles = Article::select('*')->where('id', $id)->first();
        $categories = Category::select('*')->get();
        return view('admin.editArticle', compact('articles', 'categories'));
    }

    public function update(Request $request){
        $article = Article::select('*')->where('id', $request->id)->first();

        if($request->user_id){
            $request->validate([
                'user_id' => 'numeric',
            ]);
            $admin = User::select('*')->where('id', $request->user_id)->first();
            $article->username = $admin->name;
        }

        if($request->title){
            $request->validate([
                'title' => 'string|max:150',
            ]);
            $article->title = $request->title;
        }

        if($request->image){
            $request->validate([
                'image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);

            $getMime = $request->file('image')->getMimeType(); 
            $explodedMime = explode('/' ,$getMime);
            $mime = end($explodedMime);
            $name = Str::random(40) . '.' . $mime;
            $request->image->move('img', $name);
    
            $article->image = ('/img/' . $name);
        }

        if($request->detail){
            $request->validate([
                'detail' => 'string|min:30',
            ]);
            $article->detail = $request->detail;
        }

        if($request->category_name){
            $request->validate([
                'category_name' => 'min:1',
            ]);
            $category_name = '';

            foreach($request->category_name as $name){
                $category_name .= $name . ','; 
            }

            $article->category_name = $category_name;
        }

        $article->save();
        return redirect('/admin/article')->with('success', 'Update berhasil');
    }
}
