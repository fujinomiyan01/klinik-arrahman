<?php

namespace App\Http\Controllers;

class FacilityController extends Controller
{
    public function __invoke(){
        return view('admin.facility');
    }
}
