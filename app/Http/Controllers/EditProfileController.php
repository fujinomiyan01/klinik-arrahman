<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EditProfileController extends Controller
{
    public function edit(){ 
        // $profile = Profile::select('*')->where('id', $id)->first();
        $profiles = Profile::select('*')->first();
        $doctors = Doctor::select('*')->get();
        return view('admin.editProfile', compact('profiles', 'doctors'));
    }

    public function update(Request $request){
        $profiles = Profile::select('*')->first();
        // $profile = Profile::select('*')->where('id', $request->id)->first();

        if($request->clinic_name){
            $request->validate([
                'clinic_name' => 'string',
            ]);
            $profiles->clinic_name = $request->clinic_name;
        }

        if($request->description){
            $request->validate([
                'description' => 'string',
            ]);
            $profiles->description = $request->description;
        }

        if($request->doctors){
            $doctors = '';

            foreach($request->doctors as $doctor){
                $doctors .= $doctor . '|'; 
            }

            $profiles->doctors = $doctors;
        }

        if($request->vision){
            $request->validate([
                'vision' => 'string|min:20',
            ]);
            $profiles->vision = $request->vision;
        }

        if($request->mission){
            $request->validate([
                'mission' => 'string|min:20',
            ]);
            $profiles->mission = $request->mission;
        }

        if($request->facebook_name){
            $request->validate([
                'facebook_name' => 'string',
            ]);
            $profiles->facebook_name = $request->facebook_name;
        }

        if($request->facebook_link){
            $request->validate([
                'facebook_link' => 'string',
            ]);
            $profiles->facebook_link = $request->facebook_link;
        }

        if($request->instagram_name){
            $request->validate([
                'instagram_name' => 'string',
            ]);
            $profiles->instagram_name = $request->instagram_name;
        }

        if($request->instagram_link){
            $request->validate([
                'instagram_link' => 'string',
            ]);
            $profiles->instagram_link = $request->instagram_link;
        }

        if($request->twitter_name){
            $request->validate([
                'twitter_name' => 'string',
            ]);
            $profiles->twitter_name = $request->twitter_name;
        }

        if($request->twitter_link){
            $request->validate([
                'twitter_link' => 'string',
            ]);
            $profiles->twitter_link = $request->twitter_link;
        }

        if($request->address){
            $request->validate([
                'address' => 'string|min:20',
            ]);
            $profiles->address = $request->address;
        }

        if($request->phone1){
            $request->validate([
                'phone1' => 'numeric|min:10',
            ]);
            $profiles->phone = $request->phone1 . " " . $request->phone3 . " " . $request->phone2;
        }

        if($request->phone2){
            $request->validate([
                'phone2' => 'numeric|min:10',
            ]);
            $profiles->phone = $request->phone1 . " " . $request->phone3 . " " . $request->phone2;
        }

        if($request->email){
            $request->validate([
                'email' => 'email',
            ]);
            $profiles->email = $request->email;
        }

        if($request->work_days1){
            $request->validate([
                'work_days1' => 'alpha',
            ]);
            $profiles->work_days = $request->work_days1 . " " . $request->work_days3 . " " . $request->work_days2;
        }

        if($request->work_days2){
            $request->validate([
                'work_days2' => 'alpha',
            ]);
            $profiles->work_days = $request->work_days1 . " " . $request->work_days3 . " " . $request->work_days2;
        }

        if($request->work_time1){
            $request->validate([
                'work_time1' => 'numeric',
            ]);
            $profiles->work_time = $request->work_time1 . " " . $request->work_time3 . " " . $request->work_time2;
        }

        if($request->work_time2){
            $request->validate([
                'work_time2' => 'numeric',
            ]);
            $profiles->work_time = $request->work_time1 . " " . $request->work_time3 . " " . $request->work_time2;
        }

        if($request->image){
            $request->validate([
                'image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            
            $getMime = $request->file('image')->getMimeType(); 
            $explodedMime = explode('/' ,$getMime);
            $mime = end($explodedMime);
            $name = Str::random(40) . '.' . $mime;
            $request->image->move('img', $name);

            $profiles->image = ('/img/' . $name);
        }

        $profiles->save();
        return back()->with('success', 'Update berhasil');
    }
}
