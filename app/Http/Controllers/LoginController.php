<?php

namespace App\Http\Controllers;

use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public function create(){
        return view('admin.login');
    }

    public function store(Request $request){
        $attributes = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if(Auth::attempt($attributes)){
            return redirect(RouteServiceProvider::HOME)->with('success', 'Login Sukses');
        }

        throw ValidationException::withMessages([
            'email' => 'Email atau password salah!',
            'password' => 'Email atau password salah!',
        ]);
    
    }
}
