<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Gallery;
use Illuminate\Support\Str;

class AddGalleryController extends Controller
{
    public function create(){
        return view('admin.add-gallery');
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required|string',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);
    
        $getMime = $request->file('image')->getMimeType(); 
        $explodedMime = explode('/' ,$getMime);
        $mime = end($explodedMime);
        $name = Str::random(40) . '.' . $mime;
        $request->image->move('img', $name);

        $save = new Gallery();

        $save->name = $request->name;
        $save->image = ('/img/'. $name);

        $save->save();
        return redirect('/admin/gallery')->with('success', 'Data Berhasil Ditambahkan');
    }
}
