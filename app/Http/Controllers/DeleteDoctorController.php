<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use Illuminate\Http\Request;

class DeleteDoctorController extends Controller
{
    public function __invoke($id){
        $facility = Doctor::find($id);

        $facility->delete();

        return back()->with('delete', 'Delete berhasil');
    }
}
