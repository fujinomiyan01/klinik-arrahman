<?php

namespace App\Http\Controllers;

use App\Models\ArticleComments;
use Illuminate\Http\Request;

class DeleteArticleCommentsController extends Controller
{
    public function __invoke($id){
        $articleComments = ArticleComments::find($id);

        $articleComments->delete();

        return back()->with('delete', 'Delete berhasil');
    }
}
