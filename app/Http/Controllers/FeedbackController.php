<?php

namespace App\Http\Controllers;

class FeedbackController extends Controller
{
    public function __invoke(){
        return view('admin.feedback');
    }
}
