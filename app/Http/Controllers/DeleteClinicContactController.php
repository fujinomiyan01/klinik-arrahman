<?php

namespace App\Http\Controllers;

use App\Models\ClinicContact;
use Illuminate\Http\Request;

class DeleteClinicContactController extends Controller
{
    public function __invoke($id){
        $clinicContact = ClinicContact::find($id);

        $clinicContact->delete();

        return back()->with('delete', 'Delete berhasil');
    }
}
