<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use Illuminate\Http\Request;

class AddFeedbackController extends Controller
{
    public function create(){
        return view('admin.add-feedback');
    }

    public function store(Request $request){
        $request->validate([
            'patient' => 'required|string',
            'message' => 'required|string|min:10',
            'message' => 'required',
        ]);

        $feedback = new Feedback();

        $feedback->patient = $request->patient;
        $feedback->message = $request->message;
        $feedback->rate = $request->rate;

        $feedback->save();
        return redirect('/admin/feedback');
    }
}
