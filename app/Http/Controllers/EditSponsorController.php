<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sponsor;
use Illuminate\Support\Str;

class EditSponsorController extends Controller
{
    public function edit($id){ 
        $sponsors = Sponsor::select('*')->where('id', $id)->first();
        return view('admin.editSponsor', compact('sponsors'));
    }

    public function update(Request $request){
        $sponsor = Sponsor::select('*')->where('id', $request->id)->first();
        
        $request->validate([
            'image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);
        
        $getMime = $request->file('image')->getMimeType(); 
        $explodedMime = explode('/' ,$getMime);
        $mime = end($explodedMime);
        $name = Str::random(40) . '.' . $mime;
        $request->image->move('img', $name);

        $sponsor->image = ('/img/' . $name);
        
        
        $sponsor->save();
        return redirect('/admin/sponsor')->with('success', 'Update berhasil');
    }
}
