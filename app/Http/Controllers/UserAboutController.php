<?php

namespace App\Http\Controllers;

use App\Models\Clinic;
use App\Models\Doctor;
use App\Models\Facility;
use App\Models\Profile;
use App\Models\Service;
use Illuminate\Http\Request;

class UserAboutController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $profiles = Profile::select('*')->get();
        $clinics = Clinic::select('*')->get();
        $doctors = Doctor::select('*')->get();
        $services = Service::select('*')->get();
        $facilities = Facility::select('*')->get();

        
        return view('about', [
            'profiles' => $profiles,
            'clinics' => $clinics,
            'doctors' => $doctors,
            'services' => $services,
            'facilities' => $facilities
        ]);
    }

  
}
