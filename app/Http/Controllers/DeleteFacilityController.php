<?php

namespace App\Http\Controllers;

use App\Models\Facility;
use Illuminate\Http\Request;

class DeleteFacilityController extends Controller
{
    public function __invoke($id){
        $facility = Facility::find($id);

        $facility->delete();

        return back()->with('delete', 'Delete berhasil');
    }
}
