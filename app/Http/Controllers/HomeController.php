<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use App\Models\Facility;
use App\Models\Feedback;
use App\Models\Service;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __invoke () {
        $feedbacks = Feedback::select('*')->get();
        $facilities = Facility::select('*')->get();
        $services = Service::select('*')->get();
        $doctors = Doctor::select('*')->get();

        // $facility =
        return view('dashboard', [
            'feedbacks' => $feedbacks,
            'facility' => $facilities,
            'services' => $services,
            'doctors' => $doctors
            

        ]);
    }

}
