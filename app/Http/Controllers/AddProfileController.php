<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;

class AddProfileController extends Controller
{
    public function create(){
        return view('admin.profile');
    }

    public function store(Request $request){
        $request->validate([
            'clinic_name' => 'required|string',
            'clinics' => 'required|string',
            'vision' => 'required|string|min:20',
            'mission' => 'required|string|min:20',
            'address' => 'required|string|min:20',
            'googleMap_link' => 'required',
            'phone' => 'required|numeric|min:10',
            'work_time1' => 'required|numeric',
            'work_time3' => 'required|numeric',
        ]);
        $save = new Profile();
    
        $save->clinic_name = $request->clinic_name;
        $save->clinics = $request->clinics;
        $save->vision = $request->vision;
        $save->mission = $request->mission;

        if($request->facebook){
            $save->facebook = $request->facebook;
        }

        if($request->instagram){
            $save->instagram = $request->instagram;
        }

        if($request->twitter){
            $save->twitter = $request->twitter;
        }
        
        $save->address = $request->address;
        $save->googleMap_link = $request->googleMap_link;
        $save->phone = $request->phone;
        $save->work_time = $request->work_time1 . " " . $request->work_time2 . " " .  $request->work_time3;
    
        $save->save();
        return redirect('/admin/profile')->with('success', 'Data berhasil ditambahkan');
    }
}
