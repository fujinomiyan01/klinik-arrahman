<?php

namespace App\Http\Controllers;

use App\Models\HeroContent;
use Illuminate\Http\Request;

class DeleteHeroContentController extends Controller
{
    public function __invoke($id){
        $heroContent = HeroContent::find($id);
        $heroContent->delete();

        return back()->with('delete', 'Delete Berhasil');
    }
}
