<?php

namespace App\Http\Controllers;

use App\Models\HeroImage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AddHeroImageController extends Controller
{
    public function create(){
        return view('admin.add-heroImage');
    }

    public function store(Request $request){
        $request->validate([
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        $getMime = $request->file('image')->getMimeType(); 
        $explodedMime = explode('/' ,$getMime);
        $mime = end($explodedMime);
        $name = Str::random(40) . '.' . $mime;
        $request->image->move('img', $name);

        $heroImage = new HeroImage();

        $heroImage->image = ('/img/'. $name);

        $heroImage->save();
        return redirect('/admin/heroImage');
    }
}
