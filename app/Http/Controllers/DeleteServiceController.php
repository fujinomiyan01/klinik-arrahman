<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;

class DeleteServiceController extends Controller
{
    public function __invoke($id){
        $facility = Service::find($id);

        $facility->delete();

        return back()->with('delete', 'Delete berhasil');
    }
}
