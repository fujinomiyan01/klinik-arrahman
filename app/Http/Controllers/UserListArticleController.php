<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ArticleComments;
use App\Models\Category;
use App\Models\Facility;
use App\Models\Service;
use Illuminate\Http\Request;

class UserListArticleController extends Controller
{
    public function __invoke(Request $request)
    {
        $search = request()->query('search', '');
        $articleComments = ArticleComments::select('*')->get();
        
        
        $articles = Article::when($search, function($query) use ($search) {
                $query->where('title', 'LIKE','%'.$search.'%');
            })
            ->paginate(9);
           

        $categories = Category::select('*')->get();
        $services = Service::select('*')->get();
        $facilities = Facility::get();
        $popularArticles = Article::select('*')->limit(3)->orderBy('views', 'DESC')->get(); 


        return view('list-artikel', [
            'articles' => $articles,
            'categories' => $categories,
            'services' => $services,
            'facilities' => $facilities,
            'articleComments' => $articleComments,
            'popularArticles' => $popularArticles
        ]);
    }
}
