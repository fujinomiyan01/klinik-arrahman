<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use Illuminate\Http\Request;

class UserAddFeedbackController extends Controller
{
    public function create(){
        return view('user-add-feedback');
    }

    public function store(Request $request){
        $request->validate([
            'patient' => 'required|string',
            'message' => 'required|string|min:10',
            'message' => 'required',
        ]);

        $feedback = new Feedback();

        $feedback->patient = $request->patient;
        $feedback->message = $request->message;
        $feedback->rate = $request->rate;

        $feedback->save();
        return redirect('/#testimoni');
    }
}
