<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ArticleComments;
use Illuminate\Http\Request;

class AddArticleCommentsController extends Controller
{
    public function create(){
        $article_titles = Article::select('title')->get();
        return view('admin.add-articleComments', compact('article_titles'));
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required|string',
            'article_title' => 'required|string',
            'email' => 'required|email',
            'message' => 'required|string|min:3',
            'g-recaptcha-response' => 'required|captcha',
        ]);

        $articleComments = new ArticleComments();
        $userComments = ArticleComments::get();
        $jumlahKomentar = count($userComments);

        $articleComments->current_index = $jumlahKomentar;
        $articleComments->article_title = $request->article_title;
        $articleComments->name = $request->name;
        $articleComments->email = $request->email;
        $articleComments->comment_status = $request->comment_status;
        $articleComments->message = $request->message;

        if($request->comment_status == 'active') {
            Article::where('title', $request->article_title)->increment('comments', 1);
        }
        
        $articleComments->save();

        return redirect('admin/articleComments/')->with('success', 'Komentar berhasil ditambahkan');
    }
}
