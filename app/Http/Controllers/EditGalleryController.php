<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EditGalleryController extends Controller
{
    public function edit($id){ 
        $galleries = Gallery::select('*')->where('id', $id)->first();
        return view('admin.editGallery', compact('galleries'));
    }

    public function update(Request $request){
        $gallery = Gallery::select('*')->where('id', $request->id)->first();

        if($request->name){
            $request->validate([
                'name' => 'string',
            ]);
            $gallery->name = $request->name;
        }

        if($request->image){
            $request->validate([
                'image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            
            $getMime = $request->file('image')->getMimeType(); 
            $explodedMime = explode('/' ,$getMime);
            $mime = end($explodedMime);
            $name = Str::random(40) . '.' . $mime;
            $request->image->move('img', $name);

            $gallery->image = ('/img/' . $name);
        }

        $gallery->save();
        return redirect('/admin/gallery')->with('success', 'Update berhasil');
    }
}
