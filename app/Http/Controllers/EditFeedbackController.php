<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use Illuminate\Http\Request;

class EditFeedbackController extends Controller
{
    public function edit($id){ 
        $feedbacks = Feedback::select('*')->where('id', $id)->first();
        return view('admin.editFeedback', compact('feedbacks'));
    }

    public function update(Request $request){
        $feedback = Feedback::select('*')->where('id', $request->id)->first();

        if($request->patient){
            $request->validate([
                'patient' => 'required|string',
            ]);
            $feedback->patient = $request->patient;
        }

        if($request->message){
            $request->validate([
                'message' => 'required|string|min:10',
            ]);
            $feedback->message = $request->message;
        }

        if($request->rate){
            $feedback->rate = $request->rate;
        }

        $feedback->save();
        return redirect('/admin/feedback')->with('success', 'Update berhasil');
    }
}
