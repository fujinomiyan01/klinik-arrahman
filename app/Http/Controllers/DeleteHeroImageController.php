<?php

namespace App\Http\Controllers;

use App\Models\HeroImage;
use Illuminate\Http\Request;

class DeleteHeroImageController extends Controller
{
    public function __invoke($id){
        $heroImage = HeroImage::find($id);

        $heroImage->delete();

        return back()->with('delete', 'Delete berhasil');
    }
}
