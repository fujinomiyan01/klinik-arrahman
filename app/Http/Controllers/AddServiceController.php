<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AddServiceController extends Controller
{
    public function create(){
        return view('admin.add-service');
    }

    public function store(Request $request){

        // ensure the request has a file before we attempt anything else.
        $request->validate([
            'service_name' => 'required|string',
            'mini_detail' => 'required|string|max:50',
            'detail' => 'required|string|min:30',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);
    
        $getMime = $request->file('image')->getMimeType(); 
        $explodedMime = explode('/' ,$getMime);
        $mime = end($explodedMime);
        $name = Str::random(40) . '.' . $mime;
        $request->image->move('img', $name);
    
        $save = new Service();
    
        $save->service_name = $request->service_name;
        $save->route = str_replace(' ', '_', $request->service_name);
        $save->mini_detail = $request->mini_detail;

        $save->detail = $request->detail;
        $save->image = ('/img/'. $name);
    
        $save->save();
        return redirect('/admin/service')->with('success', 'Data Berhasil Ditambahkan');
    }
}
