<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function create(){
        return view('admin.profile');
    }

    public function store(Request $request){
        $request->validate([
            'clinic_id' => 'required',
            'clinic_name' => 'required|string',
            'vision' => 'required|string|min:20',
            'mission' => 'required|string|min:20',
            'address' => 'required|string|min:20',
            'phone' => 'required|numeric|min:10',
            'work_time' => 'required|numeric',
        ]);
        $save = new Profile();
    
        $save->clinic_id = $request->clinic_id;
        $save->clinic_name = $request->clinic_name;
        $save->vision = $request->vision;
        $save->mission = $request->mission;
        $save->address = $request->address;
        $save->phone = $request->phone;
        $save->work_time = $request->wk1 . " " . $request->wk2 . " " .  $request->wk3;
    
        $save->save();
        return back()->with('success', 'Data berhasil ditambahkan');
    }
}
