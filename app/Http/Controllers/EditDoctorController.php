<?php

namespace App\Http\Controllers;

use App\Models\Clinic;
use App\Models\Doctor;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EditDoctorController extends Controller
{
    public function edit($id){ 
        $doctors = Doctor::select('*')->where('id', $id)->first();
        $clinics = Clinic::get();
        
        return view('admin.editDoctor', compact('doctors', 'clinics'));
    }

    public function update(Request $request){
        $doctor = Doctor::select('*')->where('id', $request->id)->first();

        if($request->doctor_name){
            $request->validate([
                'doctor_name' => 'string',
            ]);
            $doctor->doctor_name = $request->doctor_name;
        }

        if($request->role){
            $request->validate([
                'role' => 'string',
            ]);
            $doctor->role = $request->role;
        }

        if($request->workplace){
            $detail = '';

            foreach($request->workplace as $item){
                $detail .= $item . '&'; 
            }

            $doctor->detail = $detail;
        }

        if($request->image){
            $request->validate([
                'image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            
            $getMime = $request->file('image')->getMimeType(); 
            $explodedMime = explode('/' ,$getMime);
            $mime = end($explodedMime);
            $name = Str::random(40) . '.' . $mime;
            $request->image->move('img', $name);

            $doctor->image = ('/img/' . $name);
        }

        $doctor->save();
        return redirect('/admin/doctor')->with('success', 'Update berhasil');
    }
}
