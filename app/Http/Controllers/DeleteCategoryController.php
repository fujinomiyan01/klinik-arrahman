<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class DeleteCategoryController extends Controller
{
    public function __invoke($id){
        $facility = Category::find($id);

        $facility->delete();

        return back()->with('delete', 'Delete berhasil');
    }
}
