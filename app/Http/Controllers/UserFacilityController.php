<?php

namespace App\Http\Controllers;

use App\Models\Facility;
use App\Models\Service;
use Illuminate\Http\Request;

class UserFacilityController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $facilities = Facility::paginate(4);
        $services = Service::select('*')->get();

        return view('fasilitas', [
            'facilities' => $facilities,
            'services' => $services
        ]);
    }
}
