<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EditCategoryController extends Controller
{
    public function edit($id){ 
        $categories = Category::select('*')->where('id', $id)->first();
        return view('admin.editCategory', compact('categories'));
    }

    public function update(Request $request){
        $request->validate([
            'category_name' => 'required|string|min:2|max:100',
        ]);

        $category = Category::select('*')->where('id', $request->id)->first();

        $category->category_name = Str::ucfirst($request->category_name);

        $category->save();
        return redirect('/admin/category')->with('success', 'Update berhasil');
    }
}
