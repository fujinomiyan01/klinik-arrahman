<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class DeleteArticleController extends Controller
{
    public function __invoke($id){
        $article = Article::find($id);

        $article->delete();

        return back()->with('delete', 'Delete berhasil');
    }
}
