<?php

namespace App\Http\Controllers;

use App\Models\Clinic;
use App\Models\Doctor;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class AddDoctorController extends Controller
{
    public function create(){
        // $doctors = Doctor::join('doctor_clinics', 'doctor_clinics.id', '=', 'doctors.id')
        // ->get(['doctors.id', 'doctors.doctor_name', 'doctors.role', 'doctors.detail', 'doctors.image', 'doctor_clinics.doctor_id']);
        $clinics = Clinic::get();
        return view('admin.add-doctor', compact('clinics'));
    }

    public function store(Request $request){
        $request->validate([
            'doctor_name' => 'required|string',
            'role' => 'required|string',
            'workplace' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);
    
        $getMime = $request->file('image')->getMimeType(); 
        $explodedMime = explode('/' ,$getMime);
        $mime = end($explodedMime);
        $name = Str::random(40) . '.' . $mime;
        $request->image->move('img', $name);
        
        $save = new Doctor();

        $save->doctor_name = $request->doctor_name;
        $save->role = $request->role;
        $save->image = ('/img/'. $name);
        // $detailLength = count($request->detail);
        $detail = '';

        foreach($request->workplace as $item){
            $detail .= $item . '&'; 
        }
        $save->detail = $detail;

        $save->save();
        return redirect('/admin/doctor')->with('success', 'Data berhasil ditambahkan');
    }
}
