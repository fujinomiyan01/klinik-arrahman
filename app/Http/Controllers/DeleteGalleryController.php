<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use Illuminate\Http\Request;

class DeleteGalleryController extends Controller
{
    public function __invoke($id){
        $facility = Gallery::find($id);

        $facility->delete();

        return back()->with('delete', 'Delete berhasil');
    }
}
