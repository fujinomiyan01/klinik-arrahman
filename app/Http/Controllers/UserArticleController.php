<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ArticleComments;
use Illuminate\Http\Request;

class UserArticleController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create($title)
    {
        // $articles = Article::select('*')->where('id', $id)->first();
        Article::where('title', $title)->increment('views', 1);

        $articles = Article::select('*')->where('title', $title)->first();
        $articleComments = ArticleComments::select('*')->where('article_title', $title)->get();
        $activeArticleComments = ArticleComments::select('*')->where('article_title', $title)
        ->where('comment_status', 'active')->get();


        $popularArticles = Article::select('*')->limit(3)->orderBy('views', 'DESC')->get(); 

        return view('artikel', compact('articles', 'articleComments', 'popularArticles', 'activeArticleComments'));
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|email|min:10',
            'message' => 'required|string|min:3',
            'g-recaptcha-response' => 'required|captcha',
        ]);
        
        Article::where('title', $request->judul)->increment('comments', 1);
        ArticleComments::where('article_title', $request->judul)->increment('current_index', 1);

        $articleComments = new ArticleComments();
        $userComments = ArticleComments::get();
        $jumlahKomentar = count($userComments);

        $articleComments->current_index = $jumlahKomentar;

        $articleComments->article_title = $request->judul;
        $articleComments->name = $request->name;
        $articleComments->email = $request->email;
        $articleComments->message = $request->message;
        $articleComments->comment_status = 'active';

        $articleComments->save();

        // return redirect('/admin/articleComments')->with('success', 'Komentar berhasil ditambahkan')->with('viewBaru', 'User telah menambahkan komentar pada artikel '. $request->judul .'. Mohon refresh halaman')->with('judul', $request->judul);
        return back()->with('success', 'Komentar berhasil ditambahkan');
    }
}
