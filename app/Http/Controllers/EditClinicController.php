<?php

namespace App\Http\Controllers;

use App\Models\Clinic;
use App\Models\Doctor;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EditClinicController extends Controller
{
    public function edit($id){ 
        $clinics = Clinic::select('*')->where('id', $id)->first();
        $doctors = Doctor::select('*')->get();
        return view('admin.editClinic', compact('clinics', 'doctors'));
    }

    public function update(Request $request){
        $clinic = Clinic::select('*')->where('id', $request->id)->first();

        if($request->clinic_name){
            $request->validate([
                'clinic_name' => 'string|min:5|max:120',
            ]);
            $clinic->clinic_name = $request->clinic_name;
        } 
        
        if($request->description){
            $request->validate([
                'description' => 'string|min:30',
            ]);
            $clinic->description = $request->description;
        } 

        if($request->doctors){
            $doctors = '';

            foreach($request->doctors as $doctor){
                $doctors .= $doctor . '|'; 
            }

            $clinic->doctors = $doctors;
        }
        
        if($request->image){
            $request->validate([
                'image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            
            $getMime = $request->file('image')->getMimeType(); 
            $explodedMime = explode('/' ,$getMime);
            $mime = end($explodedMime);
            $name = Str::random(40) . '.' . $mime;
            $request->image->move('img', $name);

            $clinic->image = ('/img/' . $name);
        }

        $clinic->save();
        return redirect('/admin/clinic')->with('success', 'Update berhasil');
    }
}
