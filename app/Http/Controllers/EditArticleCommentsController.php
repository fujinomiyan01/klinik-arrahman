<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ArticleComments;
use Illuminate\Http\Request;

class EditArticleCommentsController extends Controller
{
    public function edit($id){
        $article_comment = ArticleComments::where('id', $id)->first();
        $article_titles = Article::select('title')->get();
        return view('admin.editArticleComments', compact('article_comment', 'article_titles'));
    }

    public function update(Request $request){
        $request->validate([
            'g-recaptcha-response' => 'required|captcha',
        ]);
        $article_comment = ArticleComments::select('*')->where('id', $request->id)->first();

        if($request->article_title){
            $request->validate([
                'article_title' => 'string',
            ]);
            $article_comment->article_title = $request->article_title;
        } 
        
        if($request->name){
            $request->validate([
                'name' => 'string',
            ]);
            $article_comment->name = $request->name;
        }

        if($request->email){
            $request->validate([
                'email' => 'email',
            ]);
            $article_comment->email = $request->email;
        }

        if($request->message){
            $request->validate([
                'message' => 'string|min:3',
            ]);
            $article_comment->message = $request->message;
        }

        if($request->comment_status){
            $request->validate([
                'comment_status' => 'string',

            ]);

            if($request->comment_status == 'hidden') {
                Article::where('title', $request->article_title)->decrement('comments', 1);
            }

            Article::where('title', $request->article_title)->increment('comments', 1);
            $article_comment->comment_status = $request->comment_status;
        }

        $article_comment->save();
        return redirect('/admin/articleComments')->with('success', 'Update berhasil');
    }
}
