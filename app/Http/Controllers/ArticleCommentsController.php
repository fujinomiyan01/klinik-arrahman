<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ArticleComments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArticleCommentsController extends Controller
{
    // public function __construct(){
    //     $this->middleware('guest');
    //     if(!Auth::check()){
    //         return back();
    //     }
    // }

    public function __invoke(){
        $articleComments = ArticleComments::get();
        if(count($articleComments) > 1){
            $lastComment = ArticleComments::all()->last()->current_index;
        } else {
            $lastComment = 0;
        }
        return view('admin.articleComments', compact('articleComments', 'lastComment'));
    }
}
