<?php

namespace App\Http\Controllers;

use App\Models\HeroContent;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EditHeroContentController extends Controller
{
    public function edit(){
        $heroContentEdit = HeroContent::select('*')->first();
        return view('admin.editHeroContent', compact('heroContentEdit'));
    }

    public function update(Request $request){
        // $heroContent = HeroContent::select('*')->where('id', $request->id)->first();
        $heroContent = HeroContent::select('*')->first();

        if($request->title){
            $request->validate([
                'title' => 'string|min:6',
            ]);
            $heroContent->title = $request->title;
        }

        if($request->detail){
            $request->validate([
                'detail' => 'string|min:15',
            ]);
            $heroContent->detail = $request->detail;
        }

        // if($request->image){
        //     $request->validate([
        //         'image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        //     ]);
            
        //     $getMime = $request->file('image')->getMimeType(); 
        //     $explodedMime = explode('/' ,$getMime);
        //     $mime = end($explodedMime);
        //     $name = Str::random(40) . '.' . $mime;
        //     $request->image->move('img', $name);

        //     $heroContent->image = ('/img/' . $name);
        // }

        $heroContent->save();
        return redirect('/admin/heroContent')->with('success', 'Update berhasil');
    }
}
