<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class FeedbackFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
                'message' => 'Dokternya enak diajak konsul, alhamdulillah walau saya pasien BPJS tidak membedakan dengan pasien umum yg bayar',
                'rate' => 5,
                'patient' => 'Rinawati',
            // ],

            // [
            //     'message' => 'Sangat puas dengan pelayanannya',
            //     'rate' => 5,
            //     'patient' => 'Riani',
            // ],
            // [
            //     'message' => 'Pelayanan di klinik Ar-Rahman memuaskan dari mulai pelayanan pendaftaran yang baik. Pemeriksaan oleh dokter sangat baik karena asik untuk diajak berbicara mengenai tentang hal apa yang dirasakan saat sakit, solusi nya lebih tepat.',
            //     'rate' => 5,
            //     'patient' => 'Indri Khoriati',
            // ],
            // [
            //     'message' => 'Pelayanan ramah, dokternya bagus',
            //     'rate' => 4,
            //     'patient' => 'Khaerul Amri',
            // ],
            // [
            //     'message' => 'Pelayanan baik, petugas ramah',
            //     'rate' => 4,
            //     'patient' => 'Andri Budiono',
            // ],
        ];
    }
}
