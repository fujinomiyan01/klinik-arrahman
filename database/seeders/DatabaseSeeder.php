<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use function PHPSTORM_META\map;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin Fadhil',
            'username' => 'fadhil01',
            'email' => 'admin01@gmail.com',
            'password' => bcrypt('unpredictable'),
        ]);

        DB::table('users')->insert([
            'name' => 'Admin Fahmi',
            'username' => 'fahmi',
            'email' => 'fahmiramdhan21@gmail.com',
            'password' => bcrypt('password'),
        ]);
        
        // \App\Models\User::factory(10)->create();
        DB::table('feedback')->insert( 
            [
                'message' => 'Dokternya enak diajak konsul, alhamdulillah walau saya pasien BPJS tidak membedakan dengan pasien umum yg bayar',
                'rate' => 5,
                'patient' => 'Rinawati',
            ],
        );
        
        DB::table('feedback')->insert(
            [
                'message' => 'Sangat puas dengan pelayanannya',
                'rate' => 5,
                'patient' => 'Riani',
            ],
        );
        
        DB::table('feedback')->insert(
            [
                'message' => 'Pelayanan di klinik Ar-Rahman memuaskan dari mulai pelayanan pendaftaran yang baik. Pemeriksaan oleh dokter sangat baik karena asik untuk diajak berbicara mengenai tentang hal apa yang dirasakan saat sakit, solusi nya lebih tepat.',
                'rate' => 5,
                'patient' => 'Indri Khoriati',
            ],
        );
            
        DB::table('feedback')->insert(
            [
                'message' => 'Pelayanan ramah, dokternya bagus',
                'rate' => 4,
                'patient' => 'Khaerul Amri',
            ],
        );    
        
        DB::table('feedback')->insert(
            [
                'message' => 'Pelayanan baik, petugas ramah',
                'rate' => 4,
                'patient' => 'Andri Budiono',
            ],
        );

        DB::table('clinics')->insert([
            'clinic_name' => 'Klinik Ar-Rahman Galuh Mas',
            'description' => 'Klinik Ar-rahman Galuh Mas merupakan klinik kedua di daerah galuh mas dan perumnas. Didirikan Bulan februari 2019.',
            'doctors' => 'dr. Sigit Aryanto| dr. Dwi Susilo SH,MH| dr. Chintia Nilna Muna| drg. Widi Astuti',
            'image' => '/img/klinikgaluh.jpg',
        ]);

        // DB::table('clinics')->insert([
        //     'clinic_name' => 'Klinik Ar-Rahman Medika',
        //     'description' => 'Klinik Ar-rahman Medika berdiri 2 Januari 2014 di lokasi strategis padat penduduk di Teluk Jambe Timur.',
        //     'doctors' => 'dr. Sigit Aryanto| dr. Dwi Susilo SH,MH| dr. Chintia Nilna Muna| drg. Widi Astuti| dr. Ayu Delvira Putri| dr. Onelola Dewita Karya, MARS| drg. Rori Sasmita',
        //     'image' => '/img/klinikgaluh.jpg',
        // ]);

        DB::table('doctors')->insert([
            'doctor_name' => 'dr. Sigit Aryanto',
            'role' => 'Ketua Yayasan Ar-Rahman Medika',
            'detail' => 'Klinik Ar-Rahman Medika & Klinik Ar-Rahman Galuh Mas',
            'image' => '/img/jlOUMNKgf7WduJ7L8uM2tyWSjU3TzYxglwUH4BSf.jpeg',
        ]);

        DB::table('doctors')->insert([
            'doctor_name' => 'dr. Dwi Susilo SH,MH',
            'role' => 'Dokter Penanggung Jawab',
            'detail' => 'Klinik Ar-Rahman Medika & Klinik Ar-Rahman Galuh Mas',
            'image' => '/img/nybFm9ngJdQkI9jLw5DaAbA5Qh20jXUwvXTXwkWy.jpeg',
        ]);

        DB::table('doctors')->insert([
            'doctor_name' => 'dr. Chintia Nilna Munadr',
            'role' => 'Dokter Umum',
            'detail' => 'Klinik Ar-Rahman Medika & Klinik Ar-Rahman Galuh Mas',
            'image' => '/img/KAArlZhUr5yiphwcDa0TQDNPI65IjnbUbzARxA1M.jpeg',
        ]);

        DB::table('doctors')->insert([
            'doctor_name' => 'dr. Ayu Delvira Putri',
            'role' => 'Dokter Umum',
            'detail' => 'Klinik Ar-Rahman Medika',
            'image' => '/img/NkDnLzdukDf4lIPEFi3Xz9r8nULjd5StEXlmdeFt.jpeg',
        ]);

        DB::table('doctors')->insert([
            'doctor_name' => 'dr. Onelola Dewita Karya, MARS',
            'role' => 'Dokter Umum',
            'detail' => 'Klinik Ar-Rahman Medika',
            'image' => '/img/Ml9rF9ZFwzACH3aNVb2ogPrh92Jy26AAkcuEqhcJ.jpeg',
        ]);

        DB::table('doctors')->insert([
            'doctor_name' => 'drg. Rori Sasmita',
            'role' => 'Dokter Gigi',
            'detail' => 'Klinik Ar-Rahman Medika',
            'image' => '/img/Ue1ltDxwt6oatDgo1XHaUUE98kMSDTfQwMB3iaoH.jpeg',
        ]);

        DB::table('doctors')->insert([
            'doctor_name' => 'drg. Widi Astuti',
            'role' => 'Dokter Gigi',
            'detail' => 'Klinik Ar-Rahman Medika & Klinik Ar-Rahman Galuh Mas',
            'image' => '/img/0896883bdfa124ecc282338eced85346.jpg',
        ]);

        DB::table('sponsors')->insert([
            'image' => '/img/QKIiqZwuwnOOhgZ5KrJ7AhS7uw0XBM9pZKHoXRdC.png',
        ]);

        DB::table('sponsors')->insert([
            'image' => '/img/SeJQ6htMqGVTwa9yimZoivGbvsBFunGPdU6sYtkx.png',
        ]);

        DB::table('sponsors')->insert([
            'image' => '/img/Z2xrlGXjRYPUSX12NtEARuiEgvktAE4c3HydfSPB.png',
        ]);

        DB::table('sponsors')->insert([
            'image' => '/img/ZemYtPumNE0WC8fn3m2IlR4m9HKhdFPPlgiIjgUL.png',
        ]);

        DB::table('sponsors')->insert([
            'image' => '/img/ahEpuWwBMrHP9dgBJEG7pfjBPa0Ty5RA9JNUZx9g.png',
        ]);

        DB::table('facilities')->insert([
            'facility_name' => 'Farmasi',
            'mini_detail' => 'Farmasi dengan sumber daya manusia yang tentunya profesional dan obat yang lengkap',
            'detail' => 'Selain fasilitas yang lengkap dan tenaga medis yang profesional, Klinik Ar-Rahman pun dilengkapi bagian Farmasi dengan sumber daya manusia yang tentunya profesional dan obat yang lengkap serta terjamin mutu dan kualitasnya.',
            'route' => 'Farmasi',
            'image' => '/img/a04JOGXUOWJAyQ65WJCvdQDRfhR0tHfEHDu4cFit.jpeg'
        ]);

        DB::table('facilities')->insert([
            'facility_name' => 'Mushola',
            'mini_detail' => 'Mushola yang nyaman untuk ibadah ketika masuk waktu shalat bagi siapapun',
            'detail' => 'Untuk memberikan pelayanan yang nyaman, Klinik Ar-Rahman menyediakan mushola baik di Klinik Ar-Rahman Medika maupun Klinik Ar-Rahman Galuh Mas sehingga bagi pengunjung yang sedang menunggu untuk diperiksa dan telah masuk waktu sholat dapat melakukan ibadah dengan tenang dan khusyu.',
            'route' => 'Mushola',
            'image' => '/img/klinik-arrahman-mushola.jpg'
        ]);

        DB::table('facilities')->insert([
            'facility_name' => 'Laboratorium',
            'mini_detail' => 'Fasilitas penunjang yang mempermudah pemeriksaan pasien,',
            'detail' => 'Untuk menunjang dan mempermudah pemeriksaan pasien, Klinik Ar-Rahman menyediakan layanan Laboratorium baik di Klinik Ar-Rahman Medika maupun di Klinik Ar-Rahman Galuh Mas. Dengan adanya layanan laboratorium ini pasien lebih mudah dalam berobat apabila dibutuhkan pemeriksaan penunjang karena laboratorium tersedia di dua cabang klinik kami.',
            'route' => 'Laboratorium',
            'image' => '/img/klinik-arrahman-laboratorium.jpg'
        ]);

        DB::table('facilities')->insert([
            'facility_name' => 'Pojok Asi',
            'mini_detail' => 'Tempat khusus bagi ibu yang ingin menyusui bayinya',
            'detail' => 'Salah satu kendala seorang ibu yang memiliki bayi adalah kesulitan memberikan ASI Eksklusif bagi bayinya di tempat umum, di Klinik Ar-Rahman kami menyediakan Pojok ASI yaitu tempat khusus yang bagi ibu yang ingin menyusui bayinya.',
            'route' => 'Pojok_Asi',
            'image' => '/img/aw1NASfSuSjalNyEGIK02fmQ5I0f4qBmlde5itHI.jpeg'
        ]);

        DB::table('services')->insert([
            'service_name' => 'Poliklinik Umum',
            'mini_detail' => 'Melayani pasien Umum maupun pasien BPJS Kesehatan dengan dokter yang profesional dan ter registrasi secara legal',
            'detail' => 'Melayani pasien Umum maupun pasien BPJS Kesehatan dengan dokter yang profesional dan ter registrasi secara legal, kami melayani dengan sepenuh hati guna memberikan pelayanan yang optimal kepada pasien. Selain itu kami juga bekerja sama dengan banyak mitra asuransi kesehatan untuk menjangkau seluruh lapisan masyarakat dalam menyediakan pelayanan kesehatan tanpa membeda - bedakan pasien, sehingga dapat membantu pemerintah dalam meningkatkan derajat kesehatan masyarakat.',
            'route' => 'Poliklinik_Umum',
            'image' => '/img/RuPFa6OJY41xWBqrE4jn3tp06uBXHZdLPviS8y6t.jpeg'
        ]);

        DB::table('services')->insert([
            'service_name' => 'Poliklinik Gigi',
            'mini_detail' => 'Tempat khusus bagi ibu yang ingin menyusui bayinya',
            'detail' => 'Salah satu kendala seorang ibu yang memiliki bayi adalah kesulitan memberikan ASI Eksklusif bagi bayinya di tempat umum, di Klinik Ar-Rahman kami menyediakan Pojok ASI yaitu tempat khusus yang bagi ibu yang ingin menyusui bayinya.',
            'route' => 'Poliklinik_Gigi',
            'image' => '/img/oPcFZUN64LzhFa69MbV5ZjVqXtd0BNDpCKdhDgFq.jpeg'
        ]);

        DB::table('services')->insert([
            'service_name' => 'KIA',
            'mini_detail' => 'Pelayanan dan pemeliharaan ibu hamil, ibu bersalin, ibu menyusui, bayi dan anak balita serta anak prasekolah.',
            'detail' => 'Kesehatan Ibu dan Anak merupakan upaya di bidang kesehatan yang menyangkut pelayanan dan pemeliharaan ibu hamil, ibu bersalin, ibu menyusui, bayi dan anak balita serta anak prasekolah. Tujuan Pelayanan Kesehatan Ibu dan anak (KIA) adalah tercapainya kemampuan hidup sehat melalui peningkatan derajat kesehatan yang optimal, bagi ibu dan keluarganya untuk menuju Norma Keluarga Kecil Bahagia Sejahtera (NKKBS) serta meningkatnya derajat kesehatan anak untuk menjamin proses tumbuh kembang optimal yang merupakan landasan bagi peningkatan kualitas manusia seutuhnya.',
            'route' => 'KIA',
            'image' => '/img/6PZRiWil9w4ei74zOQKEVRMpQqHZg5BjnuP0tyGT.jpeg'
        ]);
        
        DB::table('services')->insert([
            'service_name' => 'Trauma Center',
            'mini_detail' => 'Klinik Ar-Rahman Medika telah bekerja sama dengan BPJS Ketenagakerjaan menghadirkan trauma center yang berfungsi menyelenggarakan program jaminan kecelakaan kerja bagi peserta BPJS Ketenagakerjaan.',
            'detail' => 'Klinik Ar-Rahman Medika telah bekerja sama dengan BPJS Ketenagakerjaan menghadirkan trauma center yang berfungsi menyelenggarakan program jaminan kecelakaan kerja bagi peserta BPJS Ketenagakerjaan. Kecelakaan yang terjadi dalam hubungan kerja, termasuk kecelakaan yang terjadi dalam perjalanan dari rumah menuju tempat kerja atau sebaliknya, dan penyakit yang disebabkan oleh lingkungan kerja.',
            'route' => 'Trauma_Center',
            'image' => '/img/jg8WCUNygoVy52wHSR6ZOrd9KYiKV04S9myooeUU.jpeg'
        ]);

        DB::table('services')->insert([
            'service_name' => 'Khitan',
            'mini_detail' => 'Khitan yang didukung dengan dokter yang ahli sehingga dipastikan aman',
            'detail' => 'Klinik Ar-Rahman Medika membuka layanan Khitanan yang dilakukan dengan metode laser dan khitan biasa dengan dokter dan petugas medis yang profesional dan berpengalaman sehingga khitan dipastikan aman.',
            'route' => 'Khitan',
            'image' => '/img/istockphoto-861286764-170667a.jpg'
        ]);

        DB::table('profiles')->insert([
            'clinic_name' => 'Klinik Ar-Rahman Medika',
            'image' => '/img/klinikmedika.jpg',
            'description' => 'Klinik Ar-Rahman Medika didirikan pada tanggal 02 Januari 2014 dibawah naungan Yayasan Ar Rahman Medika. Pada saat itu kami mendirikan klinik tersebut karena jumlah populasi masyarakat dengan fasilitas kesehatan sangat kurang. Berdasarkan hal tersebut ketua Yayasan Ar Rahman Medika yaitu dr. Sigit Aryanto mendirikan Klinik Ar-rahman Medika. Ketua pimpinan Klinik Ar-Rahman Medika yaitu dr. Dwi Susilo SH,MH melakukan inovasi terhadap klinik untuk dapat berkontribusi terhadap masyarakat seperti sunatan massal, baksos, dll sehingga manfaatnya sangat terasa oleh masyarakat sekitar. Pada tahun 2019 Yayasan Ar-Rahman Medika membuka cabang tepatnya didaerah Jl. Bharata Perumnas yang mana didirikan atas permintaan masyarakat daerah tersebut.',
            'doctors' => 'dr. Sigit Aryanto|dr. Dwi Susilo SH,MH|dr. Chintia Nilna Muna|dr. Ayu Delvira Putri|dr. Onelola Dewita Karya, MARS|drg. Rori Sasmita|drg. Widi Astuti',
            'vision' => 'Menjadi Klinik pusat pelayanan kesehatan yang prima di Teluk Jambe kabupaten Karawang',
            'mission' => 'Mengutamakan pelayanan yang berkualitas dan profesionalisme demi menjamin kualitas hidup yang lebih baik. <br><br>Membantu peran pemerintah dalam mengangkat derajat kesehatan masyarakat. <br><br>Membantu pelayanan kesehatan yang prima dengan menjangkau seluruh lapisan masyarakat.',
            'facebook_name' => 'Klinik Ar-Rahman Medika',
            'facebook_link' => 'https://www.facebook.com/KlinikArrahmanMedika',
            'instagram_name' => 'klinik.arrahman',
            'instagram_link' => 'https://www.instagram.com/klinik.arrahman/',
            'twitter_name' => 'Klinik Ar-Rahman',
            'twitter_link' => 'https://twitter.com/Karawang_ID/status/1457291659310366725?t=KT9SiLKe_esKmCj1qHFcAQ&s=19',
            'address' => 'Jl.Raya Puri no 7 Desa Sukakarya rt 02/01 Teluk Jambe Timur Karawang 41361',
            'googleMap_link' => 'https://www.google.com/maps/place/Klinik+Ar+Rahman+Medika/@-6.3319977,107.3097634,17z/data=!3m1!4b1!4m5!3m4!1s0x2e69762018b32a95:0x88aab747205f153f!8m2!3d-6.3320617!4d107.3119551?hl=id',
            'phone' => '02678458155 | 081288051383',
            'email' => 'medika@klinikarrahman.com',
            'work_days' => 'Senin - Minggu',
            'work_time' => '08.00 - 22.00',
        ]);

        DB::table('galleries')->insert([
            'name' => 'Penyerahan Obat',
            'image' => '/img/klinik-arrahman-penyerahan-obat.jpg'
        ]);

        DB::table('galleries')->insert([
            'name' => 'Pemeriksaan Gigi',
            'image' => '/img/klinik-arrahman-periksa-gigi.jpg'
        ]);

        DB::table('galleries')->insert([
            'name' => 'Ruang Tunggu',
            'image' => '/img/klinik-arrahman-ruang-tunggu.jpg'
        ]);

        DB::table('galleries')->insert([
            'name' => 'Kursi Prioritas',
            'image' => '/img/klinik-arrahman-kursi-prioritas.jpg'
        ]);

        DB::table('galleries')->insert([
            'name' => 'Kunjungan klinik berprestasi 1',
            'image' => '/img/klinik-arrahman-kunjungan-klinik-berprestasi.jpg'
        ]);

        DB::table('galleries')->insert([
            'name' => 'Kunjungan klinik berprestasi 2',
            'image' => '/img/klinik-arrahman-kunjungan-klinik-berprestasi-2.jpg'
        ]);

        DB::table('categories')->insert([
            'category_name' => 'Kesehatan'
        ]);

        DB::table('categories')->insert([
            'category_name' => 'Berita'
        ]);

        DB::table('categories')->insert([
            'category_name' => 'Gaya hidup'
        ]);

        DB::table('categories')->insert([
            'category_name' => 'Olahraga'
        ]);

        DB::table('categories')->insert([
            'category_name' => 'Makanan sehat'
        ]);

        DB::table('hero_contents')->insert([
            'title' => 'Periksakan kesehatan anda bersama dr. Chintia Nilna Muna',
            'detail' => 'di klinik ar-rahman medika, senin, rabu, dan jumat, pukul 15.30 - 17.00',
            // 'image' => '/gambar/hero2.png'
        ]);

        DB::table('hero_images')->insert([
          
            'image' => '/img/HJzHd86tfxJFEdz23gAKg5Gu00ufroK4kCAavOcx.png'
        ]);

        DB::table('hero_images')->insert([
          
            'image' => '/img/fps8prv4mt0aEuOT7pGTGTa1tivUIcqe54cCdQ49.jpeg'
        ]);

        DB::table('hero_images')->insert([
          
            'image' => '/img/f1Z8IlIje66IckhZdDn7mezX4ZAFgDREwuUx4RfL.png'
        ]);

        DB::table('clinic_contacts')->insert([
            'clinic_name' => 'Klinik Ar-Rahman Galuh Mas',
            'facebook_name' => '#usernamefacebook',
            'facebook_link' => 'https://www.facebook.com/#',
            'instagram_name' => '#usernameinstagram',
            'instagram_link' => 'https://www.instagram.com/#',
            'twitter_name' => '#usernametwitter',
            'twitter_link' => 'https://www.twitter.com/#',
            'address' => 'Jl.Bharata blok U-23 Sukaluyu Teluk jambe Timur Karawang 41361',
            'googleMap_link' => 'https://www.google.com/maps/place/Klinik+Ar-rahman+Galuh+Mas/@-6.329173,107.286864,16z/data=!4m5!3m4!1s0x0:0x7306f42c858b46bf!8m2!3d-6.3291727!4d107.2868635?hl=en',
            'phone' => '081290270571',
            'email' => 'galuh@klinikarrahman.com',
        ]);
    }
}
