<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            // $table->bigInteger('clinic_id');
            $table->string('clinic_name');
            $table->string('image');
            $table->text('description');
            $table->text('doctors');
            $table->text('vision');
            $table->text('mission');
            $table->string('facebook_name')->nullable();
            $table->string('facebook_link')->nullable();
            $table->string('instagram_name')->nullable();
            $table->string('instagram_link')->nullable();
            $table->string('twitter_name')->nullable();
            $table->string('twitter_link')->nullable();
            $table->text('address');
            $table->text('googleMap_link');
            $table->string('phone');
            $table->string('email');
            $table->string('work_days');
            $table->string('work_time');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
