<?php

use App\Http\Controllers\AddArticleCommentsController;
use App\Http\Controllers\AddArticleController;
use App\Http\Controllers\AddCategoryController;
use App\Http\Controllers\AddClinicContactController;
use App\Http\Controllers\AddClinicController;
use App\Http\Controllers\AddContactClinicController;
use App\Http\Controllers\AddContactCliniController;
use App\Http\Controllers\AddDoctorController;
use App\Http\Controllers\AddFacilityController;
use App\Http\Controllers\AddFeedbackController;
use App\Http\Controllers\AddGalleryController;
use App\Http\Controllers\AddHeroContentController;
use App\Http\Controllers\AddHeroImageController;
use App\Http\Controllers\AddProfileController;
use App\Http\Controllers\AddServiceController;
use App\Http\Controllers\AddSponsorController;
use App\Http\Controllers\ArticleCommentsController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ClinicContactController;
use App\Http\Controllers\ClinicController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DeleteArticleCommentsController;
use App\Http\Controllers\DeleteArticleController;
use App\Http\Controllers\DeleteCategoryController;
use App\Http\Controllers\DeleteClinicContactController;
use App\Http\Controllers\DeleteClinicController;
use App\Http\Controllers\DeleteContactClinicController;
use App\Http\Controllers\DeleteDoctorClinicController;
use App\Http\Controllers\DeleteDoctorController;
use App\Http\Controllers\DeleteFacilityController;
use App\Http\Controllers\DeleteFeedbackController;
use App\Http\Controllers\DeleteGalleryController;
use App\Http\Controllers\DeleteHeroContentController;
use App\Http\Controllers\DeleteHeroImageController;
use App\Http\Controllers\DeleteProfileController;
use App\Http\Controllers\DeleteServiceController;
use App\Http\Controllers\DeleteSponsorController;
use App\Http\Controllers\DoctorClinicController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\EditArticleCommentsController;
use App\Http\Controllers\EditArticleController;
use App\Http\Controllers\EditCategoryController;
use App\Http\Controllers\EditClinicContactController;
use App\Http\Controllers\EditClinicController;
use App\Http\Controllers\EditContactClinicController;
use App\Http\Controllers\EditDoctorClinicController;
use App\Http\Controllers\EditDoctorController;
use App\Http\Controllers\EditFacilityController;
use App\Http\Controllers\EditFeedbackController;
use App\Http\Controllers\EditGalleryController;
use App\Http\Controllers\EditHeroContentController;
use App\Http\Controllers\EditHeroImageController;
use App\Http\Controllers\EditProfileController;
use App\Http\Controllers\EditServiceController;
use App\Http\Controllers\EditSponsorController;
use App\Http\Controllers\FacilityController;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\HeroContentController;
use App\Http\Controllers\HeroImageController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\SponsorController;
use App\Http\Controllers\UserAboutController;
use App\Http\Controllers\UserAddFeedbackController;
use App\Http\Controllers\UserArticleController;
use App\Http\Controllers\UserContactController;
use App\Http\Controllers\UserFacilityController;
use App\Http\Controllers\UserListArticleController;
use App\Http\Controllers\UserListCategoryController;
use App\Http\Controllers\UserLocGaluhController;
use App\Http\Controllers\UserLocMedikaController;
use App\Http\Controllers\UserServiceController;
use App\Models\Article;
use App\Models\ArticleComments;
use App\Models\Category;
use App\Models\Clinic;
use App\Models\ClinicContact;
use App\Models\Doctor;
use App\Models\DoctorClinic;
use App\Models\EditFacility;
use App\Models\Facility;
use App\Models\Feedback;
use App\Models\Gallery;
use App\Models\HeroContent;
use App\Models\HeroImage;
use App\Models\Profile;
use App\Models\Service;
use App\Models\Sponsor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;

Route::get('getArticle', function (Request $request) {
    if ($request->ajax()) {
            $data = Article::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="/admin/article/edit/'. $row->id .'" class="edit btn btn-success btn-sm">Edit</a> <a href="/admin/article/delete/'. $row->id .'" class="delete btn btn-danger btn-sm">Delete</a>';
                    
                    return $actionBtn;
                })->addColumn('desc', function($row){
                    return Str::limit($row->detail, 60);
                })->addColumn('image', function($row){
                    $image = '<img src="'. $row->image .'" alt="'. $row->service_name .'" width="130">';
                    return $image;
                })->addColumn('tanggal', function($row){
                    $tanggal = $row->created_at;
                    return $tanggal;
                })
                ->rawColumns(['action', 'desc', 'image', 'tanggal'])
                ->make(true);
        }
})->name('admin.article'); 

Route::get('getCategory', function (Request $request) {
    if ($request->ajax()) {
            $data = Category::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="/admin/category/edit/'. $row->id .'" class="edit btn btn-success btn-sm">Edit</a> <a href="/admin/category/delete/'. $row->id .'" class="delete btn btn-danger btn-sm">Delete</a>';
                    return $actionBtn;
                })->addColumn('tanggal', function($row){
                    $tanggal = $row->created_at;
                    return $tanggal;
                })
                ->rawColumns(['action', 'tanggal'])
                ->make(true);
        }
})->name('admin.category'); 

Route::get('getClinic', function (Request $request) {
    if ($request->ajax()) {
            $data = Clinic::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="/admin/clinic/edit/'. $row->id .'" class="edit btn btn-success btn-sm">Edit</a> <a href="/admin/clinic/delete/'. $row->id .'" class="delete btn btn-danger btn-sm">Delete</a>';
                    return $actionBtn;
                })->addColumn('image', function($row){
                    $image = '<img src="'. $row->image .'" alt="'. $row->clinic_name .'" width="130">';
                    return $image;
                })->addColumn('desc', function($row){
                    return Str::limit($row->description, 60);
                })->addColumn('tanggal', function($row){
                    $tanggal = $row->created_at;
                    return $tanggal;
                })
                ->rawColumns(['action', 'image', 'desc', 'tanggal'])
                ->make(true);
        }
})->name('admin.clinic');

Route::get('getDoctor', function (Request $request) {
    if ($request->ajax()) {
            $data = Doctor::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="/admin/doctor/edit/'. $row->id .'" class="edit btn btn-success btn-sm">Edit</a> <a href="/admin/doctor/delete/'. $row->id .'" class="delete btn btn-danger btn-sm">Delete</a>';
                    return $actionBtn;
                })->addColumn('image', function($row){
                    $image = '<img src="'. $row->image .'" alt="'. $row->doctor_name .'" width="130">';
                    return $image;
                })->addColumn('desc', function($row){
                    // $exploded = explode('&',$row->detail);
                    // $workplaces = '';
                    // foreach($exploded as $workplace){
                    //     $workplaces .= $workplace . ' & '; 
                    // }
                    return $row->detail;
                })->addColumn('tanggal', function($row){
                    $tanggal = $row->created_at;
                    return $tanggal;
                })
                ->rawColumns(['action', 'image', 'desc', 'tanggal'])
                ->make(true);
        }
})->name('admin.doctor'); 

Route::get('getDoctorClinic', function (Request $request) {
    if ($request->ajax()) {
            $data = DoctorClinic::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="/admin/doctorClinic/edit/'. $row->id .'" class="edit btn btn-success btn-sm">Edit</a> <a href="/admin/doctorClinic/delete/'. $row->id .'" class="delete btn btn-danger btn-sm">Delete</a>';
                    return $actionBtn;
                })->addColumn('tanggal', function($row){
                    $tanggal = $row->created_at;
                    return $tanggal;
                })
                ->rawColumns(['action', 'tanggal'])
                ->make(true);
        }
})->name('admin.doctorClinic'); 

Route::get('getFacility', function (Request $request) {
    if ($request->ajax()) {
            $data = Facility::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="/admin/facility/edit/'. $row->id .'" class="edit btn btn-success btn-sm">Edit</a> <a href="/admin/facility/delete/'. $row->id .'" class="delete btn btn-danger btn-sm">Delete</a>';
                    return $actionBtn;
                })->addColumn('image', function($row){
                    $image = '<img src="'. $row->image .'" alt="'. $row->facility_name .'" width="130">';
                    return $image;
                })->addColumn('desc', function($row){
                    return Str::limit($row->detail, 60);
                })->addColumn('tanggal', function($row){
                    $tanggal = $row->created_at;
                    return $tanggal;
                })
                ->rawColumns(['action', 'image', 'desc', 'tanggal'])
                ->make(true);
        }
})->name('admin.facility'); 

Route::get('getFeedback', function (Request $request) {
    if ($request->ajax()) {
            $data = Feedback::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="/admin/feedback/edit/'. $row->id .'" class="edit btn btn-success btn-sm">Edit</a> <a href="/admin/feedback/delete/'. $row->id .'" class="delete btn btn-danger btn-sm">Delete</a>';
                    return $actionBtn;
                })->addColumn('tanggal', function($row){
                    $tanggal = $row->created_at;
                    return $tanggal;
                })
                ->rawColumns(['action', 'tanggal'])
                ->make(true);
        }
})->name('admin.feedback'); 

Route::get('getGallery', function (Request $request) {
    if ($request->ajax()) {
            $data = Gallery::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="/admin/gallery/edit/'. $row->id .'" class="edit btn btn-success btn-sm">Edit</a> <a href="/admin/gallery/delete/'. $row->id .'" class="delete btn btn-danger btn-sm">Delete</a>';
                    return $actionBtn;
                })->addColumn('image', function($row){
                    $image = '<img src="'. $row->image .'" alt="'. $row->name .'" width="130">';
                    return $image;
                })->addColumn('tanggal', function($row){
                    $tanggal = $row->created_at;
                    return $tanggal;
                })
                ->rawColumns(['action', 'image', 'tanggal'])
                ->make(true);
        }
})->name('admin.gallery'); 

Route::get('getProfile', function (Request $request) {
    if ($request->ajax()) {
            $data = Profile::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="/admin/profile/edit/'. $row->id .'" class="edit btn btn-success btn-sm">Edit</a> <a href="/admin/profile/delete/'. $row->id .'" class="delete btn btn-danger btn-sm">Delete</a>';
                    return $actionBtn;
                })
                ->addColumn('desc', function($row){
                    return $row->detail;
                })
                ->rawColumns(['action', 'desc'])
                ->make(true);
        }
})->name('admin.profile'); 

Route::get('getService', function (Request $request) {
    if ($request->ajax()) {
            $data = Service::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="/admin/service/edit/'. $row->id .'" class="edit btn btn-success btn-sm">Edit</a> <a href="/admin/service/delete/'. $row->id .'" class="delete btn btn-danger btn-sm">Delete</a>';
                    return $actionBtn;
                })
                ->addColumn('image', function($row){
                    $image = '<img src="'. $row->image .'" alt="'. $row->service_name .'" width="130">';
                    return $image;
                })
                ->addColumn('desc', function($row){
                    return Str::limit($row->detail, 60);
                })->addColumn('tanggal', function($row){
                    $tanggal = $row->created_at;
                    return $tanggal;
                })
                ->rawColumns(['action', 'image', 'desc', 'tanggal'])
                ->make(true);
        }
})->name('admin.service'); 

Route::get('getSponsor', function (Request $request) {
    if ($request->ajax()) {
            $data = Sponsor::latest()->get();
            return DataTables::of($data)
                // ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="/admin/sponsor/edit/'. $row->id .'" class="edit btn btn-success btn-sm">Edit</a> <a href="/admin/sponsor/delete/'. $row->id .'" class="delete btn btn-danger btn-sm">Delete</a>';
                    return $actionBtn;
                })->addColumn('image', function($row){
                    $image = '<img src="'. $row->image .'" width="130">';
                    return $image;
                })->addColumn('tanggal', function($row){
                    $tanggal = $row->created_at;
                    return $tanggal;
                })
                ->rawColumns(['action', 'image', 'tanggal'])
                ->make(true);
        }
})->name('admin.sponsor'); 

Route::get('getClinicContact', function (Request $request) {
    if ($request->ajax()) {
            $data = ClinicContact::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="/admin/clinicContact/edit/'. $row->id .'" class="edit btn btn-success btn-sm">Edit</a> <a href="/admin/clinicContact/delete/'. $row->id .'" class="delete btn btn-danger btn-sm">Delete</a>';
                    
                    return $actionBtn;
                })->addColumn('telepon', function($row){
                    $telepon = '(+62)'. $row->phone;
                    return $telepon;
                })->addColumn('tanggal', function($row){
                    $tanggal = $row->created_at;
                    return $tanggal;
                })
                ->rawColumns(['action', 'telepon', 'tanggal'])
                ->make(true);
        }
})->name('admin.clinicContact');

Route::get('getArticleComments', function (Request $request) {
    if ($request->ajax()) {
            $data = ArticleComments::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="/admin/articleComments/edit/'. $row->id .'" class="edit btn btn-success btn-sm">Edit</a> <a href="/admin/articleComments/delete/'. $row->id .'" class="delete btn btn-danger btn-sm">Delete</a>';
                    
                    return $actionBtn;
                })->addColumn('telepon', function($row){
                    $telepon = '(+62)'. $row->phone;
                    return $telepon;
                })->addColumn('tanggal', function($row){
                    $tanggal = $row->created_at;
                    return $tanggal;
                })
                ->rawColumns(['action', 'telepon', 'tanggal'])
                ->make(true);
        }
})->name('admin.articleComments');

Route::get('getHeroContent', function (Request $request) {
    if ($request->ajax()) {
            $data = HeroContent::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="/admin/heroContent/edit/'. $row->id .'" class="edit btn btn-success btn-sm">Edit</a> <a href="/admin/heroContent/delete/'. $row->id .'" class="delete btn btn-danger btn-sm">Delete</a>';
                    
                    return $actionBtn;
                })
                // ->addColumn('image', function($row){
                //     $image = '<img src="'. $row->image .'" width="130">';
                //     return $image;
                // })
                ->addColumn('desc', function($row){
                    return Str::limit($row->detail, 60);
                })
                ->rawColumns(['action', 'desc'])
                ->make(true);
        }
})->name('admin.heroContent');

Route::get('getHeroContent', function (Request $request) {
    if ($request->ajax()) {
            $data = HeroImage::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="/admin/heroImage/edit/'. $row->id .'" class="edit btn btn-success btn-sm">Edit</a> <a href="/admin/heroImage/delete/'. $row->id .'" class="delete btn btn-danger btn-sm">Delete</a>';
                    
                    return $actionBtn;
                })
                ->addColumn('image', function($row){
                    $image = '<img src="'. $row->image .'" width="130">';
                    return $image;
                })->addColumn('tanggal', function($row){
                    $tanggal = $row->created_at;
                    return $tanggal;
                })
                ->rawColumns(['action', 'image', 'tanggal'])
                ->make(true);
        }
})->name('admin.heroImage');

Route::get('/', DashboardController::class)->name('/');
Route::get('/about', UserAboutController::class)->name('about');
Route::get('/layanan', UserServiceController::class)->name('layanan');
Route::get('/fasilitas', UserFacilityController::class)->name('fasilitas');
Route::get('/lokasi-medika', UserLocMedikaController::class)->name('lokasi-medika');
Route::get('/lokasi-galuh', UserLocGaluhController::class)->name('lokasi-galuh');
Route::get('/kontak', UserContactController::class)->name('kontak');
Route::get('/list-artikel', UserListArticleController::class)->name('list-artikel');
Route::get('/list-artikel/{category_name}', UserListCategoryController::class)->name('category-list-artikel');
Route::get('/user-add-feedback', [UserAddFeedbackController::class, 'create'])->name('user-add-feedback');
Route::post('/user-add-feedback', [UserAddFeedbackController::class, 'store'])->name('user-add-feedback');
Route::get('/artikel/{title}', [UserArticleController::class, 'create'])->name('artikel');
Route::post('/artikel/{title}', [UserArticleController::class, 'store'])->name('artikel');




Route::middleware('guest')->group(function(){
    
    Route::get('/login', [LoginController::class, 'create'])->name('login');
    Route::post('/login', [LoginController::class, 'store'])->name('login');
});

Route::middleware('auth')->group(function(){
    Route::get('/register', [RegistrationController::class, 'create'])->name('register');
    Route::post('/register', [RegistrationController::class, 'store'])->name('register');
    
    Route::get('/admin/sponsor/delete/{id}', DeleteSponsorController::class)->name('deleteSponsor');
    Route::get('/admin/article/delete/{id}', DeleteArticleController::class)->name('deleteArticle');
    Route::get('/admin/category/delete/{id}', DeleteCategoryController::class)->name('deleteCategory');
    Route::get('/admin/clinic/delete/{id}', DeleteClinicController::class)->name('deleteClinic');
    Route::get('/admin/doctor/delete/{id}', DeleteDoctorController::class)->name('deleteDoctor');
    Route::get('/admin/doctorClinic/delete/{id}', DeleteDoctorClinicController::class)->name('deleteDoctorClinic');
    Route::get('/admin/facility/delete/{id}', DeleteFacilityController::class)->name('deleteFacility');
    Route::get('/admin/feedback/delete/{id}', DeleteFeedbackController::class)->name('deleteFeedback');
    Route::get('/admin/gallery/delete/{id}', DeleteGalleryController::class)->name('deleteGallery');
    Route::get('/admin/profile/delete/{id}', DeleteProfileController::class)->name('deleteProfile');
    Route::get('/admin/service/delete/{id}', DeleteServiceController::class)->name('deleteService');
    Route::get('/admin/clinicContact/delete/{id}', DeleteClinicContactController::class)->name('deleteClinicContact');
    Route::get('/admin/articleComments/delete/{id}', DeleteArticleCommentsController::class)->name('deleteArticleComments');
    Route::get('/admin/heroContent/delete/{id}', DeleteHeroContentController::class)->name('deleteHeroContent');
    Route::get('/admin/heroImage/delete/{id}', DeleteHeroImageController::class)->name('deleteHeroImage');

    Route::get('/admin/article/edit/{id}', [EditArticleController::class, 'edit'])->name('editArticle');
    Route::post('/admin/article/edit/{id}', [EditArticleController::class, 'update'])->name('editArticle');
    Route::get('/admin/clinic/edit/{id}', [EditClinicController::class, 'edit'])->name('editClinic');
    Route::post('/admin/clinic/edit/{id}', [EditClinicController::class, 'update'])->name('editClinic');
    // Route::get('/admin/profile/edit/{id}', [EditProfileController::class, 'edit'])->name('editProfile');
    // Route::post('/admin/profile/edit/{id}', [EditProfileController::class, 'update'])->name('editProfile');
    Route::get('/admin/category/edit/{id}', [EditCategoryController::class, 'edit'])->name('editCategory');
    Route::post('/admin/category/edit/{id}', [EditCategoryController::class, 'update'])->name('editCategory');
    Route::get('/admin/gallery/edit/{id}', [EditGalleryController::class, 'edit'])->name('editGallery');
    Route::post('/admin/gallery/edit/{id}', [EditGalleryController::class, 'update'])->name('editGallery');
    Route::get('/admin/doctorClinic/edit/{id}', [EditDoctorClinicController::class, 'edit'])->name('editDoctorClinic');
    Route::post('/admin/doctorClinic/edit/{id}', [EditDoctorClinicController::class, 'update'])->name('editDoctorClinic');
    Route::get('/admin/doctor/edit/{id}', [EditDoctorController::class, 'edit'])->name('editDoctor');
    Route::post('/admin/doctor/edit/{id}', [EditDoctorController::class, 'update'])->name('editDoctor');
    Route::get('/admin/service/edit/{id}', [EditServiceController::class, 'edit'])->name('editService');
    Route::post('/admin/service/edit/{id}', [EditServiceController::class, 'update'])->name('editService');
    Route::get('/admin/facility/edit/{id}', [EditFacilityController::class, 'edit'])->name('editFacility');
    Route::post('/admin/facility/edit/{id}', [EditFacilityController::class, 'update'])->name('editFacility');
    Route::get('/admin/feedback/edit/{id}', [EditFeedbackController::class, 'edit'])->name('editFeedback');
    Route::post('/admin/feedback/edit/{id}', [EditFeedbackController::class, 'update'])->name('editFeedback');
    Route::get('/admin/sponsor/edit/{id}', [EditSponsorController::class, 'edit'])->name('editSponsor');
    Route::post('/admin/sponsor/edit/{id}', [EditSponsorController::class, 'update'])->name('editSponsor');
    Route::get('/admin/clinicContact/edit/{id}', [EditClinicContactController::class, 'edit'])->name('editClinicContact');
    Route::post('/admin/clinicContact/edit/{id}', [EditClinicContactController::class, 'update'])->name('editClinicContact');
    Route::get('/admin/articleComments/edit/{id}', [EditArticleCommentsController::class, 'edit'])->name('editArticleComments');
    Route::post('/admin/articleComments/edit/{id}', [EditArticleCommentsController::class, 'update'])->name('editArticleComments');
    Route::get('/admin/heroContent', [EditHeroContentController::class, 'edit'])->name('heroContent');
    Route::post('/admin/heroContent', [EditHeroContentController::class, 'update'])->name('heroContent');
    Route::get('/admin/heroImage/edit/{id}', [EditHeroImageController::class, 'edit'])->name('editHeroImage');
    Route::post('/admin/heroImage/edit/{id}', [EditHeroImageController::class, 'update'])->name('editHeroImage');
    
    Route::post('/admin/logout', LogoutController::class)->name('logout');

    Route::get('/admin/add-facility', [AddFacilityController::class, 'create'])->name('add-facility');
    Route::post('/admin/add-facility', [AddFacilityController::class, 'store'])->name('add-facility');
    Route::get('/admin/add-doctor', [AddDoctorController::class, 'create'])->name('add-doctor');
    Route::post('/admin/add-doctor', [AddDoctorController::class, 'store'])->name('add-doctor');
    Route::get('/admin/add-article', [AddArticleController::class, 'create'])->name('add-article');
    Route::post('/admin/add-article', [AddArticleController::class, 'store'])->name('add-article');
    Route::get('/admin/add-service', [AddServiceController::class, 'create'])->name('add-service');
    Route::post('/admin/add-service', [AddServiceController::class, 'store'])->name('add-service');
    Route::get('/admin/add-clinic', [AddClinicController::class, 'create'])->name('add-clinic');
    Route::post('/admin/add-clinic', [AddClinicController::class, 'store'])->name('add-clinic');
    Route::get('/admin/add-gallery', [AddGalleryController::class, 'create'])->name('add-gallery');
    Route::post('/admin/add-gallery', [AddGalleryController::class, 'store'])->name('add-gallery');
    Route::get('/admin/add-category', [AddCategoryController::class, 'create'])->name('add-category');
    Route::post('/admin/add-category', [AddCategoryController::class, 'store'])->name('add-category');
    Route::get('/admin/add-feedback', [AddFeedbackController::class, 'create'])->name('add-feedback');
    Route::post('/admin/add-feedback', [AddFeedbackController::class, 'store'])->name('add-feedback');
    Route::get('/admin/add-profile', [AddProfileController::class, 'create'])->name('add-profile');
    Route::post('/admin/add-profile', [AddProfileController::class, 'store'])->name('add-profile');
    Route::get('/admin/add-sponsor', [AddSponsorController::class, 'create'])->name('add-sponsor');
    Route::post('/admin/add-sponsor', [AddSponsorController::class, 'store'])->name('add-sponsor');
    Route::get('/admin/add-clinicContact', [AddClinicContactController::class, 'create'])->name('add-clinicContact');
    Route::post('/admin/add-clinicContact', [AddClinicContactController::class, 'store'])->name('add-clinicContact');
    Route::get('/admin/add-articleComments', [AddArticleCommentsController::class, 'create'])->name('add-articleComments');
    Route::post('/admin/add-articleComments', [AddArticleCommentsController::class, 'store'])->name('add-articleComments');
    Route::get('/admin/add-heroContent', [AddHeroContentController::class, 'create'])->name('add-heroContent');
    Route::post('/admin/add-heroContent', [AddHeroContentController::class, 'store'])->name('add-heroContent');
    Route::get('/admin/add-heroImage', [AddHeroImageController::class, 'create'])->name('add-heroImage');
    Route::post('/admin/add-heroImage', [AddHeroImageController::class, 'store'])->name('add-heroImage');

    // Route::get('/admin/doctorClinic', [DoctorClinicController::class, 'create'])->name('doctorClinic');
    // Route::post('/admin/doctorClinic', [DoctorClinicController::class, 'store'])->name('doctorClinic');
    Route::get('/admin/facility', FacilityController::class)->name('facility');
    Route::get('/admin/doctor', DoctorController::class)->name('doctor');
    Route::get('/admin/article', ArticleController::class)->name('article');
    Route::get('/admin/profile', [EditProfileController::class, 'edit'])->name('profile');
    Route::post('/admin/profile', [EditProfileController::class, 'update'])->name('profile');
    Route::get('/admin/category', CategoryController::class)->name('category');
    Route::get('/admin/clinic', ClinicController::class)->name('clinic');
    Route::get('/admin/gallery', GalleryController::class)->name('gallery');
    Route::get('/admin/service', ServiceController::class)->name('service');
    Route::get('/admin/feedback', FeedbackController::class)->name('feedback');
    Route::get('/admin/sponsor', SponsorController::class)->name('sponsor');
    Route::get('/admin/clinicContact', ClinicContactController::class)->name('clinicContact');
    Route::get('/admin/articleComments', ArticleCommentsController::class)->name('articleComments');
    Route::get('/admin/heroImage', HeroImageController::class)->name('heroImage');
    // Route::get('/admin/heroContent', HeroContentController::class)->name('heroContent');
    Route::get('/admin', [IndexController::class, 'create'])->name('index');
});
