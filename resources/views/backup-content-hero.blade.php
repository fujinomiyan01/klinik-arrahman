<section class="hero container mt-5 px-4 px-md-0">
    <div class="row">          
        <div class="col">
          <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel" style="z-index: -1px">
            <div class="carousel-indicators" style="margin-bottom: -50px">
              <button type="button" data-bs-target="#carouselExampleFade" data-bs-slide-to="0" class="active bg-success" aria-current="true" aria-label="Slide 1"></button>
              <button type="button" data-bs-target="#carouselExampleFade" data-bs-slide-to="1" class="bg-success" aria-label="Slide 2"></button>
              <button type="button" data-bs-target="#carouselExampleFade" data-bs-slide-to="2" class="bg-success" aria-label="Slide 3"></button>
              <button type="button" data-bs-target="#carouselExampleFade" data-bs-slide-to="3" class="bg-success" aria-label="Slide 4"></button>
            </div>

            <div class="carousel-inner d-flex" data-aos="zoom-out">
              <div class="col-12 d-flex">
                  <div class="carousel-item active">
                    <div class="d-flex">
                      <div class="col-md-7 p-3">
                        <h1 class="judul">{{ $heroContents[1]->title }}</h1>
                        <p class="desk">{{ $heroContents[1]->detail }}</p>
                      </div>
                      <div class="col-md-5 d-flex justify-content-end ">
                        <img src="{{ $heroContents[1]->image }}" class="d-none d-md-block" alt="...">
                      </div>
                    </div>
                  </div>
  
                  @foreach ($heroContents as $heroContent)
                    <div class="carousel-item">
                      <div class="d-flex">
                        <div class="col-md-7 p-3">
                          <h1 class="judul ">{{ $heroContent->title }}</h1>
                          <p class="desk">{{ $heroContent->detail }}</p>
                        </div>
                        <div class="col-md-5 d-flex justify-content-end ">
                          <img src="{{ $heroContent->image }}" class="d-none d-md-block" alt="...">
                        </div>
                      </div>
                    </div>
                  @endforeach
              </div>
            </div>

          </div>
        </div>
    </div>
  </section>