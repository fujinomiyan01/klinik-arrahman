<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- <link rel="stylesheet" href="../responsive/styles.css"> --}}
    <link rel="stylesheet" href="../responsive/footer.css">
    
    <link rel="stylesheet" href="../responsive/headernavbar.css">  
    <link rel="stylesheet" href="../styles/list-artikel.css">
    <link rel="stylesheet" href="styles/media-query.css">

    <link rel="icon" href="/img/icon_logo-removebg-preview.png" type="image/png">

    

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    {{-- Owl Carousel CSS --}}
    <link rel="stylesheet" href="owl.carousel.min.css">
    <link rel="stylesheet" href="owl.theme.default.min.css">

    <title>Daftar Artikel</title>
  </head>
  <body>

    <x-user-header></x-user-header>

    
   
    <div class="article-title container-fluid text-center">
        <div class="row">
            <div class="col">
                <h1>Daftar Artikel</h1>
                {{-- <p>Artikel Kegiatan Klinik Ar-Rahman, informasi kesehatan dan tips kesehatan dari kami</p>         --}}
            </div>  
        </div>
    </div>  

    <section class="navbar-article container">
      <div class="row">
          <div class="col">
              <nav class="navbar navbar-expand-lg navbar-light">
                  <div class="container-fluid">
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>
                    <a class="navbar-brand" href="">KATEGORI ARTIKEL</a>
                    
                    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                      <ul class="navbar-nav me-auto mb-2 mb-lg-0 container-fluid justify-content-center">
                        <li class="nav-item">
                          <a class="nav-link" href="/list-artikel/">ALL</a>
                        </li>
                        @foreach ($categories as $category)
                          <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="/list-artikel/{{ $category->category_name }}">{{ $category->category_name }}</a>
                          </li>
                        @endforeach
                      </ul>
                      <form class="d-flex">
                        <input class="form-control me-2" type="search" id="input-search-article" placeholder="Cari Artikel..." aria-label="Search">
                        <button class="btn btn-outline-success" type="submit" id="submit-search-article">Cari!</button>
                      </form>
                    </div>
                  </div>
              </nav>
          </div>
      </div>
    </section>

    <div class="main-content container">
        <div class="row">
            @foreach ($articles as $article)
              <div class="col d-flex justify-content-center">
                <div class="box-article-list">
                  <a href="/artikel/{{ $article->title }}">

                    <img src="{{ $article->image }}" alt="">
                    <div class="detail">
                      <?php $tanggal = explode(' ', $article->created_at) ?>

                      <p class="date">{{ $tanggal[0] }} - <span>{{ $article->username }}</span></p>
                      <p class="title">{{ $article->title }}</p>
                      <div class="mini-detail">
                          <p>{!! substr($article->detail, 0, 100)  !!}...</p>
                      </div>
                    </div>
                    {{-- <div class="button"> --}}
                        <a class="button" href="/artikel/{{ $article->title }}">baca selengkapnya</a>
                    {{-- </div> --}}
                    <div class="view py-4">
                      <i class="fa fa-eye" aria-hidden="true"><span class="ms-1">( {{ $article->views }} )</span></i>
                      <i class="fa fa-comments ms-3" aria-hidden="true"><span class="ms-1">( {{ $article->comments }} )</span></i>
                      
                    </div>
                  </a>

                </div> 
              </div>
            @endforeach

            {{ $articles->links() }}
        </div>
    </div>

    <section class="popular-article container">
      <div class="row">
          <p class="title text-center">Popular <span style="color: #0B7125">Post</span></p>

          {{-- <div class="owl-carousel owl-theme"> --}}
            @foreach ($popularArticles as $article)  
              <div class="col d-flex justify-content-center py-3">
                <a href="/artikel/{{ $article->title }}">
                  <div class="article-card">
                      <div class="bg-img">
                          <img src="{{ $article->image }}" alt="">
                      </div>
                      <div class="article-detail">
                          <?php $tanggal = explode(' ', $article->created_at) ?>
                          <p class="date">Cianjur, {{ $tanggal[0] }} - <span>{{ $article->username }}</span></p>
                          <p class="title-article">{{ $article->title }}</p>
                          <div class="sm-detail">
                            <p>{!! substr($article->detail, 0, 30) !!}...</p>
                          </div>
                          <div class="more-detail">
                              <a href="/artikel/{{ $article->title }}" class="d-flex">
                                  Selengkapnya
                                  <img src="/gambar/icon/right-icon.svg" alt="">
                              </a>
                          </div>
                          <div class="view py-4">
                            <i class="fa fa-eye" aria-hidden="true"><span class="ms-1">( {{ $article->views }} )</span></i>
                            <i class="fa fa-comments ms-3" aria-hidden="true"><span class="ms-1">( {{ $article->comments }} )</span></i>
                          </div>
                          
                      </div>
                  </div>
                </a>
              </div>
            @endforeach
          {{-- </div> --}}
      </div>
    </section>









    <x-user-footer></x-user-footer>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    {{-- Owl Carousel --}}
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="owl.carousel.js"></script>
    <script>
      $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 0,
        responsiveClass: true,
        nav: true,
        responsive: {
          0: {
            items: 1,
            nav: true,
          },
          600: {
            items: 2,
            nav: true,
          },
          1000: {
            items: 3,
            nav: true,
            loop: true,
          }
        }
      })
    </script>

    <script>
      $(document).ready(function(){
        $('#submit-search-article').click(function(e){
          e.preventDefault()

          let keyword = $('#input-search-article').val()

          let url = new URL(window.location);
          url.searchParams.set('search', keyword);
          window.history.pushState({}, '', url);
        
          location.reload();
        })
      })
    </script>
  </body>
</html>