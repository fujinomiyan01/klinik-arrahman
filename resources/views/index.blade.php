<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" href="responsive/styles.css">
  <link rel="stylesheet" href="responsive/headernavbar.css">
  <link rel="stylesheet" href="responsive/footer.css">
  <link rel="stylesheet" href="/styles/media-query.css">

  <link rel="stylesheet" href="owl.carousel.min.css">
  <link rel="stylesheet" href="owl.theme.default.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer">
  
  <link rel="icon" href="/img/icon_logo-removebg-preview.png" type="image/png">

  
  <title>Klinik Ar-Rahman</title>
</head>

<body>



  <!--navbar-->
  <x-user-header></x-user-header>
  
  
  <!--hero-->
  
  <div class="hero container mt-5  px-4 px-md-0">
    <div class="row">
      <div class="col-12 col-md-8">
        <h1 class="judul mt-md-5">{{ $heroContents->title }}</h1>
        <p class="desk py-3">{{ $heroContents->detail }}</p>
        
      </div>
      <div class="col-12 col-md-4 mt-5 mt-md-0 d-none d-md-block">
        <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
          <div class="carousel-indicators" style="margin-bottom: 30px;">
            <button type="button" data-bs-target="#carouselExampleFade" data-bs-slide-to="0" class="active bg-success" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleFade" data-bs-slide-to="1" class="bg-success" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleFade" data-bs-slide-to="2" class="bg-success" aria-label="Slide 3"></button>
          </div>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="d-block w-100" src="{{ $heroImages[2]->image }}" alt="First slide">
            </div>
            @foreach ($heroImages as $heroImage)
              <div class="carousel-item">
                <img class="d-block w-100" src="{{ $heroImage->image }}" alt="Second slide">
              </div>
            @endforeach
            
          </div>
          {{-- <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button> --}}
        </div>
      </div>
    </div>
  </div>

  <!--grup icon-->
  <div class="sponsor container mt-5 pt-md-5">
    <div class="row row-cols-1 row-cols-md-5">
      <div class="owl-sponsor owl-carousel owl-theme">
        @foreach ($sponsors as $sponsor)
          <div class="col px-lg-4 mb-3 mb-md-0 d-flex justify-content-center p-5">
            <img class="img-fluid py-3" src="{{ $sponsor->image }}" alt="">
            
          </div>
        @endforeach
      </div>
    </div>
  </div>

  <!--about-->
  <div class="about container mt-5 pt-md-5 px-4 px-md-0">
    <div class="row">
      <div class="col-12 text-center mb-md-5 mb-5">
        <h1>Tentang <span>Kami</span>
        </h1>
      </div>
      <div class="col-12 col-md-6">
        <h2 class="mt-md-5">Kinik Ar-Rahman Medika</h2>
        <p class="py-3">{!! substr($clinics[0]->description, 0, 201) !!}...</p>
        <a href="{{ route('about') }}">Selengkapnya</a>
      </div>
      <div class="col-12 col-md-6 mt-5 mt-md-0">
        <img class="img-fluid" src="gambar/heroabout.png" alt="about">
      </div>
    </div>
  </div>


  <!--layanan-->
  <div class="layanan container mt-5 pt-5">
    <div class="row">
      <h1 class="text-center"><font>Layanan <span style="color: #555">&</span> Fasilitas</font> 
      </h1>
      <p class="text-center">Memberikan pelayanan terbaik dengan menyediakan Fasilitas dan
        Layanan yang lengkap dan profesional</p>

      <div class="col d-flex justify-content-center mt-3">
        <div id="carouselExampleCaptions" class="carousel slide w-50" data-bs-ride="carousel">
          {{-- <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
          </div> --}}
          
          <div class="carousel-inner">
            <div class="carousel-item active">
              <a href="/layanan#{{ $services[3]->route }} ">
                <img src="{{ $services[3]->image }}" class=" w-100" alt="...">
                <div class="bg-img">
                  <div class="carousel-caption d-none  d-md-block">
                    <p class="title">{{ $services[3]->service_name }}</p>
                    <p>{{ substr($services[3]->detail, 0, 100) }}...</p>
                  </div>
                </div>
              </a>
            </div>
           
            @foreach ($services as $service)
              <div class="carousel-item">
                <a href="/layanan#{{ $service->route }}">

                  <img src="{{ $service->image }}" class=" w-100" alt="...">
                  <div class="bg-img">
                    <div class="carousel-caption d-none d-md-block">
                      <p class="title">{{ $service->service_name }}</p>
                      <p>{{ substr($service->detail, 0, 100) }}...</p>
                    </div>
                  </div>
                 </a> 
              </div>
            @endforeach
            
            @foreach ($facilities as $facility)
                <div class="carousel-item">
                  <a href="/fasilitas#{{ $facility->route }}">
                    <img src="{{ $facility->image }}" class=" w-100" alt="...">
                    <div class="bg-img">
                      <div class="carousel-caption  d-md-block">
                        <p class="title">{{ $facility->facility_name }}</p>
                        <p>{{ substr($facility->detail, 0, 100) }}...</p>
                      </div>
                    </div>
                  </a>
                </div>
            @endforeach
          </div>

          <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>
      </div>

      {{-- <div class="col d-flex justify-content-center">
        
      </div> --}}

      {{-- <div class="col d-flex justify-content-center">
        <div>
          <div>
            @foreach ($services as $service) 
              <div class="mt-4 mt-md-0 mb-5">
                <div class="boximg-1">
                  <img src="{{ $service->image }}" alt="">
                  <div class="box">
                    <h2>{{ $service->service_name }}</h2>
                    <p style="width: 250px;">{!! substr($service->mini_detail, 0, 50) !!}...</p>
                  </div>
                </div>
              </div>
            @endforeach

          </div>
        </div>
      </div>
      <div class="col d-flex justify-content-center">
        <div>
          @foreach ($facilities as $facility)
              
            <div class="mt-4 mt-md-5">
              <div class="boximg-2">
                <img src="{{ $facility->image }}" alt="">
                <div class="box">
                  <h2>{{ $facility->facility_name }}</h2>
                  <p style="width: 250px;">{!! $facility->mini_detail !!}...</p>
                </div>
              </div>
            </div>
          @endforeach

        </div>
      </div> --}}
    </div>

    <div class="button text-center mt-5">
      <a href="{{ route('layanan') }}">Lihat Semua</a>
    </div>
  </div>


  <!--testimoni-->
  <div class="testimoni container-fluid mt-5 pt-5" id="testimoni">
    <div class="row">
      <div class="col-12 col-md-5 p-0">
        <img class="img-fluid" src="gambar/testiimg.png" alt="">
      </div>
      <div class="col-12 col-md-7 judul px-4 px-md-0">
        <h1>Testimonial dari <font>Pasien Kami</font>
        </h1> <br>
        <p>Kirim Masukan Anda <a href="{{ route('user-add-feedback') }}">Klik disini</a></p>
          <div class="wrap row row-cols-1 row-cols-md-2 mt-5">
            
          <div class="owl-testimoni owl-carousel owl-theme">
              @foreach ($feedbacks as $feedback)
              <div class="slide col px-3">
                <div class="boxslide">
                  <img class="icon" src="gambar/testitext.png" alt="">
                  <div class="texttesti">
                    <p>{{ substr($feedback->message, 0, 30) }} ...</p>
                    <div class="starimg ms-4 d-flex">
                      <?php for ($i=1; $i <= $feedback->rate; $i++) : ?>
                        <img src="gambar/startesti.png" alt="startesti" width="25">
                      <?php endfor; ?>
                    </div>
                    <div class="pasien">
                      <h1>{{ $feedback->patient }} | <font>Pasien</font>
                      </h1>
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div>
      </div>
    </div>
  </div>

  <!-- certifikatdoktor -->
  <div class="certifikatdoktor container mt-5 pt-md-5 px-4 px-md-0" id="certified-doctor">
    <div class="row">
      <div class="col-12 text-center"> 
      <p class="title">Dokter <span style="color: #0B7125">Kami</span></p>
      </div>
      <div class="col-12 mt-3 mt-md-5">
        <div class="owl-dokter owl-carousel owl-theme">
          @foreach ($doctors as $doctor)
            <div class="ms-5">
              <img class="img-fluid " src="{{ $doctor->image }}" alt="">
              <div class="ket-doctor text-center" style="color: #555">
                <p class="doctor-name mt-3">{{ $doctor->doctor_name }}</p>
                <p class="doctor-role">{{ $doctor->role }}</p>
                <div class="detail">
                  <p>{!! $doctor->detail !!}</p>
                </div>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>

  <!--galerry-->
  <section class="galeri" id="documentation">
    <div class="row">
      <div class="col">
        <div class="container">
          <h2>GALERI</h2>
          <h1>Dokumentasi Klinik</h1>
          <div class="owl-galeri owl-carousel owl-theme mt-md-5">
            @foreach ($galleries as $gallery)
              <div class="ms-5 d-flex justify-content-center">
                <img src="{{ $gallery->image }}" alt="">
              </div> 
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </section>

  <!--location-->
  <div class="location container mt-5 pt-md-5 px-4 px-md-0">
    <div class="row">
      <div class="col-12 mb-5 text-center">
        <h1 class="label">Lokasi <font>Klinik</font>
      </div>
      
      <div class="col-12 col-md-6">
        <img class="img-fluid" src="{{ $profiles->image }}" alt="">
      </div>
      <div class="desk col-12 col-md-6">
        <h1>{{ $profiles->clinic_name }}</h1>
        <p>{!! substr($profiles->description, 0, 105) !!}</p>
        <div class="button mt-5">
          <a href="{{ route('lokasi-medika') }}">Selengkapnya</a>
        </div>
      </div>
      <div class="desk col-12 col-md-6 mt-md-5 d-none d-md-block">
        <h1>{{ $clinics[0]->clinic_name }}</h1>
        <p>{!! $clinics[0]->description !!}</p>
        <div class="button mt-5">
          <a href="{{ route('lokasi-galuh') }}">Selengkapnya</a>
        </div>
      </div>
      <div class="col-12 col-md-6 mt-5">
        <img class="img-fluid" src="{{ $clinics[0]->image }}" alt="">
      </div>
      <div class="desk col-12 col-md-6 mt-md-5 d-md-none">
        <h1>{{ $clinics[0]->clinic_name }}</h1>
        <p>{!! $clinics[0]->description !!}</p>
        <div class="button mt-5">
          <a href="{{ route('lokasi-galuh') }}">Selengkapnya</a>
        </div>
      </div>
    </div>
  </div>

  <!--artikel-->
  <div class="artikel container-fluid mt-5 pt-md-5 px-4 px-md-0 pb-5">
    <div class="container">
      <div class="title">
        <h1>Artikel <font>Terbaru</font>
        </h1>
      </div>

      <div class="row mt-5 ">
        @foreach ($articles as $article)
          <div class="col py-3 d-flex justify-content-center">
            <a href="/artikel/{{ $article->title }}">
              <div class="card-1">
                <div class="bg-img">
                  <img src="{{ $article->image }}" class="card-img-top" alt="...">
                </div>
                <div class="card-body">
                  <p style="height:auto;" class="subtitle">Cianjur, 26 oktober 2022 - <span style="color: #0B7125; font-weight: 500; text-transform: capitalize;">{{ $article->username }}</span></p>
                  <a href="/artikel/{{ $article->title }}">
                    <h5 class="card-title" style="width: 294px;" >{{ $article->title }}</h5>
                  </a>
                  <div class="card-text mb-5">
                    <p>{!! substr($article->detail, 0, 20) !!}...</p>
                  </div>
                  <a href="/artikel/{{ $article->title }}" class="card-link mb-5">Selengkapnya</a>
                </div>
              </div>
            </a>
          </div>
        @endforeach
      </div>
    </div>

    <div class="button mb-5">
      <a href="{{ route('list-artikel') }}">Lihat Semua</a>
    </div>
  </div>

  <x-user-footer></x-user-footer>


  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
  </script>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="owl.carousel.js"></script>
  <script>
    $(document).ready(function(){
      $('.owl-hero').owlCarousel({
      loop: true,
      margin:10,
      nav:true,
      responsive: {
        200: {
          items: 1,
          nav: true,
        },
        600: {
          items: 3,
          nav: true,
        },
        1000: {
          items: 1,
          nav: true,
          loop: true,
        }
      }
    });
    
      $('.owl-sponsor').owlCarousel({
      loop: true,
      margin:10,
      nav:true,
      responsive: {
        0: {
          items: 1,
          nav: true,
        },
        600: {
          items: 2,
          nav: true,
        },
        1000: {
          items:5,
          nav: true,
          loop: true,
        }
      }
    });
    
    $('.owl-testimoni').owlCarousel({
      loop: true,
      margin:10,
      nav:true,
      responsive: {
        0: {
          items: 1,
          nav: true,
        },
        600: {
          items: 2,
          nav: true,
        },
        1000: {
          items: 2,
          nav: true,
          loop: true,
        }
      }
    });

    $('.owl-dokter').owlCarousel({
      loop: true,
      margin:10,
      nav:true,
      responsive: {
        0: {
          items: 1,
          nav: true,
        },
        600: {
          items: 2,
          nav: true,
        },
        1000: {
          items: 4,
          nav: true,
          loop: true,
        }
      }
    });
    
    $('.owl-galeri').owlCarousel({
      loop: true,
      margin:10,
      nav:true,
      responsive: {
        0: {
          items: 1,
          nav: true,
        },
        600: {
          items: 2,
          nav: true,
        },
        1000: {
          items: 4,
          nav: true,
          loop: true,
        }
      }
    });

    });

  </script>

  
  
  



</body>
</html>