<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="styles/about.css">
    <link rel="stylesheet" href="responsive/headernavbar.css">
    <link rel="stylesheet" href="responsive/footer.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="icon" href="/img/icon_logo-removebg-preview.png" type="image/png">

    <title>Tentang Klinik Arrahman</title>
  </head>
  <body>
  
  <x-user-header></x-user-header>

  <div class="container mt-5" style="background-color: aliceblue">
    <div class="row">
      <div class="col-xl-12">
        <div class="card p-5">
          <form action="" method="post">
              @csrf
              <h4 class="mb-5" style="color: #555">Kirim Masukan Anda!</h4>

              <label for="patient">Nama</label>
              <input type="text" name="patient" id="patient" class="form-control mb-4">
  
              <label for="message">Pesan</label>
              <textarea name="message" id="message" class="form-control mb-4" placeholder=""></textarea>
  
              <label for="rate-bintang">Rate Bintang (1-5)</label>
              <select name="rate" id="rate-bintang" class="form-control mb-4">
                  <option value="5">5</option>
                  <option value="4">4</option>
                  <option value="3">3</option>
                  <option value="2">2</option>
                  <option value="1">1</option>
              </select>
              
              <div class="button">
                {{-- <a href="#" id="tombol" class="btn btn-danger" style="display: none;">Reset Image</a> --}}
                <button type="reset" class="btn btn-danger">Reset Data</button>
                <button type="submit" class="btn btn-success ms-3" name="submit">Kirim</button>
              </div>
          </form>
        </div>
      </div>
    </div>
  </div>













  
  {{-- FOOTER --}}

  <x-user-footer></x-user-footer>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>