<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <title>Document</title>
</head>
<body>
  <!--footer-->
<div class="footer container-fluid mt-5 pt-md-5 px-4 px-md-0 pb-5">
  <div class="container py-5">
    <div class="row row-cols-1 row-cols-md-4">
      <div class="col contenfooter d-flex justify-content-center">
        <div>
          <div class="cardfooter ms-1 ms-md-0 me-md-5 d-block">
            <div class="logofooter">
              <img class="img" src="/gambar/logo.png" alt="">
            </div>

            <div class="imgiconfooter">
              <div class="imgfooter1">
                <img src="/gambar/locfooter.svg" alt="">
                <p>Alamat <br>
                  <font>Jl. Raya Puri, Jambe Timur.</font>
                </p>
              </div>

              <div class="imgfooter2">
                <img src="/gambar/call.svg" alt="">
                <p>Phone <br>
                  <font>(0267) 8458155</font>
                </p>
              </div>

              <div class="imgfooter3">
                <img src="/gambar/imgfooter.svg" alt="">
                <p>Work Time <br>
                  <font>Senin-Min : <label> 08.00 - 22.00</label></font>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>



      <div class="col contenfooter">
        <div class="menufooter mt-5 mt-md-0">
          <h1>Menu</h1>
          <li><a href="{{ route('/') }}">Home</a></li>
          <li><a href="{{ route('about') }}">About</a></li>
          <li><a href="{{ route('layanan') }}">Layanan</a></li>
          <li><a href="{{ route('lokasi-medika') }}">Lokasi</a></li>
          <li><a href="{{ route('kontak') }}">Kontak</a></li>
          <li><a href="{{ route('list-artikel') }}">Artikel</a></li>
        </div>
      </div>
      <div class="col contenfooter">
        <div class="menulink mt-5 mt-md-0">
          <h1>Quick Links</h1>
          <li><a href="/#certified-doctor">Doktor Bersertifikat</a></li>
          <li><a href="/#documentation">Galeri Klinik</a></li>
          <li><a href="/about#visi-misi">Visi-Misi</a></li>
        </div>
      </div>
      <div class="col contenfooter">
        <div class="menuhel mt-5 mt-md-0">
          <h1>Our Social Media</h1>
          @php
              $link = App\Models\Profile::select('*')->first();
          @endphp
          <div class="icon-sosial">
            <div class="icon-fb">
              <a href="{{ $link->facebook_link }}">
                <i class='fa fa-facebook-f'></i>
                <span class="text">Facebook</span>
              </a>
            </div>
            <div class="icon-instagram">
              <a href="{{  $link->instagram_link }}">
                <i class="fa fa-instagram" aria-hidden="true"></i>
                <span class="text">Instagram</span>
              </a>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>


<div class="footercopyright">
  <footer>© Copyright Klinik Ar-Rahman 2022. All right reserved. <br>
    Supported By <font> Madtive Studio</font>
  </footer>
</div>


<a href="https://api.whatsapp.com/send?phone=6281288051383&text=Halo!" class="whatsapp-icon" target="_blank" id="btnWhatsapp">
  <i class="fa fa-whatsapp" aria-hidden="true"></i>
</a>
<button onclick="topFunction()" id="myBtn" title="Go to top">
  <i class="fa fa-angle-up " aria-hidden="true"></i>
</button>
<script>
  //Get the button:
mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}
z
// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}
</script>
</body>
</html>