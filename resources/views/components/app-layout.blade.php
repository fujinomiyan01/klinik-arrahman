<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/> --}}

  

  

  <title>{{ $title }} | Admin</title>
  <!-- Favicon -->
  <link rel="icon" href="/img/icon_logo-removebg-preview.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="/assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Page plugins -->
  <!-- Argon CSS -->
  <link rel="stylesheet" href="/assets/css/argon.css?v=1.2.0" type="text/css">
  {{-- Data Tables --}}
  <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">

  {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap5.min.css"> --}}

  
  
  
  <style>
    .header, .navbar {
      background-color: #0B7125;
    }
    
    .button {
        /* margin-top: 2rem; */
        text-align: left;
    }

    .button button {
      background-color: #0B7125;
    }

    .button a:hover {
      background-color: #0B7125;
      color: #ccc;
    }

    .button a.merah:hover, .button button.merah:hover{
      background-color: red;
      color: #ccc;
    }

    .button button:hover {
      background-color: #0B7125;
      color: #ccc;
    }

    #edit {
      background-color: #0B7125;
      color: white;
      padding: 5px;
      border-radius: 5px;
    }

    #hapus {
      background-color: red;
      color: white;
      padding: 5px;
      border-radius: 5px;
    }
    
    .hijau-tua {
      background-color: #0B7125;
    }

    a.merah, button.merah {
      background-color: red;
    }

    #fileSelect {
      font-weight: bolder;
    }

    form #detail, form #message {
      height: 250px;
    }

    form #facebook_link, form #instagram_link, form #twitter_link, form #googleMap_link {
      height: 120px;
    }

  </style>
</head>
<body>
  <x-sidenav></x-sidenav>
  
  <!-- Main content -->
  <div class="main-content" id="panel">

  <!-- Topnav -->
  @extends('layouts.header')


 
 

<!-- Argon Scripts -->
<!-- Core -->
<script src="/assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="/assets/vendor/js-cookie/js.cookie.js"></script>
<script src="/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
<!-- Optional JS -->
<script src="/assets/vendor/chart.js/dist/Chart.min.js"></script>
<script src="/assets/vendor/chart.js/dist/Chart.extension.js"></script>
<!-- Argon JS -->
<script src="/assets/js/argon.js?v=1.2.0"></script>
{{-- Data Tables --}}

{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script> --}}

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap5.min.js"></script>

</body>
</html>

