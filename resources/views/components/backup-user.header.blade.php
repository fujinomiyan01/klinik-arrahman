<!--header-->
<div class="header container-fluid p-0">
    <div class="container">
      <div class="row p-0">
        <div class="col-6 p-0 d-flex align-items-center justify-content-start">
          <a class="navbar-brand" href="">
            <img class="logo" src="/gambar/logo.png" alt="logo">
          </a>
        </div>
        <div class="col-6 d-flex align-items-center justify-content-end p-0">
          <a href="#">
            <img class="iconjam" width="40" src="/gambar/logo1.png" alt="icon">
          </a>
          <p class="day">Sen-Min</p>
          <p class="text">08.00 - 22.00</p>
        </div>
      </div>
    </div>
  </div>
  
  {{-- NAVBAR --}}
  <nav class="navbar navbar-expand-lg navbar-light bg-white">
    <div class="container"> 
      <button class="navbar-toggler no_shadow" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav py-3 py-md-2">
          <li class="nav-item px-2">
            <a class="nav-link active" aria-current="page" href="{{ route('/') }}">Home</a>
          </li>
          <li class="nav-item px-2">
            <a class="nav-link active" aria-current="page" href="{{ route('about') }}">Tentang</a>
          </li>
          <li class="nav-item px-2">
            <div class="nav-item dropdown-layanan">
              <span>Layanan</span>
              <div class="dropdown-content">
                <div class="d-flex p-4">
                  <div class="item">
                    <a href="{{ route('layanan') }}">Layanan</a>
                    <hr>
                    @foreach(App\Models\Service::get() as $value)
                    <a href="/layanan#{{ $value->route }}">{{ $value->service_name }}</a>
                    @endforeach
                  </div>
                  <div class="item ms-5">
                    <a href="{{ route('fasilitas') }}">Fasilitas</a>
                    <hr>
  
                    @php $page = 2 @endphp
                    @php $gap = 4 @endphp
                    
                    @foreach(App\Models\Facility::get() as $value)
                    
                    @if($value->iteration > 3 && $value->iteration == $gap)
  
                      <a href="/fasilitas?page{{ $page }}=#{{ $value->route }}">{{ $value->facility_name }}</a>
                      @php $page++ @endphp
                      @php $gap += 4 @endphp
                    
                    @else
                      <a href="/fasilitas#{{ $value->route }} {{  request()->query('page') }}">{{ $value->facility_name }}</a>
                    @endif
  
                    @endforeach
                  </div>
                </div>
              
              </div>
            </div>
          </li>
          <li class="nav-item px-2">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Lokasi
              </a>
              <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <li><a class="dropdown-item" href="{{ route('lokasi-medika') }}">KLinik Ar-Rahman Medika</a></li>
                  <li><a class="dropdown-item" href="{{ route('lokasi-galuh') }}">KLinik Ar-Rahman Galuh</a></li>
              </ul>
            </li>
          </li>
          <li class="nav-item px-2">
            <a class="nav-link active" href="{{ route('kontak') }}">Kontak</a>
          </li>
          <li class="nav-item px-2">
            <a class="nav-link active" href="{{ route('list-artikel') }}">Artikel</a>
          </li>
        </ul>
      </div>
      
      <div class="icon-kontak d-flex">
        <li class="nav-item px-3 mt-2 mt-lg-0 d-flex align-items-center ms-lg-5">
          <a href=""><img width="17" src="/gambar/call.svg" alt="icon"></a>
          <a href="" class="ms-3" style="text-decoration: none; color: inherit;"><span>(0267) 8458155</span></a>
        </li>
        <li class="nav-item px-3 mt-3 mt-lg-0 d-flex align-items-center">
          <a href=""><img width="17" src="/gambar/map.svg" alt="icon"></a>
          <a href="" class="ms-3" style="text-decoration: none; color: inherit;"><span>Jl.Raya Puri, Jambe
              Timur.</span>
          </a>
        </li>
      </div>
    </div>
  </nav>