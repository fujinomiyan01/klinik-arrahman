<!-- Sidenav -->
<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header  align-items-center">
        <a class="navbar-brand" href="{{ route('index') }}">
          <img src="/img/logo.png" class="navbar-brand-img" alt="...">
        </a>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav mt-1">
            <li class="nav-item">
               <a class="nav-link active bg-white" href="{{ route('index') }}">
                <i class="ni ni-tv-2 text-primary"></i>
                <span class="nav-link-text">Home</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active bg-white" href="{{ route('register') }}">
               <i class="ni ni-single-02 text-primary"></i>
               <span class="nav-link-text">Register new Admin</span>
             </a>
           </li>
          </ul>

          <!-- Divider -->
          <hr class="my-3">
          <!-- Heading -->
          <h6 class="navbar-heading p-0 text-muted">
            <span class="docs-normal">Menu Tambah</span>
          </h6>

          <!-- Nav items -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link active bg-white" href="{{ route('profile') }}">
                <i class="ni ni-single-02 text-"></i>
                <span class="nav-link-text">Profil Klinik Medika</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active bg-white" href="{{ route('clinic') }}">
                <i class="ni ni-building text-success"></i>
                <span class="nav-link-text">Klinik</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active bg-white" href="{{ route('article') }}">
                <i class="ni ni-world-2 text-info"></i>
                <span class="nav-link-text">Artikel</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active bg-white" href="{{ route('articleComments') }}">
                <i class="ni ni-satisfied text-primary"></i>
                <span class="nav-link-text">Komentar-komentar artikel</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active bg-white" href="{{ route('feedback') }}">
                <i class="ni ni-like-2 text-info"></i>
                <span class="nav-link-text">Testimonial Pasien</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active bg-white" href="{{ route('doctor') }}">
                <i class="ni ni-badge text-default"></i>
                <span class="nav-link-text">Dokter</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active bg-white" href="{{ route('facility') }}">
                <i class="ni ni-building text-primary"></i>
                <span class="nav-link-text">Fasilitas</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active bg-white" href="{{ route('service') }}">
                <i class="ni ni-app text-success"></i>
                <span class="nav-link-text">Layanan</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active bg-white" href="{{ route('gallery') }}">
                <i class="ni ni-album-2 text-info"></i>
                <span class="nav-link-text">Gallery</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active bg-white" href="{{ route('category') }}">
                <i class="ni ni-tag text-warning"></i>
                <span class="nav-link-text">Kategori</span>
              </a>
            </li>
           
            
            
            
            <li class="nav-item">
              <a class="nav-link active bg-white" href="{{ route('sponsor') }}">
                <i class="ni ni-bell-55 text-success"></i>
                <span class="nav-link-text">Sponsor</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active bg-white" href="{{ route('clinicContact') }}">
                <i class="ni ni-email-83 text-warning"></i>
                <span class="nav-link-text">Kontak Cabang Klinik</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active bg-white" href="{{ route('heroContent') }}">
                <i class="ni ni-archive-2 text-success"></i>
                <span class="nav-link-text">Keunggulan Klinik Ar-Rahman (Hero Content)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active bg-white" href="{{ route('heroImage') }}">
                <i class="ni ni-album-2 text-default"></i>
                <span class="nav-link-text">Gambar Keunggulan Klinik Ar-Rahman (Hero Content)</span>
              </a>
            </li>
            
            
          </ul>

          <!-- Divider -->
          <hr class="my-3">
          <!-- Heading -->
          <h6 class="navbar-heading p-0 text-muted">
            <span class="docs-normal">Dashboard</span>
          </h6>
          
          <!-- Navigation -->
          <ul class="navbar-nav mb-md-3">
            <li class="nav-item">
              <a class="nav-link active bg-white" href="{{ route('/') }}">
                <i class="ni ni-books text-primary"></i>
                <span class="nav-link-text">Landing Page</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>