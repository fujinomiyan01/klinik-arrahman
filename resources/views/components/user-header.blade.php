<!--header-->
<div class="header container-fluid p-0">
  <div class="container">
    <div class="row p-0">
      <div class="col-6 p-0 d-flex align-items-center justify-content-start">
        <a class="navbar-brand" href="/">
          <img class="logo" src="/gambar/logo.png" alt="logo">
        </a>
      </div>
      <div class="col-6 d-flex align-items-center justify-content-end p-0 icon">
        <img class="iconjam d-none d-md-block" width="40" src="/gambar/logo1.png" alt="icon" >
        <p class="day d-none d-md-block">Sen-Min:</p>
        <p class="text d-none d-md-block">08.00 - 22.00</p>
      </div>
    </div>
  </div>
</div>

{{-- NAVBAR --}}
<nav class="navbar navbar-expand-lg navbar-light bg-white">
  <div class="container">
    <button class="navbar-toggler no_shadow" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
      aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav py-3 py-md-2">
        <li class="nav-item px-2">
          <a class="nav-link" aria-current="page" href="{{ route('/') }}">Home</a>
        </li>
        <li class="nav-item px-2">
          <a class="nav-link" href="{{ route('about') }}">Tentang</a>
        </li>
        <li class="nav-item px-2">
          <div class="nav-item dropdown-layanan">
            <span>Layanan</span>
            <div class="dropdown-content">
              <div class="d-flex p-4">
                <div class="item">
                  <p><a href="{{ route('layanan') }}">Layanan</a></p>
                  <hr>
                  @foreach(App\Models\Service::get() as $value)
                    @if($loop->index > 3)
                    <a href="/layanan?page=2#{{ $value->route }}">{{ $value->service_name }}</a>
                    @elseif($loop->index > 7)
                    <a href="/layanan?page=3#{{ $value->route }}">{{ $value->service_name }}</a>
                    @elseif($loop->index > 11)
                    <a href="/layanan?page=4#{{ $value->route }}">{{ $value->service_name }}</a>
                    @elseif($loop->index > 15)
                    <a href="/layanan?page=5#{{ $value->route }}">{{ $value->service_name }}</a>
                    @elseif($loop->index > 19)
                    <a href="/layanan?page=6#{{ $value->route }}">{{ $value->service_name }}</a>
                    @elseif($loop->index > 23)
                    <a href="/layanan?page=7#{{ $value->route }}">{{ $value->service_name }}</a>
                    @elseif($loop->index > 27)
                    <a href="/layanan?page=8#{{ $value->route }}">{{ $value->service_name }}</a>
                    @elseif($loop->index > 31)
                    <a href="/layanan?page=9#{{ $value->route }}">{{ $value->service_name }}</a>
                    @elseif($loop->index > 35)
                    <a href="/layanan?page=10#{{ $value->route }}">{{ $value->service_name }}</a>
                    @else
                      <a href="/layanan#{{ $value->route }}">{{ $value->service_name }}</a>
                    @endif
                  @endforeach
                </div>
                <div class="item ms-5">
                  <p><a href="{{ route('fasilitas') }}">Fasilitas</a></p>
                  <hr>
                  @foreach(App\Models\Facility::get() as $value)
                  
                    @if($loop->index > 3)
                    <a href="/fasilitas?page=2#{{ $value->route }}">{{ $value->facility_name }}</a>
                    @elseif($loop->index > 7)
                    <a href="/fasilitas?page=3#{{ $value->route }}">{{ $value->facility_name }}</a>
                    @elseif($loop->index > 11)
                    <a href="/fasilitas?page=4#{{ $value->route }}">{{ $value->facility_name }}</a>
                    @elseif($loop->index > 15)
                    <a href="/fasilitas?page=5#{{ $value->route }}">{{ $value->facility_name }}</a>
                    @elseif($loop->index > 19)
                    <a href="/fasilitas?page=6#{{ $value->route }}">{{ $value->facility_name }}</a>
                    @elseif($loop->index > 23)
                      <a href="/fasilitas?page=7#{{ $value->route }}">{{ $value->facility_name }}</a>
                    @elseif($loop->index > 27)
                      <a href="/fasilitas?page=8#{{ $value->route }}">{{ $value->facility_name }}</a>
                    @elseif($loop->index > 31)
                      <a href="/fasilitas?page=9#{{ $value->route }}">{{ $value->facility_name }}</a>
                    @elseif($loop->index > 35)
                      <a href="/fasilitas?page=10#{{ $value->route }}">{{ $value->facility_name }}</a>
                    @else
                      <a href="/fasilitas#{{ $value->route }}">{{ $value->facility_name }}</a>
                    @endif

                  @endforeach
                </div>
              </div>
            </div>
          </div>
        </li>
        <li class="nav-item px-2 dropdown ">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
          Lokasi
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <li><a class="dropdown-item" href="{{ route('lokasi-medika') }}">KLinik Ar-Rahman Medika</a></li>
            <li><a class="dropdown-item" href="{{ route('lokasi-galuh') }}">KLinik Ar-Rahman Galuh</a></li>
          </ul>
        </li>
        <li class="nav-item px-2">
          <a class="nav-link" href="{{ route('kontak') }}">Kontak</a>
        </li>
        <li class="nav-item px-2">
          <a class="nav-link" href="{{ route('list-artikel') }}">Artikel</a>
        </li>
        
        {{-- <li class="nav-item px-3 mt-2 mt-lg-0 d-flex align-items-center ms-lg-5">
          <a href="{{ route('kontak') }}" style="text-decoration: none">
            <img width="17" src="/gambar/call.svg" alt="icon">
            <span class="ms-3" style="text-decoration: none;;">(0267) 8458155</span>
          </a>
        </li>
        <li class="nav-item px-3 mt-3 mt-lg-0 d-flex align-items-center">
          @php
              $link = App\Models\Profile::select('*')->first();
          @endphp
          <a href="{{ $link->googleMap_link }}" style="text-decoration: none">
            <img width="17" src="/gambar/map.svg" alt="icon">
            <span class="ms-3" style="text-decoration: none;;">Jl.Raya Puri, Jambe
              Timur.</span>
          </a>
        </li> --}}
      </ul>
    </div>
    
    {{-- <div class="icon-kontak d-flex d-md-bocks">
      <li class="nav-item px-3 mt-2 mt-lg-0 d-flex align-items-center ms-lg-5">
        <a href="{{ route('kontak') }}">
          <img width="17" src="/gambar/call.svg" alt="icon">
          <span class="ms-3" style="text-decoration: none;;">(0267) 8458155</span>
        </a>
      </li>
      <li class="nav-item px-3 mt-3 mt-lg-0 d-flex align-items-center">
        @php
            $link = App\Models\Profile::select('*')->first();
        @endphp
        <a href="{{ $link->googleMap_link }}">
          <img width="17" src="/gambar/map.svg" alt="icon">
          <span class="ms-3" style="text-decoration: none;;">Jl.Raya Puri, Jambe
            Timur.</span>
        </a>
      </li>
    </div> --}}
    
  </div>
</nav>