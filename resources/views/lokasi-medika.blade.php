<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/styles.css">
    <link rel="stylesheet" href="styles/stylelokasi.css">
    <link rel="stylesheet" href="responsive/headernavbar.css">
    <link rel="stylesheet" href="responsive/footer.css">

    <link rel="icon" href="/img/icon_logo-removebg-preview.png" type="image/png">

    <title>Lokasi Medika</title>
</head>
<body>

    <x-user-header></x-user-header>

    <div class="conten">        
        <div class="container">
            <h1>Lokasi Klinik Ar-Rahman Medika</h1>
        </div>
    </div>

    <div class="gmap">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.4785044284595!2d107.30976341413871!3d-6.331997663717056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69762018b32a95%3A0x88aab747205f153f!2sKlinik%20Ar%20Rahman%20Medika!5e0!3m2!1sid!2sid!4v1643093003280!5m2!1sid!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>


    <div class="card-doctor container">
      

      <div class="row">
        <div class="title">
          <h1 class="mb-5">Tim <span style="color: #0B7125">Klinik Ar-Rahman Medika</span></h1>
        </div>

        @foreach ($doctors as $doctor)
          <div class="col text-center mb-5">
            <img src="{{ $doctor->image }}" alt="" style="border-radius: 50px 0;">
              <div class="ket-doctor mt-4">
                  <h3>{{ $doctor->doctor_name }}</h3>
                  <h5>{{ $doctor->role }}</h5>
                  <p>{!! $doctor->detail !!}</p>
              </div>
          </div>
        @endforeach
      </div>
    </div>

    {{-- <div class="card-doctor container">
      <div class="card-doctor-1" style="text-align: center">
          <img src="gambar/img/doctor-img-1.png" alt="">
          <div class="ket-doctor">
              <h3>Dr. Sigit Aryanto</h3>
              <h5>Ketua Yayasan Ar-Rahman Medika</h5>
              <h6>Tim : Klinik Ar-Rahman Medika & Klinik Ar-Rahman Galuh Mas</h6>
          </div>
      </div>
      <div class="card-doctor-2" style="text-align: center">
          <img src="gambar/img/doctor-img-1.png" alt="">
          <div class="ket-doctor">
              <h3>Dr. Dwi Susilo SH,MH</h3>
              <h5>Dokter Penanggung Jawab</h5>
              <h6>Tim : Klinik Ar-Rahman Medika & Klinik Ar-Rahman Galuh Mas</h6>
          </div>
      </div>
      <div class="card-doctor-3" style="text-align: center">
          <img src="gambar/img/doctor-img-1.png" alt="">
          <div class="ket-doctor">
              <h3>Dr. Ayu Delvira Putri</h3>
              <h5>Dokter Umum</h5>
              <h6>Tim : Klinik Ar-Rahman Medika</h6>
          </div>
      </div>
    </div>

    <div class="card-doctor container">
        <div class="card-doctor-1" style="text-align: center">
            <img src="gambar/img/doctor-img-1.png" alt="">
            <div class="ket-doctor">
                <h3>Dr. Chintia Nilna Muna</h3>
                <h5>Dokter Umum</h5>
                <h6>Tim : Klinik Ar-Rahman Medika & Klinik Ar-Rahman Galuh Mas</h6>
            </div>
        </div>
        <div class="card-doctor-2" style="text-align: center">
            <img src="gambar/img/doctor-img-1.png" alt="">
            <div class="ket-doctor">
                <h3>Dr. Onelola Dewita Karya, MARS</h3>
                <h5>Dokter Umum</h5>
                <h6>Tim : Klinik Ar-Rahman Medika </h6>
            </div>
        </div>
        <div class="card-doctor-3" style="text-align: center">
            <img src="gambar/img/doctor-img-1.png" alt="">
            <div class="ket-doctor">
                <h3>Drg. Rori Sasmita</h3>
                <h5>Dokter Gigi</h5>
                <h6>Tim : Klinik Ar-Rahman Medika</h6>
            </div>
        </div>
    </div> --}}
     
  <x-user-footer></x-user-footer>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</body>
</html>