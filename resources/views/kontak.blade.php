<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
    <link rel="stylesheet" href="/styles/styles.css">
    <link rel="stylesheet" href="/styles/stylelokasi.css">
    <link rel="stylesheet" href="/responsive/headernavbar.css">
    <link rel="stylesheet" href="/responsive/footer.css">
    <link rel="stylesheet" href="/styles/contact.css">

    <link rel="icon" href="/img/icon_logo-removebg-preview.png" type="image/png">

    <title>Kontak</title>
</head>
<body>
    
    <x-user-header></x-user-header>


    <div class="contact text-center">
        <h1>Kontak</h1>
    </div>
            
    <section class="conten-page">
        <div class="container">
            <div class="row p-3 py-5">
                <div class="col-md-6 d-flex justify-content-center">
                    <div class="conten-1">
                        <p class="title">Klinik <span>Ar-Rahman Medika</span></p>
                        <div class="img-1 mt-3">
                            <div class="img-contact">
                                <img src="gambar/icon/iconcontact3.svg" alt="">
                                <a href="">{{ str_replace('|', '', $profiles->phone); }}</a>
                            </div>
                            <div class="img-contact">
                                <img src="gambar/icon/iconcontact1.svg" alt="">
                                <a href="mailto:{{ $profiles->email }}">{{ $profiles->email }}</a>
                            </div>
                            <div class="img-contact">
                                <img src="gambar/icon/iconcontact2.svg" alt="">
                                <a href="{{ $profiles->googleMap_link }}" style="width: 70%">{{ $profiles->address }}</a>

                                <div class="icon ms-3 d-flex justify-content-end">
                                    <a href="{{ $profiles->facebook_link }}"><img src="gambar/icon/facebook.svg" alt=""></a>
                                    <a href="{{ $profiles->instagram_link }}"><img src="gambar/icon/instagram.svg" alt=""></a>
                                    {{-- <a href="{{ $profiles->twitter_link }}"><img src="gambar/icon/twitter.svg" alt=""></a> --}}
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6 d-flex justify-content-center">
                    <div class="conten-1">
                        <p class="title">Klinik <span>Ar-Rahman Galuh Mas</span></p>
                        <div class="img-1 mt-3">
                            <div class="img-contact">
                                <img src="gambar/icon/iconcontact3.svg" alt="">
                                <a href="">0{{ $clinicContact->phone }}</a>
                            </div>
                            <div class="img-contact">
                                <img src="gambar/icon/iconcontact1.svg" alt="">
                                <a href="mailto:{{ $clinicContact->email }}">{{ $clinicContact->email }}</a>
                            </div>
                            <div class="img-contact">
                                <img src="gambar/icon/iconcontact2.svg" alt="">
                                <a href="{{ $clinicContact->googleMap_link }}" style="width: 70%">{{ $clinicContact->address }}</a>

                                <div class="icon ms-3 d-flex justify-content-end">
                                    <a href="{{ $clinicContact->facebook_link }}"><img src="gambar/icon/facebook.svg" alt=""></a>
                                    <a href="{{ $clinicContact->instagram_link }}"><img src="gambar/icon/instagram.svg" alt=""></a>
                                    {{-- <a href="{{ $profiles->twitter_link }}"><img src="gambar/icon/twitter.svg" alt=""></a> --}}
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div> 
            </div>         
        </div>
    </section>

    <x-user-footer></x-user-footer>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>