<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="../styles/artikel.css">
    <link rel="stylesheet" href="../responsive/footer.css">
    <link rel="stylesheet" href="../responsive/headernavbar.css">
    <link rel="stylesheet" href="../styles/media-query.css">

    <link rel="icon" href="/img/icon_logo-removebg-preview.png" type="image/png">

    <title>Artikel</title>
</head>
<body>
  <x-user-header></x-user-header>

  <section class="articelkonten">
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <div class="pagekonten">
            @if(session()->has('success'))
              <div class="p-3 bg-uccess text-white" id="alert">{{ session()->get('success') }}</div>
            @endif
            <?php $tanggal = explode(' ', $articles->created_at) ?>
            <img src="{{ $articles->image }}" alt="" style="width: 100%; height: 550px;   border-radius: 0px 50px;">
            <p class="date">{{ $tanggal[0] }} - <span>{{ $articles->username }}</span></p>
            <h1>{{ $articles->title }}</h1>
            <h2>{!! $articles->detail !!}</h2>
  
            <hr>

            <?php $categories = explode(',', $articles->category_name); ?>
            {{-- @foreach ($articles as $article) --}}
              <div class="buttonbox">
                  @foreach ($categories as $category)
                      <a href="/list-artikel/{{ $category }}" <?php if($category === ''){ echo 'style="display: none;"'; } ?>>{{ $category }}</a>
                  @endforeach
              </div>
            {{-- @endforeach --}}
  
  
            <div class="row mt-5 py-5">
              <div class="col d-flex justify-content-start">
                <p style="font-weight: bold; color: ##0B7125;">{{ $articles->views }}x</p>
                <p class="ms-1">Dilihat</p>
              </div>

              <div class="col d-flex justify-content-end">
                <p class="title">Bagikan Artikel :</p>

                <div class="icon d-flex">
                  <a target="_blank" href="https://www.facebook.com/sharer.php?u={{ url('artikel') }}">
                    <img src="/gambar/icon/facebook.svg" alt="">
                  </a>
                  {{-- <a href="/instagram">
                    <img src="/gambar/icon/instagram.svg" alt="">
                  </a> --}}
                  <a target="_blank" href="https://twitter.com/intent/tweet?url={{ url('artikel') }}">
                    <img src="/gambar/icon/twitter.svg" alt="">
                  </a>
                </div>
              </div>
            </div>       
          </div>
        </div>

        <div class="col-md-3 p-3 px-3">
          <div class="pagepost">
            <h1>ARTIKEL POPULER</h1>

            @foreach ($popularArticles as $popularArticle)
              <div class="boxpost d-flex">
                <a href="/artikel/{{ $popularArticle->title }}" class="d-flex">
                  <img src="{{ $popularArticle->image }}" alt="">
                  <div class="detail">
                    <p class="title">{{ $popularArticle->title }}</p>
                    <p>{!! substr($popularArticle->detail, 0,60) !!}</p>
                  </div>
                </a>
              </div>
            @endforeach
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-9">
           {{-- Komen artikel user --}}
           <div class="container py-5" id="form-comment">
            <div class="row">
              <div class="col">
                <div class="card p-5 mb-5">
                  <h5 style="color: #555">Tinggalkan Komentar!</h5>
                  <form action="" method="post" class="mt-5">
                    @csrf
                    <input type="hidden" name="article_id">
                    <div class="row">
                     
                      <div class="col">
                        {{-- <div class="d-flex px-"> --}}
                          <div class="row">
                            <div class="col">
                              <input type="text" name="name" id="name" class="form-control mb-4" placeholder="Nama">
                            </div>
                            <div class="col">
                              <input type="text" name="email" id="email" class="form-control mb-4" placeholder="Email">
                            </div>
                          </div>
                        {{-- </div> --}}

                        <textarea name="message" id="message" class="form-control mb-4" placeholder="Pesan..." style="height: 150px"></textarea>
                        
                        <input type="hidden" name="judul" id="judul" value="{{ $articles->title }}">
                    
                        {{-- <label for="message">Message</label> --}}

                        @if(count($errors) > 0)
                          @foreach ($errors->all() as $error)
                              <li id="error" style="display: none;">{{ $error }}</li>
                              <script>
                                var error = document.getElementById('error').innerHTML;
                                alert(error);
                              </script>
                          @endforeach
                        @endif 
                            

                        {!! NoCaptcha::renderJs('id', false, 'onloadCallback') !!}
                        {!! NoCaptcha::display() !!}

                        <input type="hidden" name="judulArtikel" value="{{ $articles->title }}">
                      
                      </div>

                      <button type="submit" class="btn btn-primary mt-4">Kirim!</button>
                      
                    </div>
                  </form>
                </div>
              </div>
            </div>
            
            <div class="view py-3">
              <i class="fa fa-comments ms-3" aria-hidden="true">
                <span class="ms-1">( {{  count($activeArticleComments)}} )  </span>
              </i>
            </div>
            
            @foreach ($articleComments as $articleComment)
              <div class="card comment-card p-3 mb-3" id="{{ $articleComment->comment_status }}">
                <p class="name">{{ $articleComment->name }}</p>
                <p class="time">{{ $articleComment->created_at }}</p>
                <p class="message mt-1">{{ $articleComment->message }}</p>
              </div>
              {{-- <p>{{ $articleComment->name }}|{{ $articleComment->email }}|{{ $articleComment->message }}</p> --}}
            @endforeach
          </div>
        </div>
      </div>

      
    </div>
  </section>
  
  <x-user-footer></x-user-footer>

  <script type="text/javascript">
      var onloadCallback = function() {
        alert("grecaptcha is ready!");
      };
  </script>

 
  <script>
    var time = document.getElementById("alert");

    setTimeout(function(){
      time.style.display = "none";
    }, 10000);
  </script>

  
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
