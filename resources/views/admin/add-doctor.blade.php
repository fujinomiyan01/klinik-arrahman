<x-app-layout title="Add Doctor"></x-app-layout>
<link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
@if(count($errors) > 0)
  <div class="alert alert-danger" id="alert">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<div class="container mt-3">
    <div class="row">
      <div class="col-xl-12">
        <div class="card p-5">
           <form action="" method="post" enctype="multipart/form-data">
              @csrf
              <label for="doctor_name">Nama Dokter</label>
              <input type="text" name="doctor_name" id="doctor_name" class="form-control mb-4" placeholder="Masukkan Nama Dokter">
  
              <label for="role">Keahlian atau Jabatan</label>
              <input type="text" name="role" id="role" class="form-control mb-4" placeholder="Keahlian atau Jabatan">
            
              <label for="workplace">Klinik Tempat Bertugas</label>  
              <select name="workplace[]" id="workplace" multiple class="chosen-select form-control mb-4">
                  <option value="Klinik Ar-Rahman Medika">Klinik Ar-Rahman Medika</option>
                @foreach ($clinics as $clinic)
                  <option value="{{ $clinic->clinic_name }}">{{ $clinic->clinic_name }}</option>
                @endforeach
              </select>
              <br><br>

              <input type="file" id="file" style="display: none;" name="image">
              <a href="#" id="fileSelect">Pilih Gambar</a>
              <div id="fileDisplay">
                  <p>Gambar Belum Dipilih</p>
              </div>

              <x-button></x-button>
          </form>
        </div>
      </div>
    </div> 
  </div>
<script type="text/javascript" src="/assets/js/previewImage.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
<script>
  $(".chosen-select").chosen({
    no_results_text: "Oops, nothing found!"
  })
</script>