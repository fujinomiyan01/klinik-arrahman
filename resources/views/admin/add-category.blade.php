<x-app-layout title="Add Category"></x-app-layout>
@if(count($errors) > 0)
  <div class="alert alert-danger" id="alert">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<div class="container mt-3">
  <div class="row">
    <div class="col-xl-12">
      <div class="card p-5">
        <form action="" method="post">
            @csrf
            <label for="category_name">Nama Kategori</label>
            <input type="text" name="category_name" id="category_name" class="form-control mb-4" placeholder="Masukkan Nama Kategori">
         
            <x-button></x-button>
        </form>
      </div>
    </div>
  </div>
</div>