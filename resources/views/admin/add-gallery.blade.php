<x-app-layout title="Add Gallery"></x-app-layout>
@if(count($errors) > 0)
  <div class="alert alert-danger" id="alert">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<div class="container mt-3">
    <div class="row">
        <div class="col-xl-12">
            <div class="card p-5">
                <form action="" method="post" enctype="multipart/form-data">
                    @csrf
                    <label for="name">Nama</label>
                    <input type="text" name="name" id="name" class="form-control mb-4">

                    <input type="file" id="file" style="display: none;" name="image">
                    <a href="#" id="fileSelect">Pilih Gambar</a>
                    <div id="fileDisplay">
                        <p>Gambar Belum Dipilih</p>
                    </div>

                    {{-- <br>
                    <img alt="Gambar" id="uploadPreview" width="100" />
                    <br>
                    <input type="file" name="image" id="image" class="mb-4" id="uploadImage" onchange="img_pathUrl(this);" /> --}}

                    <x-button></x-button>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/assets/js/previewImage.js"></script>