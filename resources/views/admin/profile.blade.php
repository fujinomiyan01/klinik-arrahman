<x-app-layout title="Available Profile"></x-app-layout>

@if(session()->has('success'))
    <div class="p-3 bg-success text-white" id="alert">{{ session()->get('success') }}</div>
@endif
@if(session()->has('delete'))
    <div class="p-3 bg-danger text-white" id="alert">{{ session()->get('delete') }}</div>
@endif
@if(count($errors) > 0)
  <div class="alert alert-danger" id="alert">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<div class="container mt-3">
  <div class="row">
    <div class="col-xl-12">
      <div class="card p-5">
        <h1 class="text-center mb-5">DATA PROFIL</h1>

        <form action="" method="post">
            @csrf
            <label for="clinic_name">Nama Klinik</label>
            <input type="text" name="clinic_name" id="clinic_name" class="form-control mb-4" placeholder="Masukkan Nama Klinik">

            <label for="clinics">Klinik-Klinik Terdaftar</label>
            <input type="text" name="clinics" id="clinics" class="form-control mb-4" placeholder="Masukkan Nama Klinik">

            <label for="vision">Visi</label>
            <input type="text" name="vision" id="vision" class="form-control mb-4" placeholder="Masukkan Visi Klinik">

            <label for="mission">Misi</label>
            <input type="text" name="mission" id="mission" class="form-control mb-4" placeholder="Masukkan Misi Klinik">

            <label for="facebook">Facebook</label>
            <input type="text" name="facebook" id="facebook" class="form-control mb-4" placeholder="Masukkan Nama Facebook Klinik">

            <label for="instagram">Instagram</label>
            <input type="text" name="instagram" id="instagram" class="form-control mb-4" placeholder="Masukkan Nama Instagram Klinik">

            <label for="twitter">Twitter</label>
            <input type="text" name="twitter" id="twitter" class="form-control mb-4" placeholder="Masukkan Nama Twitter Klinik">
            
            <label for="address">Lokasi Klinik</label>
            <input type="text" name="address" id="address" class="form-control mb-4" placeholder="Masukkan Alamat atau Lokasi Klinik">

            <label for="googleMap_link">Link Google Map Klinik</label>
            <input type="text" name="googleMap_link" id="googleMap_link" class="form-control mb-4" placeholder="Masukkan Alamat atau Lokasi Klinik">

            <label for="phone">Nomor Telepon</label>
            <input type="text" name="phone" id="phone" class="form-control mb-4" placeholder="+62.......">

            <label for="wk1">Waktu Kerja Klinik</label>
            <div class="row mb-4">
              <div class="col">
                <input type="number" name="work_time1" id="wk1" placeholder="Dari... (05.00)" class="form-control">
              </div>
              <div class="col">
                <input type="number" name="work_time3" id="wk3" placeholder="Sampai... (15.00)" class="form-control">
              </div>
              <input type="hidden" value=" - " name="work_time2" id="wk2">
            </div>
           
            <x-button></x-button>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  var time = document.getElementById("alert");

  setTimeout(function(){
    time.style.display = "none";
  }, 10000);
</script>

{{-- <div class="container mt-3">
  <div class="row">
    <div class="col-xl-12">
      <div class="card p-5">
        <h1 class="text-center mb-5">DATA PROFIL</h1>
          <div class="table-responsive">
              <table class="table table-bordered table-hover yajra-datatable">
                  <thead class="table-success">
                      <tr>
                          <th>Id</th>
                          <th>Id Klinik</th>
                          <th>Nama Klinik</th>  
                          <th>Visi</th>
                          <th>Misi</th>
                          <th>Alamat</th>
                          <th>No Telpon</th>
                          <th>Waktu Kerja</th>
                          <th>Aksi</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
              </table>

              <script type="text/javascript">
                  $(function () {
                  
                  var table = $('.yajra-datatable').DataTable({
                      processing: true,
                      serverSide: true,
                      ajax: "{{ route('admin.profile') }}",
                      columns: [
                          {data: 'id', name: 'id'},
                          {data: 'clinic_id', name: 'clinic_id'},
                          {data: 'clinic_name', name: 'detail'},
                          {data: 'vision', name: 'vision'},
                          {data: 'mission', name: 'mission'},
                          {data: 'address', name: 'address'},
                          {data: 'phone', name: 'phone'},
                          {data: 'work_time', name: 'work_time'}, 
                          {
                              data: 'action', 
                              name: 'action', 
                              orderable: true, 
                              searchable: true
                          },
                      ]
                  });
                  
                  });
              </script>  
          </div>    
      </div>
    </div>
  </div>
</div> --}}

{{-- <div class="container card border-black-1">
  <table border="1" cellspacing="0">
      <tr>
          <th>Id Klinik</th> 
          <th>Nama Klinik</th> 
          <th>Visi</th>
          <th>Misi</th>
          <th>Alamat Klinik</th>
          <th>Nomor Telepon</th>
          <th>Waktu Kerja</th>
          <th>Aksi</th>
      </tr>
      @foreach ($profiles as $profile)
        <tr>
            <td>{{$profile->clinic_id}}</td>
            <td>{{$profile->clinic_name}}</td>
            <td>{{$profile->vision}}</td>
            <td>{{$profile->mission}}</td>
            <td>{{$profile->address}}</td>
            <td>{{$profile->phone}}</td>
            <td>{{$profile->work_time}}</td>
            <td><a href="/admin/profile/edit/{{ $profile->id }}">Edit</a> | <a href="/admin/profile/{{ $profile->id }}">Hapus</a></td>
        </tr>
      @endforeach
  </table>
</div> --}}