<x-app-layout title="Edit Feedback"></x-app-layout>

@if(session()->has('success'))
    <div class="p-3 bg-success text-white">{{ session()->get('success') }}</div>
@endif
@if(count($errors) > 0)
  <div class="alert alert-danger" id="alert">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<div class="container card" style="width: 80%; margin: 20px auto; padding: 20px;">
  <form action="" method="post">
    @csrf
    <label for="patient">Nama Pasien</label>
    <br>
    <input class="form-control mb-4" type="text" name="patient" id="patient" value="{{ $feedbacks->patient }}">
    <br>

    <label for="message">Pesan</label>
    <br>
    <textarea class="form-control mb-4" name="message" id="message" value="{{ $feedbacks->message }}"></textarea>
    
    <label for="rate-bintang">Rate bintang</label>
    <br>
    <select class="form-control mb-4" name="rate" id="rate-bintang" value="{{ $feedbacks->rate }}">
        @switch($feedbacks->rate)
            @case($feedback->rate == 5)
                <option value="5">5</option>
                <option value="4">4</option>
                <option value="3">3</option>
                <option value="2">2</option>
                <option value="1">1</option>
                @break
            @case($feedback->rate == 4)
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="3">3</option>
                <option value="2">2</option>
                <option value="1">1</option>
                @break
            @case($feedback->rate == 3)
                <option value="3">3</option>
                <option value="5">5</option>
                <option value="4">4</option>
                <option value="2">2</option>
                <option value="1">1</option>
            @break
            @case($feedback->rate == 2)
                <option value="2">2</option>
                <option value="5">5</option>
                <option value="4">4</option>
                <option value="3">3</option>
                <option value="1">1</option>
            @break
            @case($feedback->rate == 2)
                <option value="1">1</option>
                <option value="5">5</option>
                <option value="4">4</option>
                <option value="3">3</option>
                <option value="2">2</option>
            @break
            @default
                <option value="5">5</option>
                <option value="4">4</option>
                <option value="3">3</option>
                <option value="2">2</option>
                <option value="1">1</option>
        @endswitch
    </select>
    
    <x-button></x-button>
  </form>
</div>
