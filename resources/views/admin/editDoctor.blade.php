<x-app-layout title="Edit Doctor"></x-app-layout>
<link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
{{-- <script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script> --}}

@if(session()->has('success'))
    <div class="p-3 bg-success text-white">{{ session()->get('success') }}</div>
@endif
@if(count($errors) > 0)
  <div class="alert alert-danger" id="alert">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<div class="container mt-3">
  <div class="row">
    <div class="col-xl-12">
      <div class="card p-5">
        <form action="" method="post" enctype="multipart/form-data">
            @csrf
            <label for="doctor_name">Nama Dokter</label>
            <input type="text" name="doctor_name" id="doctor_name" class="form-control mb-4" placeholder="Nama Fasilitas" value="{{ $doctors->doctor_name }}">

            <label for="role">Jabatan atau keahlian</label>
            <input type="text" name="role" id="role" class="form-control mb-4" placeholder="Masukkan Detail" value="{{ $doctors->role }}">

            <?php $workplace = explode('&',$doctors->detail) ?>
            <label for="workplace">Klinik Tempat Bertugas</label>  
            <select name="workplace[]" id="workplace" multiple class="chosen-select form-control mb-4">
                <option value="Klinik Ar-Rahman Medika" <?php if(in_array("Klinik Ar-Rahman Medika", $workplace)) {
                  echo "selected";
                } ?>>Klinik Ar-Rahman Medika</option>
              @foreach ($clinics as $clinic)
                <option value="{{ $clinic->clinic_name }}" <?php if (in_array($clinic->clinic_name, $workplace)) {
                  echo "selected";
                } ?>>{{ $clinic->clinic_name }}</option>
              @endforeach
            </select>
            <br><br>

            <label for="file">Gambar Sebelumnya | <a href="#" id="fileSelect">Pilih Gambar Baru</a></label>
            <br>
            <img src="{{ $doctors->image }}" alt="{{ $doctors->doctor_name }}" width="200">
            <input type="file" name="image" id="file" style="display: none;">
            
            <br><br>
            <label>Gambar Baru</label>
            <div id="fileDisplay" style="margin-top: 15px;">
                <p>Gambar Baru Belum Dipilih</p>
            </div>
            
            <x-button></x-button>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="/assets/js/previewImage.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
<script>
  $(".chosen-select").chosen({
    no_results_text: "Oops, nothing found!"
  })
</script>
{{-- <script>
    ClassicEditor
        .create( document.querySelector( '#detail' ) )
        .catch( error => {
            console.error( error );
        } );
</script> --}}
{{-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> --}}