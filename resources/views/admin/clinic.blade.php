<x-app-layout title="Available Clinic"></x-app-layout>

@if(session()->has('success'))
    <div class="p-3 bg-success text-white" id="alert">{{ session()->get('success') }}</div>
@endif
@if(session()->has('delete'))
    <div class="p-3 bg-danger text-white" id="alert">{{ session()->get('delete') }}</div>
@endif

<div class="container mt-3">
  <div class="row">
    <div class="col-xl-12">
      <div class="card p-5">
        <div class="row d-flex">
          <div class="col">
            <h1 class="">DATA KLINIK</h1>
          </div>
          <div class="col d-flex justify-content-end">
            <div class="button">
                <a href="{{ route('add-clinic') }}" class="btn btn-primary hijau-tua">Tambah Data</a>
            </div>
          </div>
        </div>



        
        <div class="table-responsive mt-5">
          <table class="table table-bordered table-hover yajra-datatable">
            <thead class="table-success">
                <tr>
                    <th>Id</th>
                    <th>Tanggal Data Dibuat</th>
                    <th>Nama Klinik</th>
                    <th>Tentang Klinik</th>
                    <th>Dokter Tersedia</th>
                    <th>Gambar Klinik</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
          </table>

          <script type="text/javascript">
            $(function () {
              
              var table = $('.yajra-datatable').DataTable({
                  processing: true,
                  serverSide: true,
                  ajax: "{{ route('admin.clinic') }}",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'tanggal', name: 'tanggal'},
                      {data: 'clinic_name', name: 'clinic_name'},
                      {data: 'desc', name: 'desc'},
                      {data: 'doctors', name: 'doctors'},
                      {data: 'image', name: 'image'},
                      {
                          data: 'action', 
                          name: 'action', 
                          orderable: true, 
                          searchable: true
                      },
                  ]
              });
              
            });
          </script>  
        </div>  
      </div>
    </div>
  </div>
</div>
<script>
  var time = document.getElementById("alert");

  setTimeout(function(){
    time.style.display = "none";
  }, 10000);
</script>
