<x-app-layout title="Edit Hero Content"></x-app-layout>
@if(count($errors) > 0)
  <div class="alert alert-danger" id="alert">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif
@if(session()->has('success'))
    <div class="p-3 bg-uccess text-white" id="alert">{{ session()->get('success') }}</div>
@endif
@if(session()->has('delete'))
    <div class="p-3 bg-danger text-white" id="alert">{{ session()->get('delete') }}</div>
@endif

<div class="container mt-3">
    <div class="row">
        <div class="col-xl-12">
            <div class="card p-5">
                <form action="" method="post" enctype="multipart/form-data">
                    @csrf
                    <label for="title">Judul Hero Content</label>
                    <input type="text" name="title" id="title" class="form-control mb-4" value="{{ $heroContentEdit->title }}">

                    <label for="detail">Detail</label>
                    <textarea name="detail" id="detail" class="form-control mb-4">{{ $heroContentEdit->detail }}</textarea>

                    {{-- <label for="file">Gambar Sebelumnya | <a href="#" id="fileSelect">Pilih Gambar Baru</a></label>
                    <br>
                    <img src="{{ $heroContentEdit->image }}" alt="{{ $heroContentEdit->title }}" width="200">
                    <input type="file" name="image" id="file" style="display: none;"> --}}
                    
                    {{-- <br><br>
                    <label>Gambar Baru</label>
                    <div id="fileDisplay" style="margin-top: 15px;">
                        <p>Gambar Baru Belum Dipilih</p>
                    </div> --}}

                    <x-button></x-button>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/assets/js/previewImage.js"></script>
<script>
    var time = document.getElementById("alert");
  
    setTimeout(function(){
      time.style.display = "none";
    }, 10000);
</script>