<x-app-layout title="Available Feedback"></x-app-layout>

@if(session()->has('success'))
    <div class="p-3 bg-success text-white" id="alert">{{ session()->get('success') }}</div>
@endif
@if(session()->has('delete'))
    <div class="p-3 bg-danger text-white" id="alert">{{ session()->get('delete') }}</div>
@endif

<div class="container mt-3">
  <div class="row">
    <div class="col-xl-12">
      <div class="card p-5">
        <div class="row d-flex">
          <div class="col">
            <h1 class="">DATA FEEDBACK</h1>
          </div>
          <div class="col d-flex justify-content-end">
            <div class="button">
                <a href="{{ route('add-feedback') }}" class="btn btn-primary hijau-tua">Tambah Data</a>
            </div>
          </div>
        </div>

        <div class="table-responsive mt-5">
          <table class="table table-bordered table-hover yajra-datatable">
            <thead class="table-success">
                <tr>
                    <th>Id</th>
                    <th>Tanggal Data Dibuat</th>
                    <th>Isi Pesan</th>
                    <th>Penilaian</th>  
                    <th>Nama Pasien</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
          </table>

          <script type="text/javascript">
            $(function () {
              
              var table = $('.yajra-datatable').DataTable({
                  processing: true,
                  serverSide: true,
                  ajax: "{{ route('admin.feedback') }}",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'tanggal', name: 'tanggal'},
                      {data: 'message', name: 'message'},
                      {data: 'rate', name: 'rate'},
                      {data: 'patient', name: 'patient'},
                      {
                          data: 'action', 
                          name: 'action', 
                          orderable: true, 
                          searchable: true
                      },
                  ]
              });
              
            });
          </script>  
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  var time = document.getElementById("alert");

  setTimeout(function(){
    time.style.display = "none";
  }, 10000);
</script>