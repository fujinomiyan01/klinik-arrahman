<x-app-layout title="Edit Category"></x-app-layout>

@if(session()->has('success'))
    <div class="p-3 bg-success text-white">{{ session()->get('success') }}</div>
@endif
@if(count($errors) > 0)
  <div class="alert alert-danger" id="alert">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<div class="container mt-3">
  <div class="row">
    <div class="col-xl-12">
      <div class="card p-5">
        <form action="" method="post">
          @csrf
          <label for="category_name">Nama Pasien</label>
          <br>
          <input type="text" class="form-control" name="category_name" id="category_name" value="{{ $categories->category_name }}">
          
          <x-button></x-button>
        </form>
      </div>
    </div>
  </div>
</div>
