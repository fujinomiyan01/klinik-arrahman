<x-app-layout title="Add Clinic"></x-app-layout>
<link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>

@if(count($errors) > 0)
  <div class="alert alert-danger" id="alert">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<div class="container mt-3">
  <div class="row">
    <div class="col-xl-12">
      <div class="card p-5">
        <form action="" method="post" enctype="multipart/form-data">
            @csrf
            <label for="clinic_name">Nama Klinik</label>
            <input type="text" name="clinic_name" id="clinic_name" class="form-control mb-4" placeholder="Masukkan Nama Klinik">

            <label for="description">Deskripsi Klinik</label>
            <textarea name="description" id="description" class="form-control mb-4" placeholder="Masukkan detail mengenai klinik (Ukuran menyesuaikan dengan isi)"></textarea>
            <br><br>

            <label for="doctors">Dokter Yang Ada/Tersedia</label>
            <br>
            <select name="doctors[]" id="doctors" multiple class="chosen-select form-control mb-4">
              @foreach ($doctors as $doctor)
                  <option value="{{ $doctor->doctor_name }}">{{$doctor->doctor_name}}</option>
              @endforeach
            </select>
            <br><br>

            <input type="file" id="file" style="display: none;" name="image">
            <a href="#" id="fileSelect">Pilih Gambar</a>
            <div id="fileDisplay">
                <p>Gambar Belum Dipilih</p>
            </div>

            <x-button></x-button>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="/assets/js/previewImage.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
<script>
  $(".chosen-select").chosen({
    no_results_text: "Oops, nothing found!"
  })
</script>
<script>
    ClassicEditor
        .create( document.querySelector( '#description' ) )
        .catch( error => {
            console.error( error );
        } );
</script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>