<x-app-layout title="Add Hero Content"></x-app-layout>
@if(count($errors) > 0)
  <div class="alert alert-danger" id="alert">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<div class="container mt-3">
    <div class="row">
        <div class="col-xl-12">
            <div class="card p-5">
                <form action="" method="post" enctype="multipart/form-data">
                    @csrf
                    <label for="title">Judul Hero Content</label>
                    <input type="text" name="title" id="title" class="form-control mb-4">

                    <label for="detail">Detail</label>
                    <textarea name="detail" id="detail" class="form-control mb-4"></textarea>

                    {{-- <input type="file" id="file" style="display: none;" name="image">
                    <a href="#" id="fileSelect">Pilih Gambar</a>
                    <div id="fileDisplay">
                        <p>Gambar Belum Dipilih</p>
                    </div> --}}

                    <x-button></x-button>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/assets/js/previewImage.js"></script>