<x-app-layout title="Add Clinic"></x-app-layout>
@if(session()->has('success'))
    <div class="p-3 bg-success text-white" id="alert">{{ session()->get('success') }}</div>
@endif
@if(count($errors) > 0)
  <div class="alert alert-danger" id="alert">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<div class="container mt-3">
  <div class="row">
    <div class="col-xl-12">
      <div class="card p-5">
         <form action="" method="post" enctype="multipart/form-data">
            @csrf
            <label for="clinic_id">Nomor Klinik</label>
            <input type="number" name="clinic_id" id="clinic_id" class="form-control mb-4" placeholder="Nomor Klinik">

            <label for="doctor_id">Nomor Dokter</label>
            <input type="number" name="doctor_id" id="doctor_id" class="form-control mb-4" placeholder="Nomor Doktor">

            <div class="button">
              <button type="submit" class="btn btn-primary" name="submit">Kirim</button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>


<div class="container mt-3">
  <div class="row">
    <div class="col-xl-12">
      <div class="card p-5">
        <h1 class="text-center mb-5">DATA KLINIK DOKTOR</h1>

        <div class="table-responsive">
          <table class="table table-bordered table-hover yajra-datatable">
            <thead class="table-success">
                <tr>
                    <th>Id</th>
                    <th>Id Klinik</th>
                    <th>Id Doctor</th>  
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
          </table>

          <script type="text/javascript">
            $(function () {
              
              var table = $('.yajra-datatable').DataTable({
                  processing: true,
                  serverSide: true,
                  ajax: "{{ route('admin.doctorClinic') }}",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'clinic_id', name: 'clinic_id'},
                      {data: 'doctor_id', name: 'doctor_id'},
                      {
                          data: 'action', 
                          name: 'action', 
                          orderable: true, 
                          searchable: true
                      },
                  ]
              });
              
            });
          </script>  
        </div>
      </div>
    </div>
  </div>
</div>
{{-- <div>
  <ul>
    @foreach ($doctors as $doctor)
        <li>{{ dd($doctor->doctor()->getRelated()->getFillable()->get('doctor_id')) }}</li>
    @endforeach
  </ul>
</div> --}}
<script>
  var time = document.getElementById("alert");

  setTimeout(function(){
    time.style.display = "none";
  }, 10000);
</script>

{{-- <div class="container card" style="width: 80%; margin: 100px auto; padding: 20px; text-align: center;">
  <table class="border-black-1" border="1" cellspacing="0">
    <tr>
      <th>Id Klinik</th> 
      <th>Id Doctor</th> 
      <th>Aksi</th>
    </tr>
    @foreach ($doctorClinics as $doctorClinic)
    <tr>
      <td>{{$doctorClinic->clinic_id}}</td>
      <td>{{$doctorClinic->doctor_id}}</td>
      <td><a href="/admin/doctorClinic/edit/{{ $doctorClinic->id }}">Edit</a> | <a href="/admin/doctorClinic/{{ $doctorClinic->id }}">Hapus</a></td>
    </tr>
    @endforeach
  </table>
</div>
<br> --}}