<x-app-layout title="Available Article Comments"></x-app-layout>

@if(session()->has('success'))
    <div class="p-3 bg-uccess text-white" id="alert">{{ session()->get('success') }}</div>
@endif
@if(session()->has('delete'))
    <div class="p-3 bg-danger text-white" id="alert">{{ session()->get('delete') }}</div>
@endif
@if(count($articleComments) > $lastComment)
  <div class="p-3 bg-danger text-white" id="alert">Ada data baru yang mungkin belum anda baca</div>
@endif
{{-- <div class="p-3 bg-success text-white">
  <p id="index">Jumlah komentar saat ini : {{ count($articles) }}</p>
  <p>Refresh untuk melihat jumlah terbaru jika ada data baru yang telah ditambahkan</p>
</div> --}}

<div class="container mt-3">
  <div class="row">
    <div class="col-xl-12">
      <div class="card p-5">
        <div class="row d-flex">
          <div class="col">
            <h1 class="">DATA SEMUA KOMENTAR</h1>
            
          </div>
          <div class="col d-flex justify-content-end">
            <div class="button">
                <a href="{{ route('add-articleComments') }}" class="btn btn-primary hijau-tua">Tambah Data</a>
            </div>
          </div>
        </div>
        
        <div class="table-responsive mt-5">
          <table class="table table-bordered table-hover yajra-datatable">
            <thead class="table-success">
                <tr>
                    <th>Id</th>
                    <th>Tanggal Data Dibuat</th>
                    <th>Judul Artikel yang dikomentari</th>
                    <th>Nama User</th>
                    <th>Email User</th>
                    <th>Pesan</th>
                    <th>Status Komentar</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
          {{-- <script>
            var $element = document.getElementById('index');
            
            var index = 1;
            index++;

            var $element.innerHTML = index;
          </script> --}}
          <script type="text/javascript">
            $(function () {
              var table = $('.yajra-datatable').DataTable({
                  processing: true,
                  serverSide: true,
                  ajax: "{{ route('admin.articleComments') }}",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'tanggal', name: 'tanggal'},
                      {data: 'article_title', name: 'article_title'},
                      {data: 'name', name: 'name'},
                      {data: 'email', name: 'email'},
                      {data: 'message', name: 'message'},
                      {data: 'comment_status', name: 'comment_status'},
                      {
                          data: 'action', 
                          name: 'action', 
                          orderable: true, 
                          searchable: true
                      },
                  ],
                  columnDefs: [{
                      targets: 1,
                      render: function (data, type, row) {
                          return type === 'display' && data.length > 50 ? data.substr(0, 50) + '…' : data;
                      }
                  }]
              });
            });
          </script> 
        </div> 
      </div>
    </div>
  </div>
</div>

<script>
  var time = document.getElementById("alert");

  setTimeout(function(){
    time.style.display = "none";
  }, 10000);
</script>
