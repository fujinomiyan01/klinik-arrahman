<x-app-layout title="Edit Doctor"></x-app-layout>

@if(session()->has('success'))
    <div class="p-3 bg-success text-white">{{ session()->get('success') }}</div>
@endif
@if(count($errors) > 0)
  <div class="alert alert-danger" id="alert">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<div class="container mt-3">
    <div class="row">
      <div class="col-xl-12">
        <div class="card p-5">
          <form action="" method="post">
              @csrf
              <label for="clinic_id">Nama Dokter</label>
              <input type="number" name="clinic_id" id="clinic_id" class="form-control mb-4" placeholder="Nama Fasilitas" value="{{ $doctorClinic->clinic_id }}">

              <label for="doctor_id">Jabatan atau keahlian</label>
              <input type="number" name="doctor_id" id="doctor_id" class="form-control mb-4" placeholder="Masukkan Detail" value="{{ $doctorClinic->doctor_id }}">
  
              <div class="button">
                <button type="submit" class="btn btn-primary" name="submit">Kirim</button>
              </div>
          </form>
        </div>
      </div>
    </div>
  </div>
