<x-app-layout title="Available Facility"></x-app-layout>

@if(session()->has('success'))
    <div class="p-3 bg-success text-white" id="alert">{{ session()->get('success') }}</div>
@endif
@if(session()->has('delete'))
    <div class="p-3 bg-danger text-white" id="alert">{{ session()->get('delete') }}</div>
@endif

<div class="container mt-3">
  <div class="row">
    <div class="col-xl-12">
      <div class="card p-5">
        <div class="row d-flex">
          <div class="col">
            <h1 class="">DATA FASILITAS</h1>
          </div>
          <div class="col d-flex justify-content-end">
            <div class="button">
                <a href="{{ route('add-facility') }}" class="btn btn-primary hijau-tua">Tambah Data</a>
            </div>
          </div>
        </div>

        <div class="table-responsive mt-5">
          <table class="table table-bordered table-hover yajra-datatable">
            <thead class="table-success">
                <tr>
                    <th>Id</th>
                    <th>Tanggal Data Dibuat</th>
                    <th>Nama Fasilitas</th>
                    <th>Mini Detail</th>
                    <th>Detail Fasilitas</th>  
                    <th>Gambar</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
          </table>

          <script type="text/javascript">
            $(function () {
              
              var table = $('.yajra-datatable').DataTable({
                  processing: true,
                  serverSide: true,
                  ajax: "{{ route('admin.facility') }}",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'tanggal', name: 'tanggal'},
                      {data: 'facility_name', name: 'facility_name'},
                      {data: 'mini_detail', name: 'mini_detail'},
                      {data: 'desc', name: 'desc'},
                      {data: 'image', name: 'image'},
                      {
                          data: 'action', 
                          name: 'action', 
                          orderable: true, 
                          searchable: true
                      },
                  ]
              });
              
            });
          </script> 
        </div>      
      </div>
    </div>
  </div>
</div>
<script>
  var time = document.getElementById("alert");

  setTimeout(function(){
    time.style.display = "none";
  }, 10000);
</script>





{{-- <div class="container card" style="width: 80%; margin: 100px auto; padding: 20px; text-align: center;">
  <table class="border-black-1" border="1" cellspacing="0">
    <tr>
      <th>Nama Fasilitas</th> 
      <th>Keterangan Fasilitas</th> 
      <th>Gambar Fasilitas</th>
      <th>Aksi</th>
    </tr>
    @foreach ($facilities as $facility)
    <tr>
      <td>{{$facility->facility_name}}</td>
      <td>{{$facility->detail}}</td>
      <td><img src="{{$facility->image}}" width="100" style="margin: 0 auto; padding: 10px;"></td>
      <td><a href="/admin/facility/edit/{{ $facility->id }}">Edit</a> | <a href="/admin/facility/hapus/{{ $facility->id }}">Delete</a></td>
    </tr>
    @endforeach
  </table>
  <br><br>
  {{ $facilities->links() }}
</div> --}}


