<x-app-layout title="Edit Article"></x-app-layout>
<link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>

@if(session()->has('success'))
    <div class="p-3 bg-success text-white">{{ session()->get('success') }}</div>
@endif
@if(count($errors) > 0)
  <div class="alert alert-danger" id="alert">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<div class="container mt-3">
  <div class="row">
    <div class="col-xl-12">
      <div class="card p-5">
        <form action="" method="post" class="mt-3">
            @csrf
            <label for="name">Nama User</label>
            <input type="text" name="name" id="name" class="form-control mb-4" placeholder="Nama User" value="{{ $article_comment->name }}">

            <label for="email">Email User</label>
            <input type="text" name="email" id="email" class="form-control mb-4" placeholder="Email User" value="{{ $article_comment->email }}">

            <label for="article_title">Judul Artikel yang Dikomentari</label>
            <select name="article_title" id="article_title" class="form-control mb-4">
                @foreach ($article_titles as $article_title)
                    <option value="{{ $article_title->title }}" <?php if($article_title->title == $article_comment->article_title){ echo "selected"; } ?>>{{ $article_title->title }}</option>
                @endforeach
            </select>

            <label for="message">Pesan User</label>
            <textarea name="message" id="message" class="form-control mb-4" placeholder="Pesan...">{{ $article_comment->message }}</textarea>

            <label for="comment_status">Status Komentar (Aktif atau disembunyikan)</label>
            <select name="comment_status" id="comment_status" class="form-control mb-4">
                <option value="active" <?php if("active" == $article_comment->comment_status){ echo "selected"; } ?> >Active</option>
                <option value="hidden" <?php if("hidden" == $article_comment->comment_status){ echo "selected"; } ?> >Hidden</option>
            </select>
            
            {!! NoCaptcha::renderJs('id', false, 'onloadCallback') !!}
            {!! NoCaptcha::display() !!}            
            <x-button></x-button>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="/assets/js/previewImage.js"></script>