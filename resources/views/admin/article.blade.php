<x-app-layout title="Available Article"></x-app-layout>

@if(session()->has('success'))
    <div class="p-3 bg-uccess text-white" id="alert">{{ session()->get('success') }}</div>
@endif
@if(session()->has('delete'))
    <div class="p-3 bg-danger text-white" id="alert">{{ session()->get('delete') }}</div>
@endif

<div class="container mt-3">
  <div class="row">
    <div class="col-xl-12">
      <div class="card p-5">
        <div class="row d-flex">
          <div class="col">
            <h1 class="">DATA ARTIKEL</h1>
          </div>
          <div class="col d-flex justify-content-end">
            <div class="button">
                <a href="{{ route('add-article') }}" class="btn btn-primary hijau-tua">Tambah Data</a>
            </div>
          </div>
        </div>
        
        <div class="table-responsive mt-5">
          <table class="table table-bordered table-hover yajra-datatable">
            <thead class="table-success">
                <tr>
                    <th>Id</th>
                    <th>Tanggal Data Dibuat</th>
                    <th>Nama Admin</th>
                    <th>Judul Artikel</th>
                    <th>Gambar</th>
                    <th>Detail Artikel</th>
                    <th>Kategori Artikel</th>
                    {{-- <th>Jumlah Pelihat Artikel</th> --}}
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
          </table>

          <script type="text/javascript">
            $(function () {
              
              var table = $('.yajra-datatable').DataTable({
                  processing: true,
                  serverSide: true,
                  ajax: "{{ route('admin.article') }}",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'tanggal', name: 'tanggal'},
                      {data: 'username', name: 'username'},
                      {data: 'title', name: 'title'},
                      {data: 'image', name: 'image'},
                      {data: 'desc', name: 'desc'},
                      {data: 'category_name', name: 'category_name'},
                      // {data: 'views', name: 'views'},
                      {
                          data: 'action', 
                          name: 'action', 
                          orderable: true, 
                          searchable: true
                      },
                  ],
                  columnDefs: [{
                      targets: 1,
                      render: function (data, type, row) {
                          return type === 'display' && data.length > 50 ? data.substr(0, 50) + '…' : data;
                      }
                  }]
              });
              
            });
          </script> 
        </div> 
      </div>
    </div>
  </div>
</div>

<script>
  var time = document.getElementById("alert");

  setTimeout(function(){
    time.style.display = "none";
  }, 10000);
</script>













{{-- <div class="container card" style="width: 100%; margin: 100px auto; padding: 20px; text-align: center;">
    <table class="border-black-1" border="1" cellspacing="0">
      <tr>
        <th>Penulis Artikel</th> 
        <th>Judul Artikel</th> 
        <th>Keterangan Artikel</th>
        <th>Kategori</th>
        <th>Aksi</th>
      </tr>
      @foreach ($articles as $article)
      <tr>
        <td>{{$article->username}}</td>
        <td>{{$article->title}}</td>
        <td>{{$article->detail}}</td>
        <td>{{$article->category_name}}</td>
        <x-edit-delete></x-edit-delete>
      </tr>
      @endforeach
    </table>
   
    {{ $articles->links() }}
</div> --}}
