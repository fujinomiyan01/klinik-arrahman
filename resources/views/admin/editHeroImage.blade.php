<x-app-layout title="Edit Hero Image"></x-app-layout>

@if(session()->has('success'))
    <div class="p-3 bg-success text-white">{{ session()->get('success') }}</div>
@endif
@if(count($errors) > 0)
  <div class="alert alert-danger" id="alert">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<div class="container mt-3">
    <div class="row">
      <div class="col-xl-12">
        <div class="card p-5">
          <form action="" method="post" enctype="multipart/form-data">
              @csrf
              <label for="file">Gambar Sebelumnya | <a href="#" id="fileSelect">Pilih Gambar Baru</a></label>
              <br>
              <img src="{{ $heroImage->image }}" width="200">
              <input type="file" name="image" id="file" style="display: none;">
              
              <br><br>
              <label>Gambar Baru</label>
              <div id="fileDisplay" style="margin-top: 15px;">
                  <p>Gambar Baru Belum Dipilih</p>
              </div>
              
              <x-button></x-button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="/assets/js/previewImage.js"></script>
