<x-app-layout title="Available Clinic Contact"></x-app-layout>

@if(session()->has('success'))
    <div class="p-3 bg-success text-white" id="alert">{{ session()->get('success') }}</div>
@endif
@if(session()->has('delete'))
    <div class="p-3 bg-danger text-white" id="alert">{{ session()->get('delete') }}</div>
@endif

<div class="container mt-3">
  <div class="row">
    <div class="col-xl-12">
      <div class="card p-5">
        <div class="row d-flex">
          <div class="col">
            <h1 class="">DATA KLINIK</h1>
          </div>
          <div class="col d-flex justify-content-end">
            <div class="button">
                <a href="{{ route('add-clinicContact') }}" class="btn btn-primary hijau-tua">Tambah Data</a>
            </div>
          </div>
        </div>

        <div class="table-responsive mt-5">
          <table class="table table-bordered table-hover yajra-datatable">
            <thead class="table-success">
                <tr>
                    <th>Id</th>
                    <th>Tanggal Data Dibuat</th>
                    <th>Nama Klinik</th>
                    <th>Nama Facebook Klinik</th>
                    <th>Link Facebook</th>
                    <th>Nama Instagram Klinik</th>
                    <th>Link Instagram</th>
                    <th>Nama Twitter Klinik</th>
                    <th>Link Twitter</th>
                    <th>Alamat Klinik</th>
                    <th>Link Google Map</th>
                    <th>Nomor Telepon</th>
                    <th>Email</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
          </table>

          <script type="text/javascript">
            $(function () {
              
              var table = $('.yajra-datatable').DataTable({
                  processing: true,
                  serverSide: true,
                  ajax: "{{ route('admin.clinicContact') }}",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'tanggal', name: 'tanggal'},
                      {data: 'clinic_name', name: 'clinic_name'},
                      {data: 'facebook_name', name: 'facebook_name'},
                      {data: 'facebook_link', name: 'facebook_link'},
                      {data: 'instagram_name', name: 'instagram_name'},
                      {data: 'instagram_link', name: 'instagram_link'},
                      {data: 'twitter_name', name: 'twitter_name'},
                      {data: 'twitter_link', name: 'twitter_link'},
                      {data: 'address', name: 'address'},
                      {data: 'googleMap_link', name: 'googleMap_link'},
                      {data: 'telepon', name: 'telepon'},
                      {data: 'email', name: 'email'},
                      {
                          data: 'action', 
                          name: 'action', 
                          orderable: true, 
                          searchable: true
                      },
                  ]
              });
              
            });
          </script>  
        </div>  
      </div>
    </div>
  </div>
</div>
<script>
  var time = document.getElementById("alert");

  setTimeout(function(){
    time.style.display = "none";
  }, 10000);
</script>
