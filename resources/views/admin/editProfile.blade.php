<x-app-layout title="Edit Profile"></x-app-layout>
<link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>

@if(session()->has('success'))
    <div class="p-3 bg-success text-white">{{ session()->get('success') }}</div>
@endif
@if(count($errors) > 0)
  <div class="alert alert-danger" id="alert">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<div class="container mt-3">
  <div class="row">
    <div class="col-xl-12">
      <div class="card p-5">
        <form action="" method="post" enctype="multipart/form-data">
          @csrf
          <label for="clinic_name">Nama Klinik</label>
          <br>
          <input type="text" class="form-control mb-4" name="clinic_name" id="clinic_name" value="{{ $profiles->clinic_name }}">

          <label for="description">Deskripsi Klinik</label>
          <br>
          <textarea class="form-control mb-4" name="description" id="description" style="height: 200px">{{ $profiles->description }}</textarea>

          <?php $doctorClinic = explode('|', $profiles->doctors); ?>
          <label for="doctors">Dokter Yang Ada/Tersedia</label>
          <br>
          <select name="doctors[]" id="doctors" multiple class="chosen-select form-control mb-4">
            @foreach ($doctors as $doctor)
                <option value="{{ $doctor->doctor_name }}" <?php if (in_array($doctor->doctor_name, $doctorClinic)) {
                  echo "selected";
                } ?> >{{$doctor->doctor_name}}</option>
            @endforeach
          </select>
          <br><br>

          <label for="vision">Visi Klinik</label>
          <br>
          <input type="text" class="form-control mb-4" name="vision" id="vision" value="{{ $profiles->vision }}">
    
          <label for="mission">Misi Klinik</label>
          <br>
          <textarea class="form-control mb-4" name="mission" id="mission" value="{{ $profiles->mission }}"></textarea>
          <br><br>
    
          <label for="facebook_name">Nama Facebook</label>
          <br>
          <input type="text" class="form-control mb-4" name="facebook_name" id="facebook_name" value="{{ $profiles->facebook_name }}">

          <label for="facebook_link">Link Facebook</label>
          <br>
          <input type="text" class="form-control mb-4" name="facebook_link" id="facebook_link" value="{{ $profiles->facebook_link }}">
    
          <label for="instagram_name">Nama Instagram</label>
          <br>
          <input type="text" class="form-control mb-4" name="instagram_name" id="instagram_name" value="{{ $profiles->instagram_name }}">

          <label for="instagram_link">Link Instagram</label>
          <br>
          <input type="text" class="form-control mb-4" name="instagram_link" id="instagram_link" value="{{ $profiles->instagram_link }}">
    
          <label for="twitter_name">Nama Twitter</label>
          <br>
          <input type="text" class="form-control mb-4" name="twitter_name" id="twitter_name" value="{{ $profiles->twitter_name }}">

          <label for="twitter_link">Link Twitter</label>
          <br>
          <input type="text" class="form-control mb-4" name="twitter_link" id="twitter_link" value="{{ $profiles->twitter_link }}">
    
          <label for="address">Alamat Klinik</label>
          <br>
          <textarea class="form-control mb-4" name="address" id="address">{{ $profiles->address }}</textarea>
    
          <label for="googleMap_link">Link Google Map Klinik</label>
          <br>
          <textarea class="form-control mb-4" name="googleMap_link" id="googleMap_link">{{ $profiles->googleMap_link }}</textarea>
          
          <?php $phones = explode(" ",$profiles->phone); ?>
          <label for="phone">Nomor Telepon atau Kontak Klinik</label>
          <div class="row mb-4">
            <div class="col">
              <input type="text" class="form-control mb-4" name="phone1" id="phone1" value="{{ $phones[0] }}">
            </div>
              <p>{{ $phones[1] }}</p>
            <div class="col">
              <input type="text" class="form-control mb-4" name="phone2" id="phone2" value="{{ $phones[2] }}">
            </div>
            <input type="hidden" name="phone3" value="{{ $phones[1] }}">
          </div>

          <label for="email">Email Klinik</label>
        <input type="text" class="form-control mb-4" name="email" id="email" value="{{ $profiles->email }}">e
          
          <?php 
              $waktuKerja = explode(' ' ,$profiles->work_time);
              $hariKerja = explode(' ' ,$profiles->work_days);
          ?>

          <label for="work_days">Hari Kerja Klinik</label>
          <div class="row mb-4">
            <div class="col">
              <input type="text" class="form-control mb-4" name="work_days1" id="work_time" value="{{ $hariKerja[0] }}">
            </div>
            <input type="hidden" name="work_days3" value="{{ $hariKerja[1] }}">
              <p>{{ $hariKerja[1] }}</p>
            <div class="col">
              <input type="text" class="form-control mb-4" name="work_days2" id="work_time" value="{{ $hariKerja[2] }}">
            </div>
            <input type="hidden" name="work_days3" value="{{ $hariKerja[1] }}">
          </div>
    
          <label for="work_time">Jam Kerja Klinik</label>
          <div class="row mb-4">
            <div class="col">
              <input type="text" class="form-control mb-4" name="work_time1" id="work_time" value="{{ $waktuKerja[0] }}">
            </div>
              <p>{{ $waktuKerja[1] }}</p>
            <div class="col">
              <input type="text" class="form-control mb-4" name="work_time2" id="work_time" value="{{ $waktuKerja[2] }}">
            </div>
            <input type="hidden" name="work_time3" value="{{ $waktuKerja[1] }}">
          </div>

          <input type="hidden" name="value-mission" id="value-mission" value="{{ $profiles->mission }}">

          <label for="file">Gambar Sebelumnya | <a href="#" id="fileSelect">Pilih Gambar Baru</a></label>
          <br>
          <img src="{{ $profiles->image }}" alt="{{ $profiles->clinic_name }}" width="200">
          <input type="file" name="image" id="file" style="display: none;">
            
          <br><br>
          <label>Gambar Baru</label>
          <div id="fileDisplay" style="margin-top: 15px;">
              <p>Gambar Baru Belum Dipilih</p>
          </div>
          <x-button></x-button>
      </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="/assets/js/previewImage.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
<script>
  $(".chosen-select").chosen({
    no_results_text: "Oops, nothing found!"
  })
</script>
<script>
    var inputValue = document.getElementById('value-mission').value;
</script>
<script>
    ClassicEditor
        .create( document.querySelector( '#mission' ) )
        .then(function (mission) {
          mission.setData(inputValue);
        })
        .catch( error => {
            console.error( error );
        } );
</script>
<script>
  var time = document.getElementById("alert");

  setTimeout(function(){
    time.style.display = "none";
  }, 10000);
</script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>