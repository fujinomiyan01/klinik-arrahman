<x-app-layout title="Available Service"></x-app-layout>

@if(session()->has('success'))
    <div class="p-3 bg-success text-white" id="alert">{{ session()->get('success') }}</div>
@endif
@if(session()->has('delete'))
    <div class="p-3 bg-danger text-white" id="alert">{{ session()->get('delete') }}</div>
@endif

<div class="container mt-3">
    <div class="row">
      <div class="col-xl-12">
        <div class="card p-5">
            <div class="row d-flex">
                <div class="col">
                  <h1 class="">DATA LAYANAN</h1>
                </div>
                <div class="col d-flex justify-content-end">
                  <div class="button">
                      <a href="{{ route('add-service') }}" class="btn btn-primary hijau-tua">Tambah Data</a>
                  </div>
                </div>
              </div>

            <div class="table-responsive mt-5">
                <table class="table table-bordered table-hover yajra-datatable">
                    <thead class="table-success">
                        <tr>
                            <th>Id</th>
                            <th>Tanggal Data Dibuat</th>
                            <th>Nama Layanan</th>
                            <th>Mini Detail</th>
                            <th>Detail Layanan</th>  
                            <th>Gambar</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
  
                <script type="text/javascript">
                    $(function () {
                    
                    var table = $('.yajra-datatable').DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: "{{ route('admin.service') }}",
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'tanggal', name: 'tanggal'},
                            {data: 'service_name', name: 'service_name'},
                            {data: 'mini_detail', name: 'mini_detail'},
                            {data: 'desc', name: 'desc'},
                            {data: 'image', name: 'image'},

                            {
                                data: 'action', 
                                name: 'action', 
                                orderable: true, 
                                searchable: true
                            },
                        ]
                    });
                    
                    });
                </script>  
            </div>    
        </div>
      </div>
    </div>
</div>
<script>
    var time = document.getElementById("alert");
  
    setTimeout(function(){
      time.style.display = "none";
    }, 10000);
  </script>


{{-- <div class="container card" style="width: 80%; margin: 20px auto; padding: 20px">
    <table border="1" cellspacing="0">
        <tr>
            <th>Nama Layanan</th> 
            <th>Tentang Dokter</th> 
            <th>Gambar Fasilitas</th>
            <th>Aksi</th>
        </tr>
        @foreach ($services as $service)
        <tr>
            <td>{{$service->service_name}}</td>
            <td>{{$service->detail}}</td>
            <td><img src="{{$service->image}}" width="100" style="margin: 0 auto; padding: 10px;"></td>
            <td><a href="/admin/service/edit/{{ $service->id }}" id="edit">Edit</a> | <a href="/admin/service/{{ $service->id }}" id="hapus">Hapus</a></td> 
        </tr>
        @endforeach
    </table>
    <br><br>
    {{ $services->links() }}
</div> --}}
