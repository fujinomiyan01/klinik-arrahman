<x-app-layout title="Add Service"></x-app-layout>
{{-- <meta name="csrf-token" content="{{ csrf_token() }}"> --}}
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
@if(count($errors) > 0)
  <div class="alert alert-danger" id="alert">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<div class="container mt-3">
    <div class="row">
        <div class="col-xl-12">
            <div class="card p-5">
                <form action="" method="post" enctype="multipart/form-data">
                    @csrf
                    <label for="service_name">Nama Layanan</label>
                    <input type="text" name="service_name" id="service_name" class="form-control mb-4" placeholder="Masukkan Nama Layanan">

                    <label for="mini_detail">Mini Detail</label>
                    <input type="text" name="mini_detail" id="mini_detail" class="form-control mb-4" placeholder="Masukkan Detail">

                    <label for="detail">Detail</label>
                    <textarea name="detail" id="detail" class="form-control mb-4" placeholder="Masukkan Detail Layanan (Ukuran menyesuaikan dengan isi)"></textarea>
                    <br><br>

                    <input type="file" id="file" style="display: none;" name="image">
                    <a href="#" id="fileSelect">Pilih Gambar</a>
                    <div id="fileDisplay">
                        <p>Gambar Belum Dipilih</p>
                    </div>

                    <x-button></x-button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    ClassicEditor
        .create( document.querySelector( '#detail' ) )
        .catch( error => {
            console.error( error );
        } );
</script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script type="text/javascript" src="/assets/js/previewImage.js"></script>
{{-- <script>
    ClassicEditor
        .create( document.querySelector( '#description' ), {
            cloudServices: {
                tokenUrl: 'https://75402.cke-cs.com/token/dev/7798ed314adc402b84b88d5ccfc6097c65bda6b8a489e8692b60e7a975f5',
                uploadUrl: 'https://75402.cke-cs.com/easyimage/upload/'
            }
        })
        
        .catch( error => {
            console.error( error );
        } );
</script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> --}}

