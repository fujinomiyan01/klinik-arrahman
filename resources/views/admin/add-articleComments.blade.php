<x-app-layout title="Add Article Comments"></x-app-layout>
@if(count($errors) > 0)
  <div class="alert alert-danger" id="alert">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<div class="container mt-3">
  <div class="row">
    <div class="col-xl-12">
      <div class="card p-5">
        <form action="" method="post" class="mt-3">
            @csrf
            <label for="name">Nama User</label>
            <input type="text" name="name" id="name" class="form-control mb-4" placeholder="Nama User">

            <label for="email">Email User</label>
            <input type="text" name="email" id="email" class="form-control mb-4" placeholder="Email User">

            <label for="article_title">Judul Artikel yang Dikomentari</label>
            <select name="article_title" id="article_title" class="form-control mb-4">
                @foreach ($article_titles as $article_title)
                    <option value="{{ $article_title->title }}">{{ $article_title->title }}</option>
                @endforeach
            </select>

            <label for="message">Pesan User</label>
            <textarea name="message" id="message" class="form-control mb-4" placeholder="Pesan..."></textarea>

            <label for="comment_status">Status Komentar (Aktif atau disembunyikan)</label>
            <select name="comment_status" id="comment_status" class="form-control mb-4">
                <option value="active">Active</option>
                <option value="hidden">Hidden</option>
            </select>
            
            {!! NoCaptcha::renderJs('id', false, 'onloadCallback') !!}
            {!! NoCaptcha::display() !!}      
                  
            <x-button></x-button>
        </form>
      </div>
    </div>
  </div>
</div>