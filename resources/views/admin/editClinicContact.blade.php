<x-app-layout title="Edit Contact Clinic"></x-app-layout>
<link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>

@if(count($errors) > 0)
  <div class="alert alert-danger" id="alert">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<div class="container mt-3">
  <div class="row">
    <div class="col-xl-12">
      <div class="card p-5">
        <form action="" method="post">
          @csrf
            
          <label for="clinic_name">Nama Klinik</label>
          <br>
          <select name="clinic_name" id="clinic_name" class="chosen-select form-control mb-4"> 
              <option value="Klinik Ar-Rahman Medika" <?php if($clinic_edit->clinic_name == "Klinik Ar-Rahman Medika") {
                echo "selected";
              } ?>>Klinik Ar-Rahman Medika</option>
            @foreach ($clinics as $clinic)
              <option value="{{ $clinic->clinic_name }}" <?php if($clinic->clinic_name == $clinic_edit->clinic_name) {
                echo "selected";
              } ?>>{{$clinic->clinic_name}}</option>
            @endforeach
            
          </select>
          <br><br>
    
          <label for="facebook_name">Nama Facebook</label>
          <br>
          <input type="text" class="form-control mb-4" name="facebook_name" id="facebook_name" value="{{ $clinic_edit->facebook_name }}">

          <label for="facebook_link">Link Facebook</label>
          <br>
          <textarea class="form-control mb-4" name="facebook_link" id="facebook_link">{{ $clinic_edit->facebook_link }}</textarea>
    
          <label for="instagram_name">Nama Instagram</label>
          <br>
          <input type="text" class="form-control mb-4" name="instagram_name" id="instagram_name" value="{{ $clinic_edit->instagram_name }}">

          <label for="instagram_link">Link Instagram</label>
          <br>
          <textarea class="form-control mb-4" name="instagram_link" id="instagram_link">{{ $clinic_edit->instagram_link }}</textarea>
    
          <label for="twitter_name">Nama Twitter</label>
          <br>
          <input type="text" class="form-control mb-4" name="twitter_name" id="twitter_name" value="{{ $clinic_edit->twitter_name }}">

          <label for="twitter_link">Link Twitter</label>
          <br>
          <textarea type="text" class="form-control mb-4" name="twitter_link" id="twitter_link">{{ $clinic_edit->twitter_link }}</textarea>
    
          <label for="address">Alamat Klinik</label>
          <br>
          <textarea class="form-control mb-4" name="address" id="address">{{ $clinic_edit->address }}</textarea>
    
          <label for="googleMap_link">Link Google Map Klinik</label>
          <br>
          <textarea class="form-control mb-4" name="googleMap_link" id="googleMap_link">{{ $clinic_edit->googleMap_link }}</textarea>

          <label for="email">Email Klinik</label>
          <br>
          <textarea class="form-control mb-4" name="email" id="email">{{ $clinic_edit->email }}</textarea>
          
          <label for="phone">Nomor Telepon atau Kontak Klinik</label>
          <input type="text" inputmode="numeric" pattern="[-+]?[0-9]*[.,]?[0-9]+" class="form-control mb-4" name="phone" id="phone" value="0{{ $clinic_edit->phone }}">
             
          <x-button></x-button>
      </form>
      </div>
    </div>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
<script>
  $(".chosen-select").chosen({
    no_results_text: "Oops, nothing found!"
  })
</script>
<script>
  var time = document.getElementById("alert");

  setTimeout(function(){
    time.style.display = "none";
  }, 10000);
</script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>