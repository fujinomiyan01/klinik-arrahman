<x-app-layout title="Add Feedback"></x-app-layout>
@if(count($errors) > 0)
  <div class="alert alert-danger" id="alert">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<div class="container">
  <div class="row">
    <div class="col-xl-12">
      <div class="card p-5">
        <form action="" method="post">
            @csrf
            <label for="patient">Nama</label>
            <input type="text" name="patient" id="patient" class="form-control mb-4" placeholder="Nama Pasien atau User">

            <label for="message">Pesan</label>
            <textarea name="message" id="message" class="form-control mb-4" placeholder="Masukkan pesan..."></textarea>

            <label for="rate-bintang">Rate bintang</label>
            <select name="rate" id="rate-bintang" class="form-control mb-4">
                <option value="5">5</option>
                <option value="4">4</option>
                <option value="3">3</option>
                <option value="2">2</option>
                <option value="1">1</option>
            </select>
            
            <x-button></x-button>
        </form>
      </div>
    </div>
  </div>
</div>