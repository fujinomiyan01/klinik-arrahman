<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="styles/about.css">
    <link rel="stylesheet" href="responsive/headernavbar.css">
    <link rel="stylesheet" href="responsive/footer.css">
    <link rel="stylesheet" href="styles/media-query.css">

    <link rel="icon" href="/img/icon_logo-removebg-preview.png" type="image/png">


    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Tentang Klinik Arrahman</title>
  </head>
  <body>
  <x-user-header></x-user-header>



    <!--contenhero-->
    <section style=" background-color: rgba(11, 113, 37, 0.1);">
      <div class="contenhero container px-4 px-md-0 xl-">
        <div class="row">
          <div class="page d-flex">
            <div class="col-12 col-md-7">
              <div class="text">
                <h1 class="md-5"><h1>Tentang Kami, <font>Klinik Ar-Rahman</font></h1></h1>
                @foreach ($profiles as $profile)
                  <p class="desk" style="width: 90%;">{!! $profile->description !!}</p>
                    
                @endforeach
              </div>
            
            </div>
            <div class="col-12 col-md-5 mt-5 mt-md-0 d-flex justify-content-center justify-content-md-end d-none d-md-block">
              <a href="">
                <div class="hero-img ">
                  <img src="gambar/img/hero-img.svg" alt="" class="img-fluid">
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>


    

<!-- {{-- Visi Misi --}} -->
{{-- <div class="d-flex flex-column  container">
  <div class="row justify-content-center">
    <section class="visi-misi" id="visi-misi">
      @foreach ($profiles as $profile)
            <div class="visi-container">
                <div class="header-visi">
                    <h1>VISI</h1>
                </div>
                <p class="mt-5">{{ $profile->vision }}</p>
            </div>
      @endforeach
    </section>
  </div>
  <div class="d-flex justify-content-end">
    <div class="row justify-content-center">
      <section class="visi-misi" id="visi-misi">
        @foreach ($profiles as $profile)
              <div class="visi-container">
                  <div class="header-visi">
                      <h1>Misi</h1>
                  </div>
                  <p class="mt-5">{!! $profile->mission !!}</p>
              </div>
        @endforeach
      
      </section>
    </div>
  </div>
</div> --}}

  <section class="container">
    <div class="row ">
      <div class="col-xl-">
        <div  class="visi-misi" id="visi-misi">
          @foreach ($profiles as $profile)
            <div class="visi-container">
              <div class="header-visi">
                  <h1>VISI</h1>
              </div>

              <p class="mt-5">{{ $profile->vision }}</p>
            </div>
          @endforeach
        </div>
      </div>

      <div class="col d-flex justify-content-end">
        <div class="visi-misi" id="visi-misi">
          @foreach ($profiles as $profile)
            <div class="visi-container">
              <div class="header-visi">
                  <h1>Misi</h1>
              </div>
              <p class="mt-5">{!! $profile->mission !!}</p>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </section>

  
 

    

<!-- {{-- Our Team --}} -->
<div class="our-team">
  <div class="container">
      <div class="title">
          <h1 class="mb-5">Meet Our <span style="color: #0B7125">Team</span></h1>
      </div>

      <div class="row">
        @foreach ($doctors as $doctor)
          <div class="col text-center mb-5  mx-auto">
            <img src="{{ $doctor->image }}" alt="" style="border-radius: 50px 0;">
              <div class="ket-doctor mt-4">
                  <h3>{{ $doctor->doctor_name }}</h3>
                  <h5>{{ $doctor->role }}</h5>
                  <p>{!! $doctor->detail !!}</p>
              </div>
          </div>
        @endforeach
      </div>
  </div>
</div>
    
  <x-user-footer></x-user-footer>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>