<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" href="styles/layanan.css">
  <link rel="stylesheet" href="responsive/headernavbar.css">
  <link rel="stylesheet" href="responsive/footer.css">

  <link rel="icon" href="/img/icon_logo-removebg-preview.png" type="image/png">


  <title>Fasilitas</title>
</head>
<body>

  <x-user-header></x-user-header>


  <div class="conten">
    <h1>Fasilitas</h1>
  </div>

  <section class="card-services container">
    <div class="row">
      @foreach ($facilities as $facility)
          <div class="card mb-5" id={{ $facility->route }}> 
            <h2>{!!  $facility->facility_name  !!}</h2>
                                                                                                                 
            <h4>CABANG: KLINIK AR-RAHMAN MEDIKA & KLINIK AR-RAHMAN GALUH MAS</h4>
            
            <div class="row">
              <div class="col-md-6 mb-3">
                <img src="{{ $facility->image }}" alt="">
              </div>
              <div class="col-md-6">
                <div class="detail">
                  <p>{!! $facility->detail !!}</p>
  
                  <div class="sm-icon d-flex">
                    <img src="gambar/icon/time-icon.svg" alt="">
                    <p>Sen-Min: <span>08.00 - 22.00</span></p>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
      @endforeach


      <div class="">{{ $facilities->links() }}</div>
    </div> 
  </section>

    


  <x-user-footer></x-user-footer>



  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="owl.carousel.js"></script>
</body>
</html>