@extends('errors::minimal')

@section('title', __('Too Many Requests'))
@section('code', 'Terlalu banyak request')
@section('message', __('Too Many Requests'))
