@extends('errors::minimal')

@section('title', __('Unauthorized'))
@section('code', 'Anda sekarang belum dapat melakukan hal ini')
@section('message', __('Unauthorized'))
