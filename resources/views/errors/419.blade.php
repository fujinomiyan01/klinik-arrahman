@extends('errors::minimal')

@section('title', __('Page Expired'))
@section('code', 'Halaman tidak lagi ada')
@section('message', __('Page Expired'))
