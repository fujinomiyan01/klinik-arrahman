@extends('errors::illustrated-layout')

@section('title', __('Server Error'))
@section('code', 'Server tidak dapat merespon')
@section('message', __('Server Error'))
