@extends('errors::illustrated-layout')

@section('title', __('Page Not Found'))
@section('code', 'Halaman tidak ditemukan')
@section('message', __('Halaman yang anda maksud tidak ada'))
