@extends('errors::minimal')

@section('title', __('Forbidden'))
@section('code', 'Anda tidak dapat mengakses halaman ini')
@section('message', __($exception->getMessage() ?: 'Forbidden'))
