@extends('errors::minimal')

@section('title', __('Service Unavailable'))
@section('code', 'Layanan yang anda maksud tidak ada')
@section('message', __('Service Unavailable'))
