<!-- Footer -->
<footer class="footer">
  <div class="nav nav-footer ">
    <div class="col-lg-6 justify-content-center justify-content-lg-end">
      <div class="copyright text-lg-left  text-muted">
        &copy; 2021 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Madtive Studio</a>
      </div>
    </div>
    <div class="col-lg-6">
      {{-- <ul class="nav nav-footer justify-content-center justify-content-lg-end">
        <li class="nav-item">
          <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
        </li>
        <li class="nav-item">
          <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
        </li>
      </ul> --}}
    </div>
  </div>
</footer>